/*
 * laser.cpp
 *
 *  Created on: Aug 27, 2012
 *      Author: nusuav
 */
#include <pthread.h>
#include <stdio.h>
#include "slam.h"
#include "laser.h"
#include "state.h"
#include "hokuyo.h"
#include "uav.h"
#include "main.h"

extern clsMain _main;

#define LASER30_OFFSET_TO_GROUND 130 //in mm

extern clsState _state;

clsURG::clsURG()
{
	m_nCount = 0;
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutex_init(&m_mtxURG, &attr);
}

clsURG::~clsURG()
{
	pthread_mutex_destroy(&m_mtxURG);
}

BOOL clsURG::InitThread()
{
	m_nURG = 0;
	m_prev_height = 0;

	if (!urg_init()) {
		printf("[URG] init fail.\n");
		return FALSE;
	}

	UAVGP.init();

	::memset(&m_info,0,sizeof(m_info));

	SAFMC_height.init();
	infile = fopen("laser_7000010101b_flight1.log", "r");
	outfile = fopen("laser_result.log", "w");

	m_info.height = 0;
	m_SlamHeight = 0;
	m_bLaserDataReady = false;

    printf("[URG] Start\n");
	return true;
}

void clsURG::ExitThread()
{
	urg_exit();
	printf("[URG] quit\n");
}

void clsURG::GetPos()
{
	long temp[13];
	long swap;

	UAVSTATE &state = _state.GetState();

	for (int i=534; i<=546; i++)
	{
		temp[i-534] = range[i];
	}

	// Bubble sort for medium filter
	bool sorted = false;
    while(!sorted)
	{
		sorted = true;
		for (int i=0; i<13; i++)
		{
			if (temp[i]<temp[i+1])
			{
				swap = temp[i];
				temp[i] = temp[i+1];
				temp[i+1] = swap;
				sorted = false;
			}
		}
	}

    if (temp[6]<=25000 && temp[6]>= 80)
    {
    	m_info.height = (double)(temp[6] - LASER30_OFFSET_TO_GROUND)*cos(state.b)*cos(state.a); //in mm
    }
}


int clsURG::EveryRun()
{
	if ( _main.getMainCount()%10 != 0)
		return TRUE;

	double tPack = ::GetTime();

/*** read laser scanner data from file ***/
	double temp;
	double a;
	double b;
//	for(int i=0; i<1087; i++)
//	{
//		if (i<1081)
//		{
//			fscanf(infile,"%lf",&temp);
//			range[i] = (long)temp;
//		}
//		else if (i == 1082)
//			fscanf(infile,"%lf",&a);
//		else if (i == 1083)
//			fscanf(infile,"%lf",&b);
//		else
//			fscanf(infile,"%lf",&temp);
//	}

/* read laser scanner data from real device ***/
	urg_requestdata(range);

	m_SlamHeight = SAFMC_height.localization(range);
	m_SlamHeight -= LASER30_OFFSET_TO_GROUND; //m_SlamHeight is the angle compensated height,
											  //output the height from the bottom of the landing skid to the ground in mm
	if(!m_bLaserDataReady)
		m_bLaserDataReady = true;

	if (m_nCount % 5 == 0)
		printf("[Laser] height %.3f \n", m_SlamHeight);
//	m_SlamTargetY = SAFMC_height.GetShipY();
//	m_bLaserYReady = SAFMC_height.GetShipYConfidence();

//	printf("m_SlamTargetY: %f\n", m_SlamTargetY);

//	GetPos();

	pthread_mutex_lock(&m_mtxURG);
//	if (m_nURG < MAX_IM9PACK)
//	{
//		m_tURG[m_nURG] = tPack;
//		for (int i=0; i<MAX_URG_PT; i++) {
//			ranges[m_nURG][i] = range[i];
//		}
//		heights[m_nURG] = m_info.height;
//		fronts[m_nURG] 	= m_SlamHeight;
//		lefts[m_nURG] 	= m_info.left;
//		UAVSTATE &state = _state.GetState();
//		phi[m_nURG] 	= state.a;
//		tht[m_nURG] 	= state.b;
//		psi[m_nURG] 	= state.c;
//		m_nURG++;
//	}
	pthread_mutex_unlock(&m_mtxURG);
	return TRUE;
}

void clsURG::SetLaserConfig(char *laserDevice, short size, short flag)
{
	::memcpy(m_laserConfig.laserDevice, laserDevice, size);
	m_laserConfig.flag = flag;
}
