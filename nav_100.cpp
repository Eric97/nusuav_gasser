/*
 * nav_100.cpp
 *
 *  Created on: Apr 8, 2013
 *      Author: nusuav
 */
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include "nav_100.h"
#include "uav.h"
#include "state.h"
#include "im8.h"
#include <stdio.h>

extern clsState _state;
extern clsIM8 _im8;

#define PAN_TRIM 2830.5
#define TILT_TRIM 3030
#define SVO_ANGLE_PAN_SCALING (-18.1594) //angle in degrees
#define SVO_ANGLE_TILT_SCALING (18.1594)

nav_100::nav_100() {
	// TODO Auto-generated constructor stub

}

nav_100::~nav_100() {
	// TODO Auto-generated destructor stub
}

BOOL nav_100::InitThread()
{
	m_nsNav = ::open("/dev/serusb2", O_RDWR|O_NONBLOCK );

	if (m_nsNav == -1) {
				printf("[NAV100] Open NAV_100 serial port (/dev/serusb2) failed!\n");
				return FALSE;
		}

	termios term;
	tcgetattr(m_nsNav, &term);
	bzero(&term, sizeof(termios));

	cfsetispeed(&term, 38400);				//input and output baudrate
	cfsetospeed(&term, 38400);

	term.c_cflag = CS8 | CLOCAL | CREAD;				//communication flags
	tcsetattr(m_nsNav, TCSANOW, &term);
	tcflush(m_nsNav, TCIOFLUSH);


//	termios term;
//	memset(&term,0,sizeof(term));
//	tcgetattr(m_nsNav, &term);
//	cfsetispeed(&term, NAV_BAUDRATE);			//input and output baudrate
//	cfsetospeed(&term, NAV_BAUDRATE);
//
//	term.c_cflag &= ~(CS5|CS6|CS7|HUPCL|IHFLOW|OHFLOW|PARENB|CSTOPB|CSIZE); // Note: must do ~CSIZE before CS8
//	term.c_cflag |= (CS8 | CLOCAL | CREAD);
//	//term.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // Non-canonical interpretation
//	term.c_iflag = 0; // Raw mode for input
//	term.c_oflag = 0; // Raw mode for output
//	tcflush(m_nsNav,TCIOFLUSH);
//	tcsetattr(m_nsNav, TCSANOW, &term);

	 m_nBuffer = 0;
	 headingAngle = 0.0;

	 //Added by LPD for 2013-UAVGP PAN-TILE control
	m_panAngle = 0; m_tiltAngle = 0;

	 printf("[NAV100] Start\n");
	return TRUE;
}

int nav_100::EveryRun()
{
		// channel 0 (channel 0 start) for camera PTU control
		// channel 0 -> PAN; channel 1 -> TILT
		char pan[6], tilt[6];
		pan[0] = tilt[0] = 0x80;
		pan[1] = tilt[1] = 0x01;
		pan[2] = tilt[2] = 0x04;
		pan[3] = 0;	// servo channel 0
		tilt[3] = 1; // servo channel 1


		PanTiltConpensateUAV(_state.GetState().a, _state.GetState().b,  &m_panAngle, &m_tiltAngle);

		int nPan = int(PAN_TRIM + SVO_ANGLE_PAN_SCALING*m_panAngle*180/PI);
		int nTilt = int(TILT_TRIM + SVO_ANGLE_TILT_SCALING*m_tiltAngle*180/PI);

		pan[4] = nPan / 128; pan[5] = nPan % 128;
		tilt[4] = nTilt / 128; tilt[5] = nTilt % 128;
		int nWrite = write(m_nsNav, pan, 6);
		write(m_nsNav, tilt, 6);
	/* End of LPD's part */


//	bool m_bGetPackage = false;
//	m_bGetPackage = GetPack();
//		if(m_bGetPackage){
//			Translate();
//		}

	return TRUE;
}

bool nav_100::GetPack(){
	// Input data stream format:
	// $VNMAG,-01.1524,+00.8496,-00.8447*75


	int nRead = ::read(m_nsNav, m_szBuffer + m_nBuffer, MAX_NAVBUFFER);
//	if(nRead > 0)
//		printf("Nav100 Read %d bytes\n", nRead);

//for(int i = 0; i<nRead; i++)
//{
//	printf("%c",m_szBuffer[i]);
//}
//printf("\n");

	if (nRead > 0)
		m_nBuffer += nRead;

	for (int i = 0; i<= m_nBuffer-1;){
		if((unsigned char)m_szBuffer[i] != '$'){
			i++;
			continue;
		}
		if((unsigned char)m_szBuffer[i+1] != 'V'){
					i++;
					continue;
				}
		if((unsigned char)m_szBuffer[i+2] != 'N'){
					i++;
					continue;
				}
		if((unsigned char)m_szBuffer[i+3] != 'M'){
					i++;
					continue;
				}
		if((unsigned char)m_szBuffer[i+4] != 'A'){
					i++;
					continue;
				}
		if((unsigned char)m_szBuffer[i+5] != 'G'){
							i++;
							continue;
						}

		if (i + 36 >m_nBuffer){
			break;
		}
		memcpy(m_szDataBuf, m_szBuffer + i + 7, 17);
		m_nBuffer = 0;
		return true;
	}
	return false;;
}

void nav_100::Translate(){
	//Buffer format: -01.1524,+00.8496;
	// Magx, magy

	int sign = 1;
	if(m_szDataBuf[0] == '-')
		sign = -1;
	float mag_x = ((int)m_szDataBuf[1]-48)*10 + ((int)m_szDataBuf[2]-48)*1 + ((int)m_szDataBuf[4]-48)*0.1 + ((int)m_szDataBuf[5]-48)*0.01 + ((int)m_szDataBuf[6]-48)*0.001 + ((int)m_szDataBuf[7]-48)*0.0001;
	mag_x *= sign;

	sign = 1;
	if(m_szDataBuf[9] == '-')
		sign = -1;
	float mag_y = ((int)m_szDataBuf[10]-48)*10 + ((int)m_szDataBuf[11]-48)*1 + ((int)m_szDataBuf[13]-48)*0.1 + ((int)m_szDataBuf[14]-48)*0.01 + ((int)m_szDataBuf[15]-48)*0.001 + ((int)m_szDataBuf[16]-48)*0.0001 ;
	mag_y *= sign;

	if(mag_x > 0 && mag_y >0)
		headingAngle = atan2f(mag_y, mag_x)*(-1); //in radian
	else if(mag_x > 0 && mag_y <0)
		headingAngle = atan2f(abs(mag_y), mag_x);
	else if(mag_x < 0 && mag_y > 0)
		headingAngle = atan2f(mag_y, abs(mag_x)) - PI;
	else if(mag_x < 0 && mag_y < 0)
		headingAngle = PI - atan2f(abs(mag_y), abs(mag_x));

//	printf("NAV 100 [NAV heading: %.3f\n", headingAngle*180/PI);

	INPI(headingAngle);
	_state.updateHeadingAngleFromNAV100(headingAngle);
//	_im8.updateRawHeading();
}

void nav_100::ExitThread()
{
	::close(m_nsNav);
	printf("[NAV100] quit\n");
}

/* Added by LPD for 2013UAVGP */
void nav_100::PanTiltConpensateUAV(double phiUAV, double thtUAV, double* phiPT, double* thtPT)
{
          //double phiUAV=uavshipStates.UAVPhi;
          //double thtUAV=uavshipStates.UAVTht;
          //double phiPT, thtPT;
          *phiPT=asin(-1*sin(phiUAV)*cos(thtUAV));
          *thtPT=atan(-sin(thtUAV) / (cos(phiUAV)*cos(thtUAV)));
}
/* End of LPD's part for 2013UAVGP */
