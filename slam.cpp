#include <iostream>
#include <stdio.h>
#include "slam.h"
#include <math.h>
#include <stdlib.h>
#include <list>
#define SIZE 1440

//#include <algorithm>
using namespace std;

void slam:: init()
{
    field_of_view = 270.25*pi/180;
 //   effective_range_near = 10000;
	effective_range = 10000;
//	effective_range_far = 15000;

    for(int i=0;i<1081;i++)
    {
        bearing_mat[i] = (pi-field_of_view)/2+i*field_of_view/(reading_count-1);
        if(bearing_mat[i] > pi)
            bearing_mat[i] = bearing_mat[i]-2*pi;
        cos_bearing_mat[i] = cos(bearing_mat[i]);   
        sin_bearing_mat[i] = sin(bearing_mat[i]);   
      //  cout<<sin_bearing_mat[i]<<endl;
    }


}


void slam:: slam_points(double data[])
{
    for(int i = 0; i < reading_count ; i++)
    {

        points[i].x = data[i]*cos_bearing_mat[i];
        points[i].y = data[i]*sin_bearing_mat[i];
        points[i].theta = bearing_mat[i];
        points[i].r = data[i];

        //        cout<<"("<<points[i].x<<","<<points[i].y<<")"<<endl;
    }

}
/*
void slam:: range_f(double input[],double data[])
{
    for(int i =0;i < reading_count;i++)
    {
        if(input[i]>effective_range)
            data[i]=0;
        else
            data[i]=input[i];
     //   cout<<i<< " " << data[i]<<endl;

    }

}*/

void slam:: range_f(long int input[],double data[])
{

    double range[SIZE] ={0};
    double x,y,angle;
	for(int i =0;i < reading_count;i++)
	    {

	        if(input[i]>25000||i<381||i>780)
	            data[i]=0;
	        else
	            data[i]=input[i];
	    }

}

void slam:: split_merge( point detected_points[],int n ,int threshold)
{  
    int last = n-1;
    double m,c,theta,r;
    double point_distance[1081];
    double winner_value;
    int winner_index;
    double l;
    line_ detect;
    if(fabs(detected_points[last].x-detected_points[0].x)>=1)//y = mx+c
    {
        m = (detected_points[last].y-detected_points[0].y)/(detected_points[last].x-detected_points[0].x);
        c = detected_points[0].y-detected_points[0].x*m;
        theta = atan(-1/m);
        r = c*sin(theta);
        if(m*c>0)
        	 theta = theta + pi;
    }
    else //x = my+c
    {

        m = (detected_points[last].x-detected_points[0].x)/(detected_points[last].y-detected_points[0].y);
        c = detected_points[0].x-detected_points[0].y*m;
        theta = atan(-m);
        r = c*cos(theta);
        if(c<0)
           	 theta = theta + pi;
    }

    r = fabs(r);

    point_distance[0] = detected_points[0].x*cos(theta)+detected_points[0].y*sin(theta)-r;
    winner_value = fabs(point_distance[0]);
    winner_index = 0;
    for (int j = 1; j<last;j=j+6)
    {
        point_distance[j] = detected_points[j].x*cos(theta) + detected_points[j].y*sin(theta) - r;
        if(fabs(point_distance[j])>winner_value)
        {
            winner_value = fabs (point_distance[j]);
            winner_index = j;
        }
    }
    if(winner_value >threshold)
    {
        split_merge(detected_points,winner_index+1,threshold);
        split_merge(&detected_points[winner_index+1],n-winner_index-1,threshold);
    }
    else 
    {

        l = (detected_points[last].x- detected_points[0].x)*(detected_points[last].x-detected_points[0].x);
        l+= (detected_points[last].y- detected_points[0].y)*(detected_points[last].y-detected_points[0].y);
        l = sqrt(l);
        detect.p1 = detected_points[0];
        detect.p2 = detected_points[last];
        detect.theta = theta;
        detect.r = r;
        detect.length = l;
        detected_line_local.push_back(detect);
    }

}

void slam:: line_detection()
{
    int start_index = 0;
    list<line_> temp;
    list<line_> result;
    int size;
    detected_line_local.clear();   
    for(int i=0;i<reading_count-1;i++)
    {
        if(fabs(points[i].r - points[i+1].r)>60||points[i].r<20||i==reading_count -2)
        {
            if(start_index!=i)
            {
                size = i-start_index+1;
                split_merge(&points[start_index],size,200);
            }
            start_index = i+1;

        }

    }
}
void slam::line_f(int threshold)
{
    list<line_>:: iterator l = detected_line_local.begin();
    for( ; l!=detected_line_local.end() ; )
    { 
        if(l->length<threshold)
            detected_line_local.erase(l);
        else
            l++;
    }

}
position slam:: localization(long int input[])
{


    range_f(input,data);
    slam_points(data);
    //detected_line_local = line_detection(points); 

    line_detection(); 
    double h;
    int line_size = detected_line_local.size();
  //  cout<<"line size is "<<line_size<<endl;
    	 line_ temp;
         list<line_> :: iterator iter1 = detected_line_local.begin();
         list<line_> :: iterator iter2 ;
        for(;iter1!=detected_line_local.end();iter1++)
        {
            iter2 = iter1;
           for( ;iter2 != detected_line_local.end();iter2++)
           {
              if(iter1->length < iter2->length)
                {
                  temp = *iter1;
                 *iter1 = *iter2;
                 *iter2 = temp;
                }
    	   }
        }


    	iter1 = detected_line_local.begin();
 //       cout<<"iter1->theta"<<iter1->theta<<endl;
    	    for(;iter1!=detected_line_local.end();iter1++)
    		{
    	    //	cout<<"iter1->theta is "<<iter1->theta<<endl;
    	    //	cout<<"iter1->r"<<iter1->r<<endl;
                if(fabs(iter1->theta-pi/2)<0.32)
    			break;
    		}

	 //   	cout<<"final->theta is "<<iter1->theta<<endl;
	 //   	cout<<"final->r"<<iter1->r<<endl;

	    	if(iter1==detected_line_local.end())
	    	{
	    		cur_position.flag = 0;

	    	}
	    	else
	    	{
	    		cur_position.flag = 1;

	    	}
            cur_position.height = iter1->r;



  return cur_position;
}
