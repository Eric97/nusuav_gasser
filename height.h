#ifndef HEIGHT_H_
#define HEIGHT_H_

#include <iostream>
#include <list>

#include "slam.h"
using namespace std;

#define pi 3.141592654
#define reading_count_height 481
#define start_angle 0.523598776 //(pi/6.0)
#define angle_per_index (pi/720.0)
#define START_INDEX 300 //((int)((start_angle+pi/4)/angle_per_index))

struct ship_ {
	point p1;
	point p2;
	point p_mid;
	double width;
	int index;
};

class height {

private:
	double h; // height
	bool height_flag; // whether the ground line has been found
	double y; // horizontal displacement w.r.t the 1st ship in the ship frame
	bool confidence_y;
	double a, b; // UAV roll, pitch angles
	line_ ground; // the ground line

	double field_of_view;
	double bearing_mat[reading_count_height];
	double cos_bearing_mat[reading_count_height];
	double sin_bearing_mat[reading_count_height];

	double data[reading_count_height];
	point points[reading_count_height];
	list <line_> detected_line_local; // use a list to store all extracted lines
	int valid_count;

	list <ship_> ships; // use a list to store all extracted ships
	point ship_points[reading_count_height];
	int ship_point_count;
	int ship_count;

public:
	void init();
	void SlamPoints(long int data[]);
	void SplitMerge(point detected_points[], int n, int threshold);
	void LineDetection();
	double localization(long int input[]);
	int GroundDetection();
	line_ LMS(int start,int end);
	int ShipDetection();
	bool GetHeightMeasurementFlag() { return height_flag; }
	int GetShipCount() { return ship_count; }
	double GetShipYConfidence() { return confidence_y; }
	double GetShipY() { return y; }
	double GetHeight() { return ground.r; }
	point Laser2Ship (point in, double a, double b);
};

#endif
