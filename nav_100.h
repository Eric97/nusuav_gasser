/*
 * nav_100.h
 *
 *  Created on: Apr 8, 2013
 *      Author: nusuav
 */

#ifndef NAV_100_H_
#define NAV_100_H_

#include "thread.h"

#define NAV_BAUDRATE 115200
#define MAX_NAVBUFFER	4096

class nav_100: public clsThread {
public:
	nav_100();
	virtual ~nav_100();
protected:
	int m_nsNav;
	char m_szBuffer[MAX_NAVBUFFER];
	char m_szDataBuf[1024];
	int m_nBuffer;
	float headingAngle;
	double m_panAngle, m_tiltAngle;

protected:
	bool GetPack();
	void Translate();
public:
	virtual BOOL InitThread();
	virtual int EveryRun();
	virtual void ExitThread();
	/* Added by LPD for 2013UAVGP */
	void PanTiltConpensateUAV(double phiUAV, double thtUAV, double* phiPT, double* thtPT);
	/* End of LPD's Part */
};

#endif /* NAV_100_H_ */
