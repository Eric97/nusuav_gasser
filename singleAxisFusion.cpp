/*
 * singleAxisFusion.cpp
 *
 *  Created on: Jul 30, 2013
 *      Author: Peidong
 */

#include "singleAxisFusion.h"
#include "uav.h"
#include "stdio.h"
#include "opencv2/opencv.hpp"
using namespace cv;

#define MAX_LASER_LOST_STEP 1e9

singleAxisFusion::singleAxisFusion() {
	// TODO Auto-generated constructor stub

	::memset(_Kalman_X, 0, 4*sizeof(double));
	::memset(_Kalman_A, 0, 16*sizeof(double));
	::memset(_Kalman_C, 0, 16*sizeof(double));
	::memset(_Kalman_Q, 0, 16*sizeof(double));
	::memset(_Kalman_RwLaser, 0, 16*sizeof(double));
	::memset(_Kalman_RwoLaser, 0, 16*sizeof(double));
	::memset(_Kalman_R, 0, 16*sizeof(double));
	::memset(_Kalman_Y, 0, 4*sizeof(double));
	::memset(_Kalman_P, 0, 16*sizeof(double));
	::memset(&m_fusionState, 0, 4*sizeof(double));

	_Kalman_P[0][0] = 1;
	_Kalman_P[1][1] = 1;
	_Kalman_P[2][2] = 1;
	_Kalman_P[3][3] = 1;

	m_bLaserFail = false;
	m_bFilterReady = false;
	m_t0 = 0;
	m_periodInitialization = 5;// default 5s for filter initialization;
	m_laserLostTh = 1;//default 1m threshold for switch between laser and GPS
	m_nLaserFailCounter = 0;

	m_nInnovationFilterCov = 0.05*0.05;
}

singleAxisFusion::~singleAxisFusion() {
	// TODO Auto-generated destructor stub
}

void singleAxisFusion::Init(const SINGLEAXISSTATE initialState){
	Kalman_X.Reset(4, (double *)_Kalman_X, TRUE);
	Kalman_A.Reset(4, 4, (double *)_Kalman_A, TRUE);
	Kalman_AT.Reset(4, 4, NULL, false);
	Kalman_C.Reset(4, 4, (double *)_Kalman_C, TRUE);
	Kalman_CT.Reset(4, 4, NULL, false);
	Kalman_Q.Reset(4, 4, (double *)_Kalman_Q, TRUE);
	Kalman_RwLaser.Reset(4, 4, (double *)_Kalman_RwLaser, TRUE);
	Kalman_RwoLaser.Reset(4, 4, (double *)_Kalman_RwoLaser, TRUE);
	Kalman_R.Reset(4, 4, (double *)_Kalman_R, TRUE);

	_Kalman_R[0][0] = _Kalman_RwLaser[0][0];
	_Kalman_R[1][1] = _Kalman_RwLaser[1][1];
	_Kalman_R[2][2] = _Kalman_RwLaser[2][2];
	_Kalman_R[3][3] = _Kalman_RwLaser[3][3];

	Kalman_Y.Reset(4, (double *)_Kalman_Y, TRUE);
	Kalman_P.Reset(4, 4, (double *)_Kalman_P, TRUE);
	Kalman_K.Reset(4, 4, NULL, false);

	_Kalman_X[0] = initialState.estx;
	_Kalman_X[1] = initialState.estv;
	_Kalman_X[2] = initialState.esta;
	_Kalman_X[3] = initialState.estBias;
}

void singleAxisFusion::UpdateFilter(SINGLEAXISMEASUREMENT measurement){
	double tc = ::GetTime();
	double tElapse = tc - m_t0;

	 if (tElapse > m_periodInitialization)
		 m_bFilterReady = true;

	_Kalman_Y[0] = measurement.laserZ;
	_Kalman_Y[1] = measurement.ig500nZ;
	_Kalman_Y[2] = measurement.ig500nWg;
	_Kalman_Y[3] = measurement.ig500nAg;

	double error ;
	error = fabs(measurement.laserZ - Kalman_X[0])/sqrt(Kalman_P[0][0] + m_nInnovationFilterCov);

	if (tElapse > m_periodInitialization && (error > 3 || !measurement.bCorrectMeasurement)){
		_Kalman_R[0][0] = _Kalman_RwoLaser[0][0];
		_Kalman_R[1][1] = _Kalman_RwoLaser[1][1];
		_Kalman_R[2][2] = _Kalman_RwoLaser[2][2];
		_Kalman_R[3][3] = _Kalman_RwoLaser[3][3];
		m_bLaserFail = true;
	}
	else{
		_Kalman_R[0][0] = _Kalman_RwLaser[0][0];
		_Kalman_R[1][1] = _Kalman_RwLaser[1][1];
		_Kalman_R[2][2] = _Kalman_RwLaser[2][2];
		_Kalman_R[3][3] = _Kalman_RwLaser[3][3];
		m_bLaserFail = false;
	}

	clsMatrix temp(4, 4, NULL, false);
	clsMatrix temp1(4, 4, NULL, false);
	clsVector tempAX(4, NULL, false);

	clsMatrix::X(Kalman_A, Kalman_X, tempAX);
	Kalman_X = tempAX;
	clsMatrix::X(Kalman_A, Kalman_P, temp);
	clsMatrix::X(temp, Kalman_AT, temp1);
	Kalman_P = temp1;
	Kalman_P += Kalman_Q;

	clsMatrix::X(Kalman_P, Kalman_CT, temp);
	clsMatrix temp_CP(4,4, NULL, false);
	clsMatrix temp_CPC(4,4, NULL, false);
	clsMatrix::X(Kalman_C, Kalman_P, temp_CP);
	clsMatrix::X(temp_CP, Kalman_CT, temp_CPC);
	temp_CPC += Kalman_R;

	clsMatrix temp_CPCi(4,4, NULL, false);
	Mat temp_CPC_mat=Mat(4,4,CV_64FC1);
	for (int row=0; row<4; row++)
	{
		for (int col=0; col<4; col++)
		{
			temp_CPC_mat.at<double>(row,col)=temp_CPC[row][col];
		}
	}
	Mat temp_CPCi_mat=temp_CPC_mat.inv();
	for (int row=0; row<4; row++)
	{
		for (int col=0; col<4; col++)
		{
			temp_CPCi[row][col]=temp_CPCi_mat.at<double>(row,col);
		}
	}

	clsMatrix::X(temp, temp_CPCi, Kalman_K);

	clsVector tempCX(4, NULL, false);
	clsMatrix::X(Kalman_C, Kalman_X, tempCX);
	clsVector tempResidual(4, NULL, false);
	tempResidual = Kalman_Y;
	tempResidual -= tempCX;

	clsVector tempFeedBack(4, NULL, false);
	clsMatrix::X(Kalman_K, tempResidual, tempFeedBack);

	double pre_estiBias = _Kalman_X[3];

	Kalman_X += tempFeedBack;

	if (m_bLaserFail){
		_Kalman_X[3] = pre_estiBias;
	}

	clsMatrix tempKC(4,4, NULL, false);
	clsMatrix tempKCP(4,4, NULL, false);

	clsMatrix::X(Kalman_K, Kalman_C, tempKC);
	clsMatrix::X(tempKC, Kalman_P, tempKCP);
	Kalman_P -= tempKCP;

	m_fusionState.estx = _Kalman_X[0];
	m_fusionState.estv = _Kalman_X[1];
	m_fusionState.esta = _Kalman_X[2];
	m_fusionState.estBias = _Kalman_X[3];
}



