/*
 * laser.h
 *
 *  Created on: Aug 27, 2012
 *      Author: nusuav
 */

#ifndef LASER_H_
#define LASER_H_

#include "state.h"
#include "im9.h"
#include "slam.h"
#include "height.h"
#define MAX_URG	128			 // log accumulation size
#define MAX_URG_PT 		1081 // 769 for 4m laser scanner

struct LASER_CONFIG {
	char laserDevice[32];
	short flag;
};

struct LASERRAWDATA
{
	long range[MAX_URG_PT];
};

#pragma pack(push, 1)
struct LASERINFO
{
	BOOL updated;
	double front, left, height; 	// Body frame wall ranges (front, left, down)
	double front_prev, left_prev, height_prev;
	double front_delta, left_delta, height_delta;
	double heading_wall;			// Left wall heading w.r.t UAV heading
	double x, y, z; 				// UAV global position
	double u, v, w;     			// UAV body velocity
	double u_ref, v_ref, psi_ref; 	// Reference by potential fiedl
};
#pragma pack(pop)

class clsURG : public clsThread {
protected:
	pthread_mutex_t m_mtxURG;
	int m_nURG;

protected:
	double m_tURG[MAX_URG];
	long ranges[MAX_URG][MAX_URG_PT];
	long range[MAX_URG_PT];
	LASER_CONFIG m_laserConfig;
	slam UAVGP; //kangli
	double m_SlamHeight;
	double m_SlamTargetY;

private:
	double heights[MAX_URG];
	double fronts[MAX_URG];
	double lefts[MAX_URG];
	double phi[MAX_URG];
	double tht[MAX_URG];
	double psi[MAX_URG];
	LASERRAWDATA m_laser_rawData;

	double m_prev_height;
	BOOL m_bLaserDataReady;
	BOOL m_bLaserYReady;

public:
	void GetPos();

	height SAFMC_height;
	FILE* infile;
	FILE* outfile;

	void SetLaserConfig(char *laserDevice, short size, short flag);
	LASER_CONFIG GetLaserConfig() const {return m_laserConfig;}

	double getURGTime(){return m_tURG[0];}
	LASERRAWDATA &getLaserData()
	{
		for(int i=0; i<MAX_URG_PT; i++ )
			m_laser_rawData.range[i]=ranges[1][i];
		return m_laser_rawData;
	}

	virtual BOOL InitThread();
	virtual int EveryRun();
	virtual void ExitThread();
public:
	clsURG();
	virtual ~clsURG();
public:
	LASERINFO m_info;
	LASERINFO &GetInfo() { return m_info; }
	double GetLaserHeight() { return (-1)*m_SlamHeight/1000.0; /*return (-1)*m_info.height/1000.0;*/ } //in m
	double GetLaserY() { return m_SlamTargetY/1000.0;}
	BOOL GetLaserReady(){ return m_bLaserDataReady; }
	BOOL GetLaserMeasurementFlag() { return SAFMC_height.GetHeightMeasurementFlag(); }
	BOOL GetLaserYReady() { return m_bLaserYReady;}
public:
//	friend class clsMAP;
	friend class clsDLG;
};

#endif /* LASER_H_ */
