/*
 * im9.cpp
 *
 *  Created on: Aug 27, 2012
 *      Author: nusuav
 */

#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <stdint.h>
#include <iostream>

#include "uav.h"
#include "state.h"
#include "im9.h"
using namespace std;
extern clsState _state;

clsIM9::clsIM9()
{
	m_nsIM9 = -1;
	m_nIM9  = 0;
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutex_init(&m_mtxIM9, &attr);
}

clsIM9::~clsIM9()
{
	pthread_mutex_destroy(&m_mtxIM9);
}

BOOL clsIM9::InitThread()
{
    //initial setting
    m_nsIM9= ::open("/dev/ser3", O_RDWR | O_NOCTTY /*| O_NONBLOCK*/);
	if (m_nsIM9 == -1) return FALSE;

    // Setup the serial port to communicate with IMU
    termios term;
    memset(&term,0,sizeof(term));
    tcgetattr(m_nsIM9, &term);
    cfsetispeed(&term, B38400);				//input and output baudrate
    cfsetospeed(&term, B38400);
    term.c_cflag &= ~(PARENB | CSTOPB | CSIZE); // Note: must do ~CSIZE before CS8
    term.c_cflag |= CS8 | CLOCAL | CREAD;
    term.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // Non-canonical interpretation
    term.c_iflag = 0; // Raw mode for input
    term.c_oflag = 0; // Raw mode for output
    term.c_cc[VMIN] = 1;
	tcsetattr(m_nsIM9, TCSANOW, &term);
	tcflush(m_nsIM9,TCIOFLUSH);

	first_time = TRUE;

    printf("[IM9] Start\n");
	return TRUE;
}

int clsIM9::EveryRun()
{
	IM9PACK pack;
	::memset(&pack,0,sizeof(IM9PACK));

	double tPack = ::GetTime();
	BOOL bPkt = MNAVUpdate(pack);

    _state.Update(tPack, &pack);	// clsState's states follows the IMU's data

    m_im90.ail = pack.ail;
    m_im90.ele = pack.ele;
    m_im90.thr = pack.thr;
    m_im90.rud = pack.rud;
    m_im90.tog = pack.tog;

    UAVSTATE &state = _state.GetState();
/*	pthread_mutex_lock(&m_mtxIM9);
	if ( bPkt && m_nIM9 < MAX_IM9PACK)
	{
		m_tIM9[m_nIM9] = tPack;

		m_MAN[m_nIM9].aileron = m_im90.ail;
		m_MAN[m_nIM9].elevator = m_im90.ele;
		m_MAN[m_nIM9].throttle = m_im90.thr;
		m_MAN[m_nIM9].rudder = m_im90.rud;
		m_MAN[m_nIM9].sv6 = m_im90.tog;

		m_nIM9++;
	}
	pthread_mutex_unlock(&m_mtxIM9);*/

    if (m_nCount%50 == 0)
	{
		cout<<"Attitude:\t"<<state.a<<" \t"<<state.b<<" \t"<<state.c<<endl;
		cout<<"Acceleration:\t"<<state.acx<<" \t"<<state.acy<<" \t"<<state.acz<<endl;
		cout<<"Position:\t"<<state.x<<" \t"<<state.y<<" \t"<<state.z<<endl<<endl;
		cout<<"Height:\t"<<-state.z<<endl;
	}

	return TRUE;
}

void clsIM9::ExitThread()
{
	::close(m_nsIM9);
	printf("[IM9] quit\n");
}

// ---------------- below for ArduPilot -----------------
BOOL clsIM9::MNAVUpdate(IM9PACK& pack)
{
	BOOL bPkt = FALSE;
    int state = 0;
    bool checkSumOK = false;
    char input_buffer[FULL_PACKET_SIZE]={0,};
    char checkVal[2];
    unsigned char in;
    char checkA, checkB;

    int trouble_count = 0;
    int nbytes = 0;

//    printf("Hello1  ");

    while (!checkSumOK)
    {
    while (state != 4)
    {
//      printf("Hello  ");
        while(1!=read(m_nsIM9,&in,1)) printf("[IM9] IMU port no data!\n");
//      printf("%d\n", in);
//      return 0;

        switch (state)
          {
            case 0:
                switch (in)
                  {
                    case 'D':
                        state = 1;
                        break;
                    case 'I':
                        state = 2;
                        break;
                    case 'Y':
                        state = 3;
                        break;
                    default:
                        state = 0;
                  }
                break;
            case 1:
                state = (in == 'I') ? 2 : 0;
                break;
            case 2:
                state = (in == 'Y') ? 3 : 0;
                break;
            case 3:
                state = (in == 'd') ? 4 : 0;
                break;
          }

        trouble_count++;
        if ( trouble_count % 1000 == 0 ) {
            printf("[IM9] Having trouble finding a valid packet header.\n");
    }
    }

    nbytes = 0;
    while (nbytes < 2) {
        nbytes += ::read(m_nsIM9, input_buffer+nbytes, 2-nbytes);
    }
    nbytes = 0;
    while (nbytes < input_buffer[0]) {
        nbytes += ::read(m_nsIM9, input_buffer+2+nbytes, input_buffer[0]-nbytes);
    }
    nbytes = 0;
    while (nbytes < 2) {
        nbytes += ::read(m_nsIM9, checkVal+nbytes, 2-nbytes);
    }

    checkA = checkB = 0;

    for (int i = 0; i < input_buffer[0] + 2; i++)
    {
        checkA += input_buffer[i];
        checkB += checkA;
    }

    if (checkVal[0] != checkA || checkVal[1] != checkB)
    {
        checkSumOK = false;
        printf("[IM9] Data checksum failed!\n");
    }
    else
        checkSumOK = true;
    }

    DecodeIMUpacket(input_buffer, pack);
    bPkt = TRUE;

    return bPkt;
}


void clsIM9::DecodeIMUpacket(char* buffer, IM9PACK& pack)
{
    int16_t i16;

    if (first_time)
    {
		i16 = (int16_t(buffer[2]) & 0x00FF) | (int16_t(buffer[3]) << 8);
		first_pack.a = ((i16 / 100.0) * PI) / 180;
		i16 = (int16_t(buffer[4]) & 0x00FF) | (int16_t(buffer[5]) << 8);
		first_pack.b = ((i16 / 100.0) * PI) / 180;
		i16 = (int16_t(buffer[6]) & 0x00FF) | (int16_t(buffer[7]) << 8);
		first_pack.c = ((i16 / 100.0) * PI) / 180;

		if (m_nCount > 250)
		{
			first_time = FALSE;
			printf("[IM9] Yaw measurement initialized! \n");
		}
    }

	i16 = (int16_t(buffer[2]) & 0x00FF) | (int16_t(buffer[3]) << 8);
	pack.a = ((i16 / 100.0) * PI) / 180;
	i16 = (int16_t(buffer[4]) & 0x00FF) | (int16_t(buffer[5]) << 8);
	pack.b = ((i16 / 100.0) * PI) / 180;
	i16 = (int16_t(buffer[6]) & 0x00FF) | (int16_t(buffer[7]) << 8);
	pack.c = ((i16 / 100.0) * PI) / 180 - first_pack.c;

	i16 = (int16_t(buffer[14]) & 0x00FF) | (int16_t(buffer[15]) << 8);
	pack.p = ((i16 / 100.0) * PI) / 180;
	i16 = (int16_t(buffer[16]) & 0x00FF) | (int16_t(buffer[17]) << 8);
	pack.q = ((i16 / 100.0) * PI) / 180;
	i16 = (int16_t(buffer[18]) & 0x00FF) | (int16_t(buffer[19]) << 8);
	pack.r = ((i16 / 100.0) * PI) / 180;

	i16 = (int16_t(buffer[8]) & 0x00FF) | (int16_t(buffer[9]) << 8);
	pack.acx = - (i16 / 1000.0);
	i16 = (int16_t(buffer[10]) & 0x00FF) | (int16_t(buffer[11]) << 8);
	pack.acy = - (i16 / 1000.0);
	i16 = (int16_t(buffer[12]) & 0x00FF) | (int16_t(buffer[13]) << 8);
	pack.acz = - (i16 / 1000.0);

	i16 = (int16_t(buffer[20]) & 0x00FF) | (int16_t(buffer[21]) << 8);
	pack.ail = (i16 - 1520) / 417.0;
	i16 = (int16_t(buffer[22]) & 0x00FF) | (int16_t(buffer[23]) << 8);
	pack.ele = (i16 - 1520) / 417.0;
	i16 = (int16_t(buffer[24]) & 0x00FF) | (int16_t(buffer[25]) << 8);
	pack.thr = (i16 - 1520) / 417.0;
	i16 = (int16_t(buffer[26]) & 0x00FF) | (int16_t(buffer[27]) << 8);
	pack.rud = (i16 - 1520) / 417.0;
	i16 = (int16_t(buffer[28]) & 0x00FF) | (int16_t(buffer[29]) << 8);
	pack.tog = (i16 - 1520) / 417.0;
}

