/*
 * vision.cpp
 *
 *  Created on: Dec 13, 2012
 *      Author: nusuav
 */

#include <stdio.h>
#include <math.h>
#include <iostream>
using namespace std;

#include "uav.h"
#include "vision.h"
#include "opencv2/opencv.hpp"
using namespace cv;


#ifndef BOOL
#define BOOL	int
#define TRUE	1
#define FALSE	0
#endif

double Ts=0.02; // INS
double Tv = VISION_INTERVAL; // vision
double g = _gravity;
Mat e1=( Mat_<double>(3,1) << 1,0,0);
Mat e2=( Mat_<double>(3,1) << 0,1,0);
Mat e3=( Mat_<double>(3,1) << 0,0,1);

Mat I_3x3 = Mat::eye(3,3,CV_64FC1);
Mat I_15x15=Mat::eye(15,15, CV_64FC1);
Mat O_3x3 = Mat::zeros(3,3,CV_64FC1);
Mat O_3x1 = Mat::zeros(3,1,CV_64FC1);

/*
 * Functions implemented by Shiyu
 */

clsVision::clsVision()
{
	m_bDetectStatus = FALSE;
	m_bFirst = TRUE;
}

clsVision::~clsVision()
{

}

void clsVision::Init(const UAVSTATE uavstate)
{
/*	Kalman_A.Reset(6, 6, (double *)_Kalman_A, TRUE);
	_parser.GetVariable("_Kalman_A", Kalman_A);

	Kalman_B.Reset(6, 3, (double *)_Kalman_B, TRUE);
	_parser.GetVariable("_Kalman_B", Kalman_B);

	Kalman_C.Reset(3, 6, (double *)_Kalman_C, TRUE);
	_parser.GetVariable("_Kalman_C", Kalman_C);

	Kalman_D.Reset(3, 3, (double *)_Kalman_D, TRUE);
	_parser.GetVariable("_Kalman_D", Kalman_D);

	Kalman_Q.Reset(3, 3, (double *)_Kalman_Q, TRUE);
	_parser.GetVariable("_Kalman_Q", Kalman_Q);

	Kalman_R.Reset(3, 3, (double *)_Kalman_R, TRUE);
	_parser.GetVariable("_Kalman_R", Kalman_R);

	BQBt.Reset(6, 6, (double *)_BQBt, TRUE);
	_parser.GetVariable("_BQBt", BQBt);*/

	m_scalar_zero = 0;
	m_Mat_sigma_wa = 10*0.1*Mat::eye(3,3,CV_64FC1); // acceleration
	m_Mat_sigma_wg = 10*0.004*Mat::eye(3,3,CV_64FC1); // angular rate
	m_Mat_Q_cov = Mat::zeros(6,6,CV_64FC1);
	m_Mat_Q_cov.at<double>(0,0) = pow(m_Mat_sigma_wa.at<double>(0,0),2);
	m_Mat_Q_cov.at<double>(1,1) = pow(m_Mat_sigma_wa.at<double>(1,1),2);
	m_Mat_Q_cov.at<double>(2,2) = pow(m_Mat_sigma_wa.at<double>(2,2),2);
	m_Mat_Q_cov.at<double>(3,3) = pow(m_Mat_sigma_wg.at<double>(0,0),2);
	m_Mat_Q_cov.at<double>(4,4) = pow(m_Mat_sigma_wg.at<double>(1,1),2);
	m_Mat_Q_cov.at<double>(5,5) = 100*pow(m_Mat_sigma_wg.at<double>(2,2),2);

	// vision, altimeter, magnetometer
	double sigma_alt = 0.5; //4;
	double sigma_mag = 1.0*PI/180.0;
	double kk;
	m_Mat_R_cov = Mat::zeros(11,11,CV_64FC1);
	int flag_imageSize_320x240 = 0; // 1 or 0
	if (flag_imageSize_320x240==1)
	{
		kk = pow(7,2); // for vision pow(2, 2) is good;
		m_Mat_R_cov.at<double>(0,0) = kk*pow(0.001, 2);
		m_Mat_R_cov.at<double>(1,1) = kk*pow(0.002, 2);
		m_Mat_R_cov.at<double>(2,2) = kk*pow(0.01, 2);
		m_Mat_R_cov.at<double>(3,3) = kk*pow(0.005, 2);
		m_Mat_R_cov.at<double>(4,4) = kk*pow(0.001, 2);
		m_Mat_R_cov.at<double>(5,5) = kk*pow(0.01, 2);
		m_Mat_R_cov.at<double>(6,6) = kk*pow(0.01, 2);
		m_Mat_R_cov.at<double>(7,7) = kk*pow(0.01, 2);
		m_Mat_R_cov.at<double>(8,8) = kk*pow(0.05, 2);
	}
	else if (flag_imageSize_320x240==0)
	{
		kk = pow(3,2); // for vision pow(2, 2) is good;
		m_Mat_R_cov.at<double>(0,0) = kk*pow(0.02, 2);
		m_Mat_R_cov.at<double>(1,1) = kk*pow(0.02, 2);
		m_Mat_R_cov.at<double>(2,2) = kk*pow(0.02, 2);
		m_Mat_R_cov.at<double>(3,3) = kk*pow(0.02, 2);
		m_Mat_R_cov.at<double>(4,4) = kk*pow(0.025, 2);
		m_Mat_R_cov.at<double>(5,5) = kk*pow(0.017, 2);
		m_Mat_R_cov.at<double>(6,6) = kk*pow(0.035, 2);
		m_Mat_R_cov.at<double>(7,7) = kk*pow(0.03, 2);
		m_Mat_R_cov.at<double>(8,8) = kk*pow(0.063, 2);
	}

	m_Mat_R_cov.at<double>(9,9) = pow(sigma_mag, 2);
	m_Mat_R_cov.at<double>(10,10) = pow(sigma_alt, 2);

	// P_1
	m_bBias = TRUE;
	if (!m_bBias)
	{
		// no bias
		m_Mat_P_1 = Mat::eye(9,9,CV_64FC1);
		m_Mat_P_1=0.01*m_Mat_P_1;
		m_Mat_P_1.at<double>(6,6)=m_Mat_P_1.at<double>(6,6)*0.1;
		m_Mat_P_1.at<double>(7,7)=m_Mat_P_1.at<double>(7,7)*0.1;
		m_Mat_P_1.at<double>(8,8)=m_Mat_P_1.at<double>(8,8)*0.1;
		m_Mat_x_kal_1 = Mat::zeros(9,1,CV_64FC1);
	}
	else
	{
		// with bias
		m_Mat_P_1 = Mat::eye(15,15,CV_64FC1);
		m_Mat_P_1=0.01*m_Mat_P_1;
		m_Mat_P_1.at<double>(6,6)=m_Mat_P_1.at<double>(6,6)*0.1;
		m_Mat_P_1.at<double>(7,7)=m_Mat_P_1.at<double>(7,7)*0.1;
		m_Mat_P_1.at<double>(8,8)=m_Mat_P_1.at<double>(8,8)*0.1;
		double ba_x = 0.05*_gravity;
		double ba_y = 0.05*_gravity;
		double ba_z = 0.05*_gravity;
		double bg_x = 1*PI/180;
		double bg_y = 1*PI/180;
		double bg_z = 1*PI/180;
		m_Mat_P_1.at<double>(9,9) = 10*pow(ba_x,2);
		m_Mat_P_1.at<double>(10,10) = 10*pow(ba_y,2);
		m_Mat_P_1.at<double>(11,11) = 10*pow(ba_z,2);
		m_Mat_P_1.at<double>(12,12) = 10*pow(bg_x,2);
		m_Mat_P_1.at<double>(13,13) = 10*pow(bg_y,2);
		m_Mat_P_1.at<double>(14,14) = 10*pow(bg_z,2);
		m_Mat_x_kal_1 = Mat::zeros(15,1,CV_64FC1);
	}
	// P_1_novis
	m_Mat_P_1_novis = m_Mat_P_1(Range(0,8+1),Range(0,8+1));

	// initial states
	m_Mat_x_init=Mat::zeros(9,1,CV_64FC1);
	m_visionState.estx = m_Mat_x_init.at<double>(0,0)=uavstate.x;
	m_visionState.esty = m_Mat_x_init.at<double>(1,0)=uavstate.y;
	m_visionState.estz = m_Mat_x_init.at<double>(2,0)=uavstate.z;
	m_visionState.estug = m_Mat_x_init.at<double>(3,0)=uavstate.ug;
	m_visionState.estvg = m_Mat_x_init.at<double>(4,0)=uavstate.vg;
	m_visionState.estwg = m_Mat_x_init.at<double>(5,0)=uavstate.wg;
	m_visionState.esta = m_Mat_x_init.at<double>(6,0)=uavstate.a;
	m_visionState.estb = m_Mat_x_init.at<double>(7,0)=uavstate.b;
	m_visionState.estc = m_Mat_x_init.at<double>(8,0)=uavstate.c;

	m_Mat_x_kal_1.rowRange(0,8+1) = m_Mat_x_init + m_scalar_zero;
	m_Mat_x_kal_1_novis = m_Mat_x_init;

	m_Mat_a_nb_ins_1 = Mat::zeros(3,1,CV_64FC1);
	m_Mat_omg_ins_1 = Mat::zeros(3,1,CV_64FC1);

	m_Mat_H_vis = Mat(3,3,CV_64FC1);
	m_Mat_temp_H_vis_vec = Mat(1,9,CV_64FC1);
	m_Mat_singlarvalues = Mat(3,1,CV_64FC1);
	m_Mat_omg_t_sk = Mat(3,3,CV_64FC1);
	m_Mat_Delta_R_ins = Mat(3,3,CV_64FC1);

	m_Mat_x_kal_2 = Mat(m_Mat_x_kal_1.rows, 1, CV_64FC1);
	m_Mat_x_kal_2_novis = Mat(m_Mat_x_kal_1_novis.rows, 1, CV_64FC1);
	m_Mat_P_2 = Mat(m_Mat_P_1.rows, m_Mat_P_1.rows, CV_64FC1);
	m_Mat_P_2_novis = Mat(m_Mat_P_1_novis.rows, m_Mat_P_1_novis.rows, CV_64FC1);

	// for test only
	m_Mat_prevUAVState = Mat(9,1,CV_64FC1);
	m_Mat_curUAVState = Mat(9,1,CV_64FC1);
	m_Mat_prevUAVState.at<double>(0,0) = uavstate.x;
	m_Mat_prevUAVState.at<double>(0,1) = uavstate.y;
	m_Mat_prevUAVState.at<double>(0,2) = uavstate.z;
	m_Mat_prevUAVState.at<double>(0,3) = uavstate.ug;
	m_Mat_prevUAVState.at<double>(0,4) = uavstate.vg;
	m_Mat_prevUAVState.at<double>(0,5) = uavstate.wg;
	m_Mat_prevUAVState.at<double>(0,6) = uavstate.a;
	m_Mat_prevUAVState.at<double>(0,7) = uavstate.b;
	m_Mat_prevUAVState.at<double>(0,8) = uavstate.c;
}

BOOL clsVision::Kalman()
{
	return TRUE;
}

BOOL clsVision::VisionFusion(UAVSTATE &uavstate, CAMTELEINFO *pcamInfo)
{
	return TRUE;
}

BOOL clsVision::KalmanFilterWithVision(BOOL bVisionUpdate, BOOL bVisionInterval, UAVSTATE &uavstate, CAMTELEINFO &camInfo)
{
	// assign (p,q,r)
	m_Mat_omg_ins_1.at<double>(0,0) = uavstate.p;
	m_Mat_omg_ins_1.at<double>(1,0) = uavstate.q;
	m_Mat_omg_ins_1.at<double>(2,0) = uavstate.r;
	// assign 3-axis acceleration in NED frame
	m_Mat_a_nb_ins_1.at<double>(0,0) = uavstate.acx;
	m_Mat_a_nb_ins_1.at<double>(1,0) = uavstate.acy;
	m_Mat_a_nb_ins_1.at<double>(2,0) = uavstate.acz;

	double y_mag;
	double y_alt;
	//double rotate_angle;
	BOOL bVision, bMag, bAlt;
	if (bVisionUpdate && bVisionInterval) {
//		printf("[State] kalman %.3f\n", ::GetTime());
		// vision measurement
		bVision=TRUE;
		m_Mat_temp_H_vis_vec.at<double>(0,0) = camInfo.ta;
		m_Mat_temp_H_vis_vec.at<double>(0,1) = camInfo.tb;
		m_Mat_temp_H_vis_vec.at<double>(0,2) = camInfo.tc;
		m_Mat_temp_H_vis_vec.at<double>(0,3) = camInfo.tx;
		m_Mat_temp_H_vis_vec.at<double>(0,4) = camInfo.ty;
		m_Mat_temp_H_vis_vec.at<double>(0,5) = camInfo.tz;
		m_Mat_temp_H_vis_vec.at<double>(0,6) = camInfo.tmp1;
		m_Mat_temp_H_vis_vec.at<double>(0,7) = camInfo.tmp2;
		m_Mat_temp_H_vis_vec.at<double>(0,8) = camInfo.tmp3;
		m_Mat_H_vis = m_Mat_temp_H_vis_vec.reshape(1,3);
	//	SVD::compute(m_Mat_H_vis, m_Mat_singlarvalues);
	//	m_Mat_H_vis = m_Mat_H_vis/m_Mat_singlarvalues.at<double>(1,0);
		// calculate Delta_R_ins
		double rotate_angle = norm(m_Mat_omg_ins_1);
		if (rotate_angle<0.0001)
		{
			m_Mat_Delta_R_ins=I_3x3;
		}
		else
		{
			fcn_RodriguesRotation(m_Mat_omg_ins_1/rotate_angle, -rotate_angle*VISION_INTERVAL, m_Mat_Delta_R_ins);
		}
		//fcn_RodriguesRotation(m_Mat_omg_ins_1, m_Mat_Delta_R_ins);
		// other sensor measurement
		bMag = TRUE;
		y_mag = uavstate.c;

		bAlt = TRUE;
		y_alt = uavstate.z;
	}
	else
	{
		bVision=FALSE;
		bMag=FALSE;
		bAlt=FALSE;
		y_alt = uavstate.z;
		y_mag = uavstate.c;
	}

//	///////////////////////////////////////////////////////////////////////////
//	///////////////////////////////////////////////////////////////////////////
//	// input a particular value to test
//	m_Mat_H_vis = ( Mat_<double>(3,3) <<
//							0.996875, -0.000725, -0.043292,
//							0.000153, 1.000184, 0.007218,
//							-0.003976, -0.001077, 0.951674);
//	m_Mat_Delta_R_ins = ( Mat_<double>(3,3) <<
//							0.999988, -0.000645, 0.004899,
//							0.000644, 1.000000, 0.000151,
//							-0.004900, -0.000148, 0.999988);
//	y_mag=-1.185561;
//	y_alt=-3.081514;
//	m_Mat_x_kal_1 = ( Mat_<double>(15,1) <<
//							-0.397890, -6.741848, -3.276914, 0.234889, -1.337590, -0.359328, 0.024319, -0.069970, -1.185755, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000);
//	m_Mat_P_1 = ( Mat_<double>(15,15) <<
//							0.010081, -0.000000, -0.000000, 0.001325, 0.000000, -0.000000, 0.000022, -0.000009, 0.000002, 0.002150, 0.005315, -0.000284, 0.000001, -0.000000, 0.000000,
//							-0.000000, 0.010081, -0.000000, -0.000000, 0.001325, -0.000001, 0.000009, 0.000022, -0.000000, -0.005307, 0.002164, 0.000323, 0.000000, 0.000001, -0.000000,
//							-0.000000, -0.000000, 0.010080, 0.000000, -0.000001, 0.001307, -0.000001, -0.000001, 0.000000, 0.000406, 0.000142, 0.005724, -0.000000, -0.000000, 0.000000,
//							0.001325, -0.000000, 0.000000, 0.027520, 0.000000, 0.000003, 0.000729, -0.000295, 0.000055, 0.071670, 0.177157, -0.009389, 0.000067, -0.000027, 0.000001,
//							0.000000, 0.001325, -0.000001, 0.000000, 0.027514, -0.000041, 0.000294, 0.000727, 0.000001, -0.176894, 0.072131, 0.010717, 0.000027, 0.000066, -0.000003,
//							-0.000000, -0.000001, 0.001307, 0.000003, -0.000041, 0.026910, -0.000016, -0.000051, 0.000000, 0.013462, 0.004666, 0.190805, -0.000001, -0.000005, 0.000000,
//							0.000022, 0.000009, -0.000001, 0.000729, 0.000294, -0.000016, 0.001023, 0.000000, -0.000018, 0.000000, 0.000000, 0.000000, 0.000244, -0.000000, -0.000017,
//							-0.000009, 0.000022, -0.000001, -0.000295, 0.000727, -0.000051, 0.000000, 0.001022, -0.000006, 0.000000, 0.000000, 0.000000, -0.000000, 0.000244, -0.000006,
//							0.000002, -0.000000, 0.000000, 0.000055, 0.000001, 0.000000, -0.000018, -0.000006, 0.001277, 0.000000, 0.000000, 0.000000, 0.000000, 0.000006, 0.000244,
//							0.002150, -0.005307, 0.000406, 0.071670, -0.176894, 0.013462, 0.000000, 0.000000, 0.000000, 2.391699, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000,
//							0.005315, 0.002164, 0.000142, 0.177157, 0.072131, 0.004666, 0.000000, 0.000000, 0.000000, 0.000000, 2.391699, 0.000000, 0.000000, 0.000000, 0.000000,
//							-0.000284, 0.000323, 0.005724, -0.009389, 0.010717, 0.190805, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 2.391699, 0.000000, 0.000000, 0.000000,
//							0.000001, 0.000000, -0.000000, 0.000067, 0.000027, -0.000001, 0.000244, -0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.003046, 0.000000, 0.000000,
//							-0.000000, 0.000001, -0.000000, -0.000027, 0.000066, -0.000005, -0.000000, 0.000244, 0.000006, 0.000000, 0.000000, 0.000000, 0.000000, 0.003046, 0.000000,
//							0.000000, -0.000000, 0.000000, 0.000001, -0.000003, 0.000000, -0.000017, -0.000006, 0.000244, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.003046);
//	m_Mat_a_nb_ins_1= ( Mat_<double>(3,1) <<
//							-0.070862,
//							-0.325243,
//							-9.794336);
//	m_Mat_omg_ins_1= ( Mat_<double>(3,1) <<
//							0.013318,
//							-0.033759,
//							-0.004463);
//	bVision=1;
//	bMag=1;
//	bAlt=1;
//	///////////////////////////////////////////////////////////////////////////
//	///////////////////////////////////////////////////////////////////////////
//printf("\n H_ivs:");
//fcn_DispDoubleMat(m_Mat_H_vis);
//printf("\n Delta_R_ins:");
//fcn_DispDoubleMat(m_Mat_Delta_R_ins);
//printf("\n a_nb:");
//fcn_DispDoubleMat(m_Mat_a_nb_ins_1);
//printf("\n omg:");
//fcn_DispDoubleMat(m_Mat_omg_ins_1);
//printf("\n x_kal_1:");
//fcn_DispDoubleMat(m_Mat_x_kal_1);
//printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");

	fcn_KalmanFilter_withBias(bVision, m_Mat_H_vis, m_Mat_Delta_R_ins, bMag, y_mag, bAlt, y_alt, m_Mat_x_kal_1,
			m_Mat_P_1,  m_Mat_a_nb_ins_1,  m_Mat_omg_ins_1,  m_Mat_Q_cov,  m_Mat_R_cov, m_Mat_x_kal_2, m_Mat_P_2);
//	fcn_DispDoubleMat(m_Mat_x_kal_1);
	// log vision estimation state
	m_visionState.estx = m_Mat_x_kal_2.at<double>(0,0);
	m_visionState.esty = m_Mat_x_kal_2.at<double>(1,0);
	m_visionState.estz = m_Mat_x_kal_2.at<double>(2,0);
	m_visionState.estug = m_Mat_x_kal_2.at<double>(3,0);
	m_visionState.estvg = m_Mat_x_kal_2.at<double>(4,0);
	m_visionState.estwg = m_Mat_x_kal_2.at<double>(5,0);
	m_visionState.esta = m_Mat_x_kal_2.at<double>(6,0);
	m_visionState.estb = m_Mat_x_kal_2.at<double>(7,0);
	m_visionState.estc = m_Mat_x_kal_2.at<double>(8,0);

	m_visionState.bias1 = m_Mat_x_kal_2.at<double>(9,0);
	m_visionState.bias2 = m_Mat_x_kal_2.at<double>(10,0);
	m_visionState.bias3 = m_Mat_x_kal_2.at<double>(11,0);
	m_visionState.bias4 = m_Mat_x_kal_2.at<double>(12,0);
	m_visionState.bias5 = m_Mat_x_kal_2.at<double>(13,0);
	m_visionState.bias6 = m_Mat_x_kal_2.at<double>(14,0);

	m_visionState.homo11 = camInfo.ta;	m_visionState.homo12 = camInfo.tb;	m_visionState.homo13 = camInfo.tc;
	m_visionState.homo21 = camInfo.tx;	m_visionState.homo22 = camInfo.ty;	m_visionState.homo23 = camInfo.tz;
	m_visionState.homo31 = camInfo.tmp1; m_visionState.homo32 = camInfo.tmp2; m_visionState.homo33 = camInfo.tmp3;

//	fcn_DispDoubleMat(m_Mat_x_kal_2);

	m_Mat_P_1 = m_Mat_P_2;
	m_Mat_x_kal_1 = m_Mat_x_kal_2;

	// assign visionState to UAVSTATE
/*
	uavstate.x = m_Mat_x_kal_2.at<double>(0,0);
	uavstate.y = m_Mat_x_kal_2.at<double>(1,0);
	uavstate.z = m_Mat_x_kal_2.at<double>(2,0);
	uavstate.ug = m_Mat_x_kal_2.at<double>(3,0);
	uavstate.vg = m_Mat_x_kal_2.at<double>(4,0);
	uavstate.wg = m_Mat_x_kal_2.at<double>(5,0);
	uavstate.a = m_Mat_x_kal_2.at<double>(6,0);
	uavstate.b = m_Mat_x_kal_2.at<double>(7,0);
	uavstate.c = m_Mat_x_kal_2.at<double>(8,0);

*/

	return TRUE;
}

void clsVision::fcn_GetSkew(Mat x, Mat x_skew)
{
	// x: 3 by 1
	// x_skew: 3 by 3
	x_skew=Mat::zeros(3,3,CV_64FC1);
	x_skew.at<double>(0,1)=-1*x.at<double>(2,0);
	x_skew.at<double>(0,2)=x.at<double>(1,0);
	x_skew.at<double>(1,0)=x.at<double>(2,0);
	x_skew.at<double>(1,2)=-1*x.at<double>(0,0);
	x_skew.at<double>(2,0)=-1*x.at<double>(1,0);
	x_skew.at<double>(2,1)=x.at<double>(0,0);
}



void clsVision::fcn_DispDoubleMat(Mat& A)
{
	// function annotation: display the matrix A
	// Note: all matrix must be double type, i.e. CV_64F
	printf("\n");
	for (int i=0;i<A.rows; i++)
	{
		for (int j=0; j<A.cols; j++)
		{
			printf("%f  ", A.at<double>(i,j));
		}
		printf("\n");
	}
}


void clsVision::fcn_Euler2Rotation(double phi, double theta, double psi, Mat Rbln)
{
	//// function annotation: compute the rotation matrix from NED to body
	//Mat Rx=( Mat_<double>(3,3) <<
	//	1, 0, 0,
	//	0, cos(phi), sin(phi),
	//	0, -sin(phi), cos(phi));
	//Mat Ry=( Mat_<double>(3,3) <<
	//	cos(theta), 0, -sin(theta),
	//	0, 1, 0,
	//	sin(theta), 0, cos(theta));
	//Mat Rz=( Mat_<double>(3,3) <<
	//	cos(psi), sin(psi), 0,
	//	-sin(psi), cos(psi), 0,
	//	0, 0, 1);
	//Rbln=Rx*Ry*Rz;                      //Rotation matrix: object -> camera frame   or NED to body
	Rbln = ( Mat_<double>(3,3) <<
	 cos(theta)*cos(psi),                                          cos(theta)*sin(psi),                                                    -sin(theta),
	 sin(phi)*sin(theta)*cos(psi)-cos(phi)*sin(psi),     sin(phi)*sin(theta)*sin(psi)+cos(phi)*cos(psi),             sin(phi)*cos(theta),
	 cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi),    cos(phi)*sin(theta)*sin(psi)-sin(phi)*cos(psi),              cos(phi)*cos(theta) )+m_scalar_zero; // don't foget this zero! otherwise problem
	//fcn_DispDoubleMat(Rbln);
}

void clsVision::fcn_fxu(Mat x, Mat a_nb, Mat omg, Mat fxu)
{
	// function annotation: this is the nonlinear function on the right hand side of the navigation equation
	//Mat vn=x.rowRange(3, 5+1);
	double phi=x.at<double>(6, 0);
	double tht=x.at<double>(7, 0);
	double psi=x.at<double>(8, 0);
	Mat R_bln=Mat(3,3,CV_64FC1);
	fcn_Euler2Rotation(phi,tht,psi,R_bln);
	Mat R_nlb=R_bln.t();
	//Mat L_nlb=( Mat_<double>(3,3) <<
	//	1, sin(phi)*tan(tht), cos(phi)*tan(tht),
	//	0, cos(phi), -sin(phi), 0,
	//	sin(phi)/cos(tht), cos(phi)/cos(tht));
	fxu.rowRange(0, 2+1)=x.rowRange(3, 5+1)+m_scalar_zero; // have to multiply an identity matrix or add a zero matrix!!!
	fxu.rowRange(3, 5+1)=R_nlb*a_nb+g*e3;
	//fxu.rowRange(6, 8+1)=L_nlb*omg;
	fxu.rowRange(6, 8+1)=( Mat_<double>(3,3) <<
			1, sin(phi)*tan(tht), cos(phi)*tan(tht),
			0, cos(phi), -sin(phi), 0,
			sin(phi)/cos(tht), cos(phi)/cos(tht)) * omg;
}

void clsVision::fcn_GetFG_noBias(Mat x_c_1, Mat a_nb_c1, Mat omg_c1, Mat F_ext, Mat G_ext)
{
	Mat I_3x3 = Mat::eye(3,3,CV_64FC1);
	Mat O_3x3 = Mat::zeros(3,3,CV_64FC1);
	Mat O_3x1 = Mat::zeros(3,1,CV_64FC1);
	double phi_c = x_c_1.at<double>(6);
	double tht_c = x_c_1.at<double>(7);
	double psi_c = x_c_1.at<double>(8);
	Mat a_nb = a_nb_c1;
	Mat omg = omg_c1;
	Mat R_bln_c=Mat(3,3,CV_64FC1);
	fcn_Euler2Rotation(phi_c, tht_c, psi_c, R_bln_c);
	Mat R_nlb_c=R_bln_c.t();
	Mat L_nlb_c=(Mat_<double>(3,3) <<
		1,   sin(phi_c)*tan(tht_c),    cos(phi_c)*tan(tht_c),
		0,                cos(phi_c),                -sin(phi_c),
		0,   sin(phi_c)/cos(tht_c),    cos(phi_c)/cos(tht_c) );

	// derivatives of Rb/n with respect to states x
	Mat R_phi=(Mat_<double>(3,3) <<
		1,               0,                0,
		0,     cos(phi_c),     sin(phi_c),
		0,    -sin(phi_c),     cos(phi_c) );
	Mat R_tht=(Mat_<double>(3,3) <<
		cos(tht_c),               0,    -sin(tht_c),
		0,                           1,                0,
		sin(tht_c),               0,     cos(tht_c)  );
	Mat R_psi=(Mat_<double>(3,3) <<
		cos(psi_c),    sin(psi_c),                0,
		-sin(psi_c),    cos(psi_c),               0,
		0,               0,                1 );

	Mat dRphi_dphi=(Mat_<double>(3,3) <<
		0,                0,                0,
		0,    -sin(phi_c),      cos(phi_c),
		0,    -cos(phi_c),     -sin(phi_c) );
	Mat dRtht_dtht=(Mat_<double>(3,3) <<
		-sin(tht_c),                0,     -cos(tht_c),
		0,                0,                 0,
		cos(tht_c),                0,     -sin(tht_c)  );
	Mat dRpsi_dpsi=(Mat_<double>(3,3) <<
		-sin(psi_c),     cos(psi_c),                 0,
		-cos(psi_c),    -sin(psi_c),                 0,
		0,                0,                 0  );

	Mat dR_dphi = dRphi_dphi * R_tht      * R_psi;
	Mat dR_dtht  = R_phi      * dRtht_dtht * R_psi;
	Mat dR_dpsi = R_phi      * R_tht      * dRpsi_dpsi;

	Mat dRnb_dphi =  dR_dphi.t();
	Mat dRnb_dtht =  dR_dtht.t();
	Mat dRnb_dpsi =  dR_dpsi.t();

	// derivatives of f(x) with respect to states x
	Mat dfp_dVn = I_3x3;
	Mat dfv_dphi = dRnb_dphi*a_nb;
	Mat dfv_dtht  = dRnb_dtht*a_nb;
	Mat dfv_dpsi = dRnb_dpsi*a_nb;

	Mat dfroh_dphi_temp=(Mat_<double>(3,3) <<
		0,                    cos(phi_c)*tan(tht_c),                   -sin(phi_c)*tan(tht_c),
		0,                                -sin(phi_c),                                -cos(phi_c),
		0,                    cos(phi_c)/cos(tht_c),                   -sin(phi_c)/cos(tht_c)  );
	Mat dfroh_dphi=dfroh_dphi_temp*omg;
	Mat dfroh_dtht_temp=(Mat_<double>(3,3) <<
		0,                  sin(phi_c)/pow(cos(tht_c), 2),                  cos(phi_c)/pow(cos(tht_c), 2),
		0,                                            0,                                            0,
		0,       sin(phi_c)/cos(tht_c)*tan(tht_c),       cos(phi_c)/cos(tht_c)*tan(tht_c)  );
	Mat dfroh_dtht=dfroh_dtht_temp*omg;

	Mat dfv_da = R_nlb_c;
	Mat dfroh_domg = L_nlb_c;

	// Continuous Error Model
	Mat F(9,9,CV_64FC1);
	F(Range(0, 2+1), Range(0,2+1))=O_3x3+m_scalar_zero;
	F(Range(0, 2+1), Range(3,5+1))=dfp_dVn+m_scalar_zero;
	F(Range(0, 2+1), Range(6,6+1))=O_3x1+m_scalar_zero;
	F(Range(0, 2+1), Range(7,7+1))=O_3x1+m_scalar_zero;
	F(Range(0, 2+1), Range(8,8+1))=O_3x1+m_scalar_zero;

	F(Range(3, 5+1), Range(0,2+1)) = O_3x3+m_scalar_zero;;
	F(Range(3, 5+1), Range(3,5+1)) = O_3x3+m_scalar_zero;
	F(Range(3, 5+1), Range(6,6+1)) = dfv_dphi+m_scalar_zero;
	F(Range(3, 5+1), Range(7,7+1)) = dfv_dtht+m_scalar_zero;
	F(Range(3, 5+1), Range(8,8+1)) = dfv_dpsi+m_scalar_zero;

	F(Range(6, 8+1), Range(0,2+1)) = O_3x3+m_scalar_zero;;
	F(Range(6, 8+1), Range(3,5+1)) = O_3x3+m_scalar_zero;
	F(Range(6, 8+1), Range(6,6+1)) = dfroh_dphi+m_scalar_zero;
	F(Range(6, 8+1), Range(7,7+1)) = dfroh_dtht+m_scalar_zero;
	F(Range(6, 8+1), Range(8,8+1)) = O_3x1+m_scalar_zero;

	Mat G=Mat(9,6,CV_64FC1);
	G(Range(0, 2+1), Range(0,2+1)) = O_3x3+m_scalar_zero;
	G(Range(0, 2+1), Range(3,5+1)) = O_3x3+m_scalar_zero;
	G(Range(3, 5+1), Range(0,2+1)) = dfv_da+m_scalar_zero;
	G(Range(3, 5+1), Range(3,5+1)) = O_3x3+m_scalar_zero;
	G(Range(6, 8+1), Range(0,2+1)) = O_3x3+m_scalar_zero;
	G(Range(6, 8+1), Range(3,5+1)) = dfroh_domg+m_scalar_zero;

	// without bias estimation
	F.copyTo(F_ext);
	G.copyTo(G_ext);

	//% with bias estimate
	//% F_ext        =  [  F                         G;
	//%                 zeros(3,9)        zeros(3,6);
	//%                 zeros(3,9)        zeros(3,6)]; % F_ext: 15x15
	//%
	//% G_ext        =  [G;
	//%                         zeros(6,6)]; % 15x6
}

void clsVision::fcn_GetFG_withBias(Mat x_c_1, Mat a_nb, Mat omg, Mat F_ext, Mat G_ext)
{
	double phi_c = x_c_1.at<double>(6);
	double tht_c = x_c_1.at<double>(7);
	double psi_c = x_c_1.at<double>(8);
	//Mat a_nb = a_nb_c1;
	//Mat omg = omg_c1;
	Mat R_bln_c=Mat(3,3,CV_64FC1);
	fcn_Euler2Rotation(phi_c, tht_c, psi_c, R_bln_c);
	Mat R_nlb_c=R_bln_c.t();
	Mat L_nlb_c=(Mat_<double>(3,3) <<
		1,   sin(phi_c)*tan(tht_c),    cos(phi_c)*tan(tht_c),
		0,                cos(phi_c),                -sin(phi_c),
		0,   sin(phi_c)/cos(tht_c),    cos(phi_c)/cos(tht_c) );

	// derivatives of Rb/n with respect to states x
	//Mat R_phi=(Mat_<double>(3,3) <<
	//	1,               0,                0,
	//	0,     cos(phi_c),     sin(phi_c),
	//	0,    -sin(phi_c),     cos(phi_c) );
	//Mat R_tht=(Mat_<double>(3,3) <<
	//	cos(tht_c),               0,    -sin(tht_c),
	//	0,                           1,                0,
	//	sin(tht_c),               0,     cos(tht_c)  );
	//Mat R_psi=(Mat_<double>(3,3) <<
	//	cos(psi_c),    sin(psi_c),                0,
	//	-sin(psi_c),    cos(psi_c),               0,
	//	0,               0,                1 );

	//Mat dRphi_dphi=(Mat_<double>(3,3) <<
	//	0,                0,                0,
	//	0,    -sin(phi_c),      cos(phi_c),
	//	0,    -cos(phi_c),     -sin(phi_c) );
	//Mat dRtht_dtht=(Mat_<double>(3,3) <<
	//	-sin(tht_c),                0,     -cos(tht_c),
	//	0,                0,                 0,
	//	cos(tht_c),                0,     -sin(tht_c)  );
	//Mat dRpsi_dpsi=(Mat_<double>(3,3) <<
	//	-sin(psi_c),     cos(psi_c),                 0,
	//	-cos(psi_c),    -sin(psi_c),                 0,
	//	0,                0,                 0  );

	//Mat dR_dphi = dRphi_dphi * R_tht      * R_psi;
	//Mat dR_dtht  = R_phi      * dRtht_dtht * R_psi;
	//Mat dR_dpsi = R_phi      * R_tht      * dRpsi_dpsi;

	//Mat dRnb_dphi =  dR_dphi.t();
	//Mat dRnb_dtht =  dR_dtht.t();
	//Mat dRnb_dpsi =  dR_dpsi.t();
	Mat dRnb_dphi=(Mat_<double>(3,3) <<
			0,  cos(phi_c)*sin(tht_c)*cos(psi_c)+sin(phi_c)*sin(psi_c), -sin(phi_c)*sin(tht_c)*cos(psi_c)+cos(phi_c)*sin(psi_c),
			0,  cos(phi_c)*sin(tht_c)*sin(psi_c)-sin(phi_c)*cos(psi_c), -sin(phi_c)*sin(tht_c)*sin(psi_c)-cos(phi_c)*cos(psi_c),
			0,                                   cos(phi_c)*cos(tht_c),                                  -sin(phi_c)*cos(tht_c) );
	Mat dRnb_dtht=(Mat_<double>(3,3) <<
			-sin(tht_c)*cos(psi_c), sin(phi_c)*cos(tht_c)*cos(psi_c), cos(phi_c)*cos(tht_c)*cos(psi_c),
			-sin(tht_c)*sin(psi_c), sin(phi_c)*cos(tht_c)*sin(psi_c), cos(phi_c)*cos(tht_c)*sin(psi_c),
			-cos(tht_c),           -sin(phi_c)*sin(tht_c),           -cos(phi_c)*sin(tht_c) );
	Mat dRnb_dpsi =(Mat_<double>(3,3) <<
			-cos(tht_c)*sin(psi_c), -sin(phi_c)*sin(tht_c)*sin(psi_c)-cos(phi_c)*cos(psi_c), -cos(phi_c)*sin(tht_c)*sin(psi_c)+sin(phi_c)*cos(psi_c),
			cos(tht_c)*cos(psi_c),  sin(phi_c)*sin(tht_c)*cos(psi_c)-cos(phi_c)*sin(psi_c),  cos(phi_c)*sin(tht_c)*cos(psi_c)+sin(phi_c)*sin(psi_c),
			0,                                                       0,                                                       0 );
	// derivatives of f(x) with respect to states x
	//Mat dfp_dVn = I_3x3;
	//Mat dfv_dphi = dRnb_dphi*a_nb;
	//Mat dfv_dtht  = dRnb_dtht*a_nb;
	//Mat dfv_dpsi = dRnb_dpsi*a_nb;

	Mat dfroh_dphi_temp=(Mat_<double>(3,3) <<
		0,                    cos(phi_c)*tan(tht_c),                   -sin(phi_c)*tan(tht_c),
		0,                                -sin(phi_c),                                -cos(phi_c),
		0,                    cos(phi_c)/cos(tht_c),                   -sin(phi_c)/cos(tht_c)  );
	//Mat dfroh_dphi=dfroh_dphi_temp*omg;
	Mat dfroh_dtht_temp=(Mat_<double>(3,3) <<
		0,                  sin(phi_c)/pow(cos(tht_c), 2),                  cos(phi_c)/pow(cos(tht_c), 2),
		0,                                            0,                                            0,
		0,       sin(phi_c)/cos(tht_c)*tan(tht_c),       cos(phi_c)/cos(tht_c)*tan(tht_c)  );
	//Mat dfroh_dtht=dfroh_dtht_temp*omg;

	//Mat dfv_da = R_nlb_c;
	//Mat dfroh_domg = L_nlb_c;

	// Continuous Error Model
	// Mat F(9,9,CV_64FC1);
	F_ext=Mat::zeros(15,15,CV_64FC1);
	//F_ext(Range(0, 2+1), Range(0,2+1))=O_3x3+m_scalar_zero;
	F_ext(Range(0, 2+1), Range(3,5+1))=I_3x3+m_scalar_zero; //dfp_dVn+m_scalar_zero;
	//F_ext(Range(0, 2+1), Range(6,6+1))=O_3x1+m_scalar_zero;
	//F_ext(Range(0, 2+1), Range(7,7+1))=O_3x1+m_scalar_zero;
	//F_ext(Range(0, 2+1), Range(8,8+1))=O_3x1+m_scalar_zero;

	//F_ext(Range(3, 5+1), Range(0,2+1)) = O_3x3+m_scalar_zero;;
	//F_ext(Range(3, 5+1), Range(3,5+1)) = O_3x3+m_scalar_zero;
	F_ext(Range(3, 5+1), Range(6,6+1)) = dRnb_dphi*a_nb; //dfv_dphi+m_scalar_zero;
	F_ext(Range(3, 5+1), Range(7,7+1)) = dRnb_dtht*a_nb; //dfv_dtht+m_scalar_zero;
	F_ext(Range(3, 5+1), Range(8,8+1)) = dRnb_dpsi*a_nb; //dfv_dpsi+m_scalar_zero;

	//F_ext(Range(6, 8+1), Range(0,2+1)) = O_3x3+m_scalar_zero;;
	//F_ext(Range(6, 8+1), Range(3,5+1)) = O_3x3+m_scalar_zero;
	F_ext(Range(6, 8+1), Range(6,6+1)) = dfroh_dphi_temp*omg; //dfroh_dphi+m_scalar_zero;
	F_ext(Range(6, 8+1), Range(7,7+1)) = dfroh_dtht_temp*omg; //dfroh_dtht+m_scalar_zero;
	//F_ext(Range(6, 8+1), Range(8,8+1)) = O_3x1+m_scalar_zero;

	//Mat G=Mat(9,6,CV_64FC1);
	G_ext=Mat::zeros(15,6,CV_64FC1);
	//G_ext(Range(0, 2+1), Range(0,2+1)) = O_3x3+m_scalar_zero;
	//G_ext(Range(0, 2+1), Range(3,5+1)) = O_3x3+m_scalar_zero;
	G_ext(Range(3, 5+1), Range(0,2+1)) = R_nlb_c+m_scalar_zero; //dfv_da+m_scalar_zero;
	//G_ext(Range(3, 5+1), Range(3,5+1)) = O_3x3+m_scalar_zero;
	//G_ext(Range(6, 8+1), Range(0,2+1)) = O_3x3+m_scalar_zero;
	G_ext(Range(6, 8+1), Range(3,5+1)) = L_nlb_c+m_scalar_zero;//dfroh_domg+m_scalar_zero;

	//
	F_ext(Range(0,14+1), Range(9,14+1))=G_ext+m_scalar_zero;

	//// with bias estimation
	//F_ext=Mat::zeros(15,15,CV_64FC1);
	//F_ext(Range(0,8+1), Range(0,8+1))=F+m_scalar_zero;
	//F_ext(Range(0,8+1), Range(9,14+1))=G+m_scalar_zero;
	//G_ext=Mat::zeros(15,6,CV_64FC1);
	//G_ext(Range(0,8+1), Range(0,5+1))=G+m_scalar_zero;
}

void clsVision::fcn_GetC_noBias(Mat x_c_1pre2, Mat Delta_R_ins, Mat C_ext)
{
	// comments:
	// input: x_c_1pre2: 9x1 matrix, Delta_R_ins: 3x3 matrix
	// output: C_ext: 9x9 matrix (without bias estimation)

	// Get C using x_c_1pre2
	Mat pn_c_2=x_c_1pre2.rowRange(0,2+1);
	Mat vn_c_2=x_c_1pre2.rowRange(3,5+1);
	double phi_c_2=x_c_1pre2.at<double>(6,0);
	double tht_c_2=x_c_1pre2.at<double>(7,0);
	double psi_c_2=x_c_1pre2.at<double>(8,0);
	Mat R_bIn_c_2=Mat(3,3,CV_64FC1);
	fcn_Euler2Rotation(phi_c_2, tht_c_2, psi_c_2, R_bIn_c_2);
	Mat Delta_R=Delta_R_ins;
	// compute Dleta_T
	Mat Delta_T=-R_bIn_c_2*vn_c_2*Tv;
	// compute d1_c_1
	double d1_c_1= -e3.dot(pn_c_2-vn_c_2*Tv); //double(-e3.t()*(pn_c_2-vn_c_2*Tv));
	// compute N1_c_1
	Mat N1_c_1=Delta_R.t()*R_bIn_c_2.col(2);

	Mat R_phi = (Mat_<double>(3,3) <<
		1,               0,                0,
		0,     cos(phi_c_2),     sin(phi_c_2),
		0,    -sin(phi_c_2),     cos(phi_c_2));
	Mat R_tht = (Mat_<double>(3,3) <<
		cos(tht_c_2),               0,    -sin(tht_c_2),
		0,               1,                0,
		sin(tht_c_2),               0,     cos(tht_c_2));
	Mat R_psi = (Mat_<double>(3,3) <<
		cos(psi_c_2),    sin(psi_c_2),                0,
		-sin(psi_c_2),    cos(psi_c_2),                0,
		0,               0,                1);

	Mat dRphi_dphi = (Mat_<double>(3,3) <<
		0,                0,                0,
		0,    -sin(phi_c_2),      cos(phi_c_2),
		0,    -cos(phi_c_2),     -sin(phi_c_2) );
	Mat dRtht_dtht = (Mat_<double>(3,3) <<
		-sin(tht_c_2),                0,     -cos(tht_c_2),
		0,                0,                 0,
		cos(tht_c_2),                0,     -sin(tht_c_2) );
	Mat dRpsi_dpsi = (Mat_<double>(3,3) <<
		-sin(psi_c_2),     cos(psi_c_2),                 0,
		-cos(psi_c_2),    -sin(psi_c_2),                 0,
		0,                0,                 0 );

	Mat dR_dphi   =  dRphi_dphi * R_tht      * R_psi;
	Mat dR_dtht   =  R_phi      * dRtht_dtht * R_psi;
	Mat dR_dpsi   =  R_phi      * R_tht      * dRpsi_dpsi;

	//==============================================================
	// methd 1
	Mat dTdxn=Mat::zeros(3,1,CV_64FC1);
	Mat dTdyn=Mat::zeros(3,1,CV_64FC1);
	Mat dTdzn=Mat::zeros(3,1,CV_64FC1);
	Mat dTdvn=-Tv*R_bIn_c_2;
	Mat dTdu=dTdvn*e1;
	Mat dTdv=dTdvn*e2;
	Mat dTdw=dTdvn*e3;
	Mat dTdphi=-dR_dphi*vn_c_2*Tv;
	Mat dTdtht=-dR_dtht*vn_c_2*Tv;
	Mat dTdpsi=-dR_dpsi*vn_c_2*Tv;

	Mat dNdxn=Mat::zeros(3,1,CV_64FC1);
	Mat dNdyn=Mat::zeros(3,1,CV_64FC1);
	Mat dNdzn=Mat::zeros(3,1,CV_64FC1);
	Mat dNdu=Mat::zeros(3,1,CV_64FC1);
	Mat dNdv=Mat::zeros(3,1,CV_64FC1);
	Mat dNdw=Mat::zeros(3,1,CV_64FC1);
	Mat temp_for_dNdphi=(Mat_<double>(3,1) <<
		0, cos(tht_c_2)*cos(phi_c_2), -cos(tht_c_2)*sin(phi_c_2) );
	Mat dNdphi=Delta_R.t()*temp_for_dNdphi;
	Mat temp_for_dNdtht=(Mat_<double>(3,1) <<
		-cos(tht_c_2), -sin(tht_c_2)*sin(phi_c_2), -sin(tht_c_2)*cos(phi_c_2) );
	Mat dNdtht=Delta_R.t()*temp_for_dNdtht;
	Mat dNdpsi=Mat::zeros(3,1,CV_64FC1);

	Mat dHdxn  =  1/d1_c_1*(dTdxn*N1_c_1.t()+Delta_T*dNdxn.t());
	Mat dHdyn  =  1/d1_c_1*(dTdyn*N1_c_1.t()+Delta_T*dNdyn.t());
	Mat dHdzn  =  1/d1_c_1*(dTdzn*N1_c_1.t()+Delta_T*dNdzn.t())   -  1/pow(d1_c_1,2)*Delta_T*N1_c_1.t()* (-1);
	Mat dHdu   = 1/d1_c_1*(dTdu*N1_c_1.t()+Delta_T*(dNdu.t()));
	Mat dHdv   = 1/d1_c_1*(dTdv*N1_c_1.t()+Delta_T*(dNdv.t()));
	Mat dHdw   = 1/d1_c_1*(dTdw*N1_c_1.t()+Delta_T*(dNdw.t()))    -  1/pow(d1_c_1,2)*Delta_T*N1_c_1.t()* (Tv);
	Mat dHdphi = 1/d1_c_1*(dTdphi*N1_c_1.t()+Delta_T*(dNdphi.t()));
	Mat dHdtht = 1/d1_c_1*(dTdtht*N1_c_1.t()+Delta_T*(dNdtht.t()));
	Mat dHdpsi = 1/d1_c_1*(dTdpsi*N1_c_1.t()+Delta_T*(dNdpsi.t()));

	// next C= [dHdxn(:), dHdyn(:), dHdzn(:), dHdu(:), dHdv(:), dHdw(:), dHdphi(:), dHdtht(:), dHdpsi(:)];
	Mat C=Mat(9,9,CV_64FC1);
	Mat dHdxn_T=dHdxn.t();
	Mat dHdyn_T=dHdyn.t();
	Mat dHdzn_T=dHdzn.t();
	Mat dHdu_T=dHdu.t();
	Mat dHdv_T=dHdv.t();
	Mat dHdw_T=dHdw.t();
	Mat dHdphi_T=dHdphi.t();
	Mat dHdtht_T=dHdtht.t();
	Mat dHdpsi_T=dHdpsi.t();
	C.col(0)=dHdxn_T.reshape(0, 9)+m_scalar_zero;
	C.col(1)=dHdyn_T.reshape(0, 9)+m_scalar_zero;
	C.col(2)=dHdzn_T.reshape(0, 9)+m_scalar_zero;
	C.col(3)=dHdu_T.reshape(0, 9)+m_scalar_zero;
	C.col(4)=dHdv_T.reshape(0, 9)+m_scalar_zero;
	C.col(5)=dHdw_T.reshape(0, 9)+m_scalar_zero;
	C.col(6)=dHdphi_T.reshape(0, 9)+m_scalar_zero;
	C.col(7)=dHdtht_T.reshape(0, 9)+m_scalar_zero;
	C.col(8)=dHdpsi_T.reshape(0, 9)+m_scalar_zero;

	// without bias estimate
	C.copyTo(C_ext);

	// with bias estimate
	// C_ext=[C , zeros(9,6)];
	// C_ext=[C , zeros(9,6);
	//             zeros(1,8), 1, zeros(1,6);
	//             0,0,1,zeros(1,12)];
}

void clsVision::fcn_GetC_withBias(Mat x_c_1pre2, Mat Delta_R_ins, Mat C_ext)
{
	// comments:
	// input: x_c_1pre2: 9x1 matrix, Delta_R_ins: 3x3 matrix
	// output: C_ext: 9x15 matrix (with bias estimation)

	// Get C using x_c_1pre2
	Mat pn_c_2=x_c_1pre2.rowRange(0,2+1);
	Mat vn_c_2=x_c_1pre2.rowRange(3,5+1);
	double phi_c_2=x_c_1pre2.at<double>(6,0);
	double tht_c_2=x_c_1pre2.at<double>(7,0);
	double psi_c_2=x_c_1pre2.at<double>(8,0);
	Mat R_bIn_c_2=Mat(3,3,CV_64FC1);
	fcn_Euler2Rotation(phi_c_2, tht_c_2, psi_c_2, R_bIn_c_2);
	Mat Delta_R=Delta_R_ins;
	// compute Dleta_T
	Mat Delta_T=-R_bIn_c_2*vn_c_2*Tv;
	// compute d1_c_1
	double d1_c_1= -e3.dot(pn_c_2-vn_c_2*Tv); //double(-e3.t()*(pn_c_2-vn_c_2*Tv));
	// compute N1_c_1
	Mat N1_c_1=Delta_R.t()*R_bIn_c_2.col(2);

	//Mat R_phi = (Mat_<double>(3,3) <<
	//	1,               0,                0,
	//	0,     cos(phi_c_2),     sin(phi_c_2),
	//	0,    -sin(phi_c_2),     cos(phi_c_2));
	//Mat R_tht = (Mat_<double>(3,3) <<
	//	cos(tht_c_2),               0,    -sin(tht_c_2),
	//	0,               1,                0,
	//	sin(tht_c_2),               0,     cos(tht_c_2));
	//Mat R_psi = (Mat_<double>(3,3) <<
	//	cos(psi_c_2),    sin(psi_c_2),                0,
	//	-sin(psi_c_2),    cos(psi_c_2),                0,
	//	0,               0,                1);

	//Mat dRphi_dphi = (Mat_<double>(3,3) <<
	//	0,                0,                0,
	//	0,    -sin(phi_c_2),      cos(phi_c_2),
	//	0,    -cos(phi_c_2),     -sin(phi_c_2) );
	//Mat dRtht_dtht = (Mat_<double>(3,3) <<
	//	-sin(tht_c_2),                0,     -cos(tht_c_2),
	//	0,                0,                 0,
	//	cos(tht_c_2),                0,     -sin(tht_c_2) );
	//Mat dRpsi_dpsi = (Mat_<double>(3,3) <<
	//	-sin(psi_c_2),     cos(psi_c_2),                 0,
	//	-cos(psi_c_2),    -sin(psi_c_2),                 0,
	//	0,                0,                 0 );

	//Mat dR_dphi   =  dRphi_dphi * R_tht      * R_psi;
	//Mat dR_dtht   =  R_phi      * dRtht_dtht * R_psi;
	//Mat dR_dpsi   =  R_phi      * R_tht      * dRpsi_dpsi;
	Mat dR_dphi = (Mat_<double>(3,3) <<
			0,                                                                 0,                                                                 0,
			  cos(phi_c_2)*sin(tht_c_2)*cos(psi_c_2)+sin(phi_c_2)*sin(psi_c_2),  cos(phi_c_2)*sin(tht_c_2)*sin(psi_c_2)-sin(phi_c_2)*cos(psi_c_2),                                         cos(phi_c_2)*cos(tht_c_2),
			 -sin(phi_c_2)*sin(tht_c_2)*cos(psi_c_2)+cos(phi_c_2)*sin(psi_c_2), -sin(phi_c_2)*sin(tht_c_2)*sin(psi_c_2)-cos(phi_c_2)*cos(psi_c_2),                                        -sin(phi_c_2)*cos(tht_c_2) );
	Mat dR_dtht = (Mat_<double>(3,3) <<
			-sin(tht_c_2)*cos(psi_c_2),             -sin(tht_c_2)*sin(psi_c_2),                          -cos(tht_c_2),
			sin(phi_c_2)*cos(tht_c_2)*cos(psi_c_2), sin(phi_c_2)*cos(tht_c_2)*sin(psi_c_2),             -sin(phi_c_2)*sin(tht_c_2),
			cos(phi_c_2)*cos(tht_c_2)*cos(psi_c_2), cos(phi_c_2)*cos(tht_c_2)*sin(psi_c_2),             -cos(phi_c_2)*sin(tht_c_2) );
	Mat dR_dpsi = (Mat_<double>(3,3) <<
			-cos(tht_c_2)*sin(psi_c_2),                                         cos(tht_c_2)*cos(psi_c_2),                                                                 0,
			-sin(phi_c_2)*sin(tht_c_2)*sin(psi_c_2)-cos(phi_c_2)*cos(psi_c_2),  sin(phi_c_2)*sin(tht_c_2)*cos(psi_c_2)-cos(phi_c_2)*sin(psi_c_2),                                                                 0,
			-cos(phi_c_2)*sin(tht_c_2)*sin(psi_c_2)+sin(phi_c_2)*cos(psi_c_2),  cos(phi_c_2)*sin(tht_c_2)*cos(psi_c_2)+sin(phi_c_2)*sin(psi_c_2),                                                                 0 );

	//==============================================================
	// methd 1
	//Mat dTdxn=Mat::zeros(3,1,CV_64FC1);
	//Mat dTdyn=Mat::zeros(3,1,CV_64FC1);
	//Mat dTdzn=Mat::zeros(3,1,CV_64FC1);
	Mat dTdvn=-Tv*R_bIn_c_2;
	Mat dTdu=dTdvn*e1;
	Mat dTdv=dTdvn*e2;
	Mat dTdw=dTdvn*e3;
	Mat dTdphi=-dR_dphi*vn_c_2*Tv;
	Mat dTdtht=-dR_dtht*vn_c_2*Tv;
	Mat dTdpsi=-dR_dpsi*vn_c_2*Tv;

	//Mat dNdxn=Mat::zeros(3,1,CV_64FC1);
	//Mat dNdyn=Mat::zeros(3,1,CV_64FC1);
	//Mat dNdzn=Mat::zeros(3,1,CV_64FC1);
	//Mat dNdu=Mat::zeros(3,1,CV_64FC1);
	//Mat dNdv=Mat::zeros(3,1,CV_64FC1);
	//Mat dNdw=Mat::zeros(3,1,CV_64FC1);
	//Mat temp_for_dNdphi=(Mat_<double>(3,1) <<
	//	0, cos(tht_c_2)*cos(phi_c_2), -cos(tht_c_2)*sin(phi_c_2) );
	//Mat dNdphi=Delta_R.t()*temp_for_dNdphi;
	Mat dNdphi=Delta_R.t() * (Mat_<double>(3,1) << 0, cos(tht_c_2)*cos(phi_c_2), -cos(tht_c_2)*sin(phi_c_2) );
	//Mat temp_for_dNdtht=(Mat_<double>(3,1) <<
	//	-cos(tht_c_2), -sin(tht_c_2)*sin(phi_c_2), -sin(tht_c_2)*cos(phi_c_2) );
	// Mat dNdtht=Delta_R.t()*temp_for_dNdtht;
	Mat dNdtht=Delta_R.t() * (Mat_<double>(3,1) << -cos(tht_c_2), -sin(tht_c_2)*sin(phi_c_2), -sin(tht_c_2)*cos(phi_c_2) );
	//Mat dNdpsi=Mat::zeros(3,1,CV_64FC1);

	//Mat dHdxn  =  1/d1_c_1*(dTdxn*N1_c_1.t()+Delta_T*dNdxn.t());
	//Mat dHdyn  =  1/d1_c_1*(dTdyn*N1_c_1.t()+Delta_T*dNdyn.t());
	//Mat dHdzn  =  1/d1_c_1*(dTdzn*N1_c_1.t()+Delta_T*dNdzn.t())   -  1/pow(d1_c_1,2)*Delta_T*N1_c_1.t()* (-1);
	//Mat dHdu   = 1/d1_c_1*(dTdu*N1_c_1.t()+Delta_T*(dNdu.t()));
	//Mat dHdv   = 1/d1_c_1*(dTdv*N1_c_1.t()+Delta_T*(dNdv.t()));
	//Mat dHdw   = 1/d1_c_1*(dTdw*N1_c_1.t()+Delta_T*(dNdw.t()))    -  1/pow(d1_c_1,2)*Delta_T*N1_c_1.t()* (Tv);
	//Mat dHdphi = 1/d1_c_1*(dTdphi*N1_c_1.t()+Delta_T*(dNdphi.t()));
	//Mat dHdtht = 1/d1_c_1*(dTdtht*N1_c_1.t()+Delta_T*(dNdtht.t()));
	//Mat dHdpsi = 1/d1_c_1*(dTdpsi*N1_c_1.t()+Delta_T*(dNdpsi.t()));

	//Mat dHdxn  =  O_3x3; //1/d1_c_1*(dTdxn*N1_c_1.t()+Delta_T*dNdxn.t());
	//Mat dHdyn  =  O_3x3; //1/d1_c_1*(dTdyn*N1_c_1.t()+Delta_T*dNdyn.t());
	Mat dHdzn  =  -  1/pow(d1_c_1,2)*Delta_T*N1_c_1.t()* (-1);//1/d1_c_1*(dTdzn*N1_c_1.t()+Delta_T*dNdzn.t())   ;
	Mat dHdu   = 1/d1_c_1*(dTdu*N1_c_1.t());
	Mat dHdv   = 1/d1_c_1*(dTdv*N1_c_1.t());
	Mat dHdw   = 1/d1_c_1*(dTdw*N1_c_1.t())    -  1/pow(d1_c_1,2)*Delta_T*N1_c_1.t()* (Tv);
	Mat dHdphi = 1/d1_c_1*(dTdphi*N1_c_1.t()+Delta_T*(dNdphi.t()));
	Mat dHdtht = 1/d1_c_1*(dTdtht*N1_c_1.t()+Delta_T*(dNdtht.t()));
	Mat dHdpsi = 1/d1_c_1*(dTdpsi*N1_c_1.t());

	// next C= [dHdxn(:), dHdyn(:), dHdzn(:), dHdu(:), dHdv(:), dHdw(:), dHdphi(:), dHdtht(:), dHdpsi(:)];
	// Mat C=Mat(9,9,CV_64FC1);
	C_ext=Mat::zeros(9,15,CV_64FC1); // cannot O_9x15
	//dHdxn=dHdxn.t();
	//dHdyn=dHdyn.t();
	dHdzn=dHdzn.t();
	dHdu=dHdu.t();
	dHdv=dHdv.t();
	dHdw=dHdw.t();
	dHdphi=dHdphi.t();
	dHdtht=dHdtht.t();
	dHdpsi=dHdpsi.t();
	//C_ext.col(0)=dHdxn.reshape(0, 9)+m_scalar_zero; // must add the zero! verified
	//C_ext.col(1)=dHdyn.reshape(0, 9)+m_scalar_zero;
	C_ext.col(2)=dHdzn.reshape(0, 9)+m_scalar_zero;
	C_ext.col(3)=dHdu.reshape(0, 9)+m_scalar_zero;
	C_ext.col(4)=dHdv.reshape(0, 9)+m_scalar_zero;
	C_ext.col(5)=dHdw.reshape(0, 9)+m_scalar_zero;
	C_ext.col(6)=dHdphi.reshape(0, 9)+m_scalar_zero;
	C_ext.col(7)=dHdtht.reshape(0, 9)+m_scalar_zero;
	C_ext.col(8)=dHdpsi.reshape(0, 9)+m_scalar_zero;
	//Mat dHdxn_T=dHdxn.t();
	//Mat dHdyn_T=dHdyn.t();
	//Mat dHdzn_T=dHdzn.t();
	//Mat dHdu_T=dHdu.t();
	//Mat dHdv_T=dHdv.t();
	//Mat dHdw_T=dHdw.t();
	//Mat dHdphi_T=dHdphi.t();
	//Mat dHdtht_T=dHdtht.t();
	//Mat dHdpsi_T=dHdpsi.t();
	//C_ext.col(0)=dHdxn_T.reshape(0, 9)+m_scalar_zero;
	//C_ext.col(1)=dHdyn_T.reshape(0, 9)+m_scalar_zero;
	//C_ext.col(2)=dHdzn_T.reshape(0, 9)+m_scalar_zero;
	//C_ext.col(3)=dHdu_T.reshape(0, 9)+m_scalar_zero;
	//C_ext.col(4)=dHdv_T.reshape(0, 9)+m_scalar_zero;
	//C_ext.col(5)=dHdw_T.reshape(0, 9)+m_scalar_zero;
	//C_ext.col(6)=dHdphi_T.reshape(0, 9)+m_scalar_zero;
	//C_ext.col(7)=dHdtht_T.reshape(0, 9)+m_scalar_zero;
	//C_ext.col(8)=dHdpsi_T.reshape(0, 9)+m_scalar_zero;

	// with bias estimate
	//C_ext=Mat::zeros(9,15,CV_64FC1);
	//C_ext(Range(0,8+1), Range(0,8+1))=C+m_scalar_zero;
}

BOOL clsVision::fcn_CheckInput_noBias(Mat H_vis, Mat Delta_R_ins0, double y_alt,
						  Mat x_kal_1, Mat P_1, Mat a_nb_ins_1, Mat omg_ins_1, Mat Q_cov, Mat R_cov,
						  Mat x_kal_2, Mat P_2)
{
	if ( !(H_vis.size() == Size(3,3)) )
	{
		printf("Vision-Nav input error: dimension of H_vis should be 3x3 \n");
		return 0;
	}
	if ( !(Delta_R_ins0.size()==Size(3,3)))
	{
		printf("Vision-Nav input error: dimension of Delta_R_ins0 should be 3x3 \n");
		return 0;
	}
	if (y_alt>0)
	{
		printf("Vision-Nav input error: y_alt=%f should <0 \n", y_alt);
	}
	if ( !(x_kal_1.rows==9 && x_kal_1.cols==1) ) // for a vector, you need to compare the rows and cols instead of size.
	{
		printf("Vision-Nav input error: dimension of x_kal_1 should be 9x1 \n");
		return 0;
	}
	if ( !(P_1.size()==Size(9,9)))
	{
		printf("Vision-Nav input error: dimension of P_1 should be 9x9 \n");
		return 0;
	}
	if ( !(a_nb_ins_1.rows==3 && a_nb_ins_1.cols==1))
	{
		printf("Vision-Nav input error: dimension of a_nb_ins_1 should be 3x1 \n");
		return 0;
	}
	if ( !(omg_ins_1.rows==3 && omg_ins_1.cols==1))
	{
		printf("Vision-Nav input error: dimension of omg_ins_1 should be 3x1 \n");
		return 0;
	}
	if ( !(Q_cov.size()==Size(6,6)))
	{
		printf("Vision-Nav input error: dimension of Q_cov should be 6x6 \n");
		return 0;
	}
	if ( !(R_cov.size()==Size(11,11)))
	{
		printf("Vision-Nav input error: dimension of R_cov should be 11x11 \n");
		return 0;
	}
	if ( !(x_kal_2.rows==9 && x_kal_2.cols==1))
	{
		printf("Vision-Nav input error: dimension of x_kal_2 should be 9x1 \n");
		return 0;
	}
	if ( !(P_2.size()==Size(9,9)))
	{
		printf("Vision-Nav input error: dimension of P_2 should be 9x9 \n");
		return 0;
	}

	// if everyting is all right
	return 1;
}

BOOL clsVision::fcn_CheckInput_withBias(Mat H_vis, Mat Delta_R_ins0, double y_alt,
							Mat x_kal_1, Mat P_1, Mat a_nb_ins_1, Mat omg_ins_1, Mat Q_cov, Mat R_cov,
							Mat x_kal_2, Mat P_2)
{
	if ( !(H_vis.size() == Size(3,3)) )
	{
		printf("Vision-Nav input error: dimension of H_vis should be 3x3 \n");
		return 0;
	}
	if ( !(Delta_R_ins0.size()==Size(3,3)))
	{
		printf("Vision-Nav input error: dimension of Delta_R_ins0 should be 3x3 \n");
		return 0;
	}
	if (y_alt>0)
	{
		printf("Vision-Nav input error: y_alt=%f should <0 \n", y_alt);
	}
	if ( !(x_kal_1.rows==15 && x_kal_1.cols==1) ) // for a vector, you need to compare the rows and cols instead of size.
	{
		printf("Vision-Nav input error: dimension of x_kal_1 should be 9x1 \n");
		return 0;
	}
	if ( !(P_1.size()==Size(15,15)))
	{
		printf("Vision-Nav input error: dimension of P_1 should be 9x9 \n");
		return 0;
	}
	if ( !(a_nb_ins_1.rows==3 && a_nb_ins_1.cols==1))
	{
		printf("Vision-Nav input error: dimension of a_nb_ins_1 should be 3x1 \n");
		return 0;
	}
	if ( !(omg_ins_1.rows==3 && omg_ins_1.cols==1))
	{
		printf("Vision-Nav input error: dimension of omg_ins_1 should be 3x1 \n");
		return 0;
	}
	if ( !(Q_cov.size()==Size(6,6)))
	{
		printf("Vision-Nav input error: dimension of Q_cov should be 6x6 \n");
		return 0;
	}
	if ( !(R_cov.size()==Size(11,11)))
	{
		printf("Vision-Nav input error: dimension of R_cov should be 11x11 \n");
		return 0;
	}
	if ( !(x_kal_2.rows==15 && x_kal_2.cols==1))
	{
		printf("Vision-Nav input error: dimension of x_kal_2 should be 9x1 \n");
		return 0;
	}
	if ( !(P_2.size()==Size(15,15)))
	{
		printf("Vision-Nav input error: dimension of P_2 should be 9x9 \n");
		return 0;
	}

	// if everyting is all right
	return 1;
}

BOOL clsVision::fcn_KalmanFilter_noBias(int bVision, Mat H_vis, Mat Delta_R_ins0, int bMag, double y_mag, int bAlt, double y_alt,
							Mat x_kal_1, Mat P_1, Mat a_nb_ins_1, Mat omg_ins_1, Mat Q_cov, Mat R_cov,
							Mat &x_kal_2, Mat &P_2)
{
	// input: H_vis: 3x3; y_alt<0; x_kal_1: 9x1; P_1: 9x9; a_nb_ins_1: 3x1; omg_ins_1: 3x1; Q_cov: 6x6; R_cov: 11x11;
	// output: x_kal_2: 9x1; P_2: 9x9
	// return value: 0 error, 1 success
	// Because this function is used by users, it is necessary to check the dimension of the input
	// check the input
	if ( ! fcn_CheckInput_noBias(H_vis, Delta_R_ins0, y_alt,	x_kal_1, P_1, a_nb_ins_1, omg_ins_1, Q_cov, R_cov, x_kal_2, P_2) )
	{
		return FALSE;
	}

	// (1) Predict x_kal_1pre2 ====================================
	// f_xc1~~~
	Mat x_c_1=x_kal_1.rowRange(0,8+1);
	Mat a_nb_c1=a_nb_ins_1; //%+[-1.5, -2, 0]';% + bias_1(1:3); % should remove bias??????????
	Mat omg_c1 =omg_ins_1; //% + bias_1(4:6);
	Mat f_xc1=Mat(9,1,CV_64FC1);
	fcn_fxu(x_c_1,a_nb_c1,omg_c1, f_xc1); // compute nonlinear navigation formula
	Mat x_kal_1pre2=x_kal_1+Ts*(f_xc1);

	// (2) Predict P_1pre2 ====================================
	// compute F, G
	Mat F=Mat(9,9,CV_64FC1);
	Mat G=Mat(9,6,CV_64FC1);
	fcn_GetFG_noBias(x_c_1, a_nb_c1, omg_c1, F, G);
	// discretize F, G
	Mat A_k_1 = Mat::eye(9,9,CV_64FC1) + Ts*F + 1/2*pow(Ts,2)*F*F; // or use expm(F*Ts)
	Mat B_k_1 = ( ( Ts*Mat::eye(9,9,CV_64FC1) ) + Ts*Ts/2*F ) * G;
	Mat P_1pre2=A_k_1*P_1*A_k_1.t()+B_k_1*Q_cov*B_k_1.t();

	//====================================
	// if there is no meausrement at all, then the filtering (data fusion) is actually a
	// dead reckoning!!!
	if (bVision==0 && bMag==0 && bAlt==0)
	{
		x_kal_2=x_kal_1pre2;
		P_2=P_1pre2;
		return TRUE; // no need to do the following
	}

	// (3) Kalman Gain ====================================
	// we assume the vision, altimeter, magnetometer are available at the same time
	if (bVision==1 && bMag==1 && bAlt==1)
	{
		// the vision part
		Mat x_c_1pre2=x_kal_1pre2;
		Mat Delta_R_ins=Delta_R_ins0;
		Mat C_vis=Mat(9,9,CV_64FC1);
		fcn_GetC_noBias(x_c_1pre2, Delta_R_ins, C_vis);
		// the Mag part
		Mat C_mag=Mat::zeros(1,9,CV_64FC1);
		C_mag.at<double>(0,8)=1;
		// the Alt part
		Mat C_alt=Mat::zeros(1,9,CV_64FC1);
		C_alt.at<double>(0,2)=1;

		Mat C_k=Mat(11,9,CV_64FC1);
		C_k.rowRange(0,8+1)=C_vis+m_scalar_zero;
		C_k.row(9)=C_mag+m_scalar_zero;
		C_k.row(10)=C_alt+m_scalar_zero;

		Mat R_k=R_cov;

		// Compute Kalman gain
		// K_k=P_1pre2*C_k'/(C_k*P_1pre2*C_k'+R_k);
		Mat temp_for_K_k=(C_k*P_1pre2*C_k.t()+R_k);
		Mat K_k=P_1pre2*C_k.t()*temp_for_K_k.inv(DECOMP_SVD); // DECOMP_LU DECOMP_CHOLESKY DECOMP_SVD
		//fcn_DispDoubleMat(K_k.colRange(0,5+1));
		//fcn_DispDoubleMat(K_k.colRange(6,10+1));
		// (4) Correct current state x_kal_2 ====================================
		// compute y_k
		Mat y_k=Mat(11,1,CV_64FC1);
		Mat H_vis_T=H_vis.t();
		y_k.rowRange(0,8+1)=H_vis_T.reshape(0,9)+m_scalar_zero;
		y_k.at<double>(9,0)=y_mag;
		y_k.at<double>(10,0)=y_alt;

		// compute y_1pre2
		// computing H using x_c_1pre2
		Mat pn_2=x_c_1pre2.rowRange(0,2+1);
		Mat vn_2=x_c_1pre2.rowRange(3,5+1);
		double phi_2=x_c_1pre2.at<double>(6,0);
		double tht_2=x_c_1pre2.at<double>(7,0);
		double psi_2=x_c_1pre2.at<double>(8,0);
		Mat R_bln2=Mat(3,3,CV_64FC1);
		fcn_Euler2Rotation(phi_2, tht_2, psi_2, R_bln2);
		// compute Delta_R from x_ins
		Mat R=Delta_R_ins;
		Mat T= -R_bln2*vn_2*Tv;
		Mat N=R.t()*R_bln2.col(2);
		double d=-e3.dot(pn_2-vn_2*Tv);
		Mat H_1pre2 = R+1/d*T*N.t();

		Mat y_1pre2=Mat(11,1,CV_64FC1);
		Mat H_1pre2_T=H_1pre2.t();
		y_1pre2.rowRange(0,8+1)=H_1pre2_T.reshape(0,9)+m_scalar_zero;
		y_1pre2.at<double>(9,0)=x_kal_1pre2.at<double>(8,0);
		y_1pre2.at<double>(10,0)=x_kal_1pre2.at<double>(2,0);

		// innovation filtering !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				int bSpike = fcn_Innovation_Filter(y_k.rowRange(0,8+1), y_1pre2.rowRange(0,8+1), temp_for_K_k);
//				if (bSpike==1)
//				{
//					printf("Dectect spike!!!!!!!!!!");
//					x_kal_2=x_kal_1pre2;
//					P_2=P_1pre2;
//					return 1; // no need to do the following
//				}
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		// correct estimation
		x_kal_2=x_kal_1pre2+K_k*(y_k - y_1pre2);

		// (5) Current P_2 ====================================
		P_2=(Mat::eye(9,9,CV_64FC1)-K_k*C_k)*P_1pre2;

		return TRUE;
	}

	//////////////////////////////////////////////////////////////////////////
	// if only alt+mag, no vision measurement
	if (bVision==0 && bMag==1 && bAlt==1)
	{
		// the vision part
		// do nothing
		// the Mag part
		Mat C_mag=Mat::zeros(1,9,CV_64FC1);
		C_mag.at<double>(0,8)=1;
		// the Alt part
		Mat C_alt=Mat::zeros(1,9,CV_64FC1);
		C_alt.at<double>(0,2)=1;

		Mat C_k=Mat(2,9,CV_64FC1);
		C_k.row(0)=C_mag+m_scalar_zero;
		C_k.row(1)=C_alt+m_scalar_zero;

		Mat R_k=R_cov(Range(9,10+1),Range(9,10+1));

		// Compute Kalman gain
		// K_k=P_1pre2*C_k'/(C_k*P_1pre2*C_k'+R_k);
		Mat temp_for_K_k=(C_k*P_1pre2*C_k.t()+R_k);
		Mat K_k=P_1pre2*C_k.t()*temp_for_K_k.inv(DECOMP_SVD); // DECOMP_LU DECOMP_CHOLESKY DECOMP_SVD
		// (4) Correct current state x_kal_2 ====================================
		// compute y_k
		Mat y_k=Mat(2,1,CV_64FC1);
		y_k.at<double>(0,0)=y_mag;
		y_k.at<double>(1,0)=y_alt;

		// compute y_1pre2
		Mat y_1pre2=Mat(2,1,CV_64FC1);
		y_1pre2.at<double>(0,0)=x_kal_1pre2.at<double>(8,0);
		y_1pre2.at<double>(1,0)=x_kal_1pre2.at<double>(2,0);

		// correct estimation
		x_kal_2=x_kal_1pre2+K_k*(y_k - y_1pre2);

		// (5) Current P_2 ====================================
		P_2=(Mat::eye(9,9,CV_64FC1)-K_k*C_k)*P_1pre2;

		return TRUE;
	}

	// if neither all=1 or all=0, then error
	return FALSE;
}

BOOL clsVision::fcn_KalmanFilter_withBias(int bVision, Mat H_vis, Mat Delta_R_ins0, int bMag, double y_mag, int bAlt, double y_alt,
							  Mat x_kal_1, Mat P_1, Mat a_nb_ins_1, Mat omg_ins_1, Mat Q_cov, Mat R_cov,
							  Mat &x_kal_2, Mat &P_2)
{
	// input: H_vis: 3x3; y_alt<0; x_kal_1: 15x1; P_1: 15x15; a_nb_ins_1: 3x1; omg_ins_1: 3x1; Q_cov: 6x6; R_cov: 11x11;
	// output: x_kal_2: 9x1; P_2: 15x15
	// return value: 0 error, 1 success
	// Because this function is used by users, it is necessary to check the dimension of the input
	// check the input
	//double t_start=(double)getTickCount();
	//double t_beforevis0=(double)getTickCount();
	//if ( ! fcn_CheckInput_withBias(H_vis, Delta_R_ins0, y_alt,	x_kal_1, P_1, a_nb_ins_1, omg_ins_1, Q_cov, R_cov, x_kal_2, P_2) )
	//{
	//	return 0;
	//}
	if (y_alt>0)
	{
		printf("Vision-Nav input error: y_alt should <0 \n");
		return 0;
	}
	// (1) Predict x_kal_1pre2 ====================================
	// f_xc1~~~
	//t0=(double)getTickCount();
	Mat x_c_1=x_kal_1.rowRange(0,8+1);
	Mat bias_1=x_kal_1.rowRange(9,14+1);
	Mat a_nb_c1=a_nb_ins_1+bias_1.rowRange(0,2+1);
	Mat omg_c1 =omg_ins_1+bias_1.rowRange(3,5+1);
	Mat f_xc1=Mat(9,1,CV_64FC1);
	fcn_fxu(x_c_1,a_nb_c1,omg_c1, f_xc1); // compute nonlinear navigation formula
	//t1=(double)getTickCount();
	//printf("fcn_fxu time: %g s\n", (t1-t0)/getTickFrequency());
	Mat x_kal_1pre2=Mat(15,1,CV_64FC1);
	x_kal_1pre2.rowRange(0,8+1)=x_kal_1+Ts*(f_xc1);
	x_kal_1pre2.rowRange(9,14+1)=x_kal_1.rowRange(9,14+1)+m_scalar_zero;

	// (2) Predict P_1pre2 ====================================
	// compute F, G
	//t0=(double)getTickCount();
	Mat F=Mat(15,15,CV_64FC1);
	Mat G=Mat(15,6,CV_64FC1);
	fcn_GetFG_withBias(x_c_1, a_nb_c1, omg_c1, F, G);
	//t1=(double)getTickCount();
	//printf("fcn_GetFG time: %g s\n", (t1-t0)/getTickFrequency());
	// discretize F, G
	//t0=(double)getTickCount();
	Mat A_k_1 = I_15x15+Ts*F; //Mat::eye(F.rows, F.cols, CV_64FC1) + Ts*F;// + 1/2*pow(Ts,2)*F*F; // or use expm(F*Ts)
	Mat B_k_1 = Ts*G; //( ( Ts*Mat::eye(F.rows, F.cols, CV_64FC1) ) + Ts*Ts/2*F ) * G;
	Mat P_1pre2=A_k_1*P_1*A_k_1.t()+B_k_1*Q_cov*B_k_1.t();

	//t1=(double)getTickCount();
	//printf("Compute A, B, P time: %g s\n", (t1-t0)/getTickFrequency());
	//double t_beforevision1=(double)getTickCount();
	//printf("Before vision time: %g s\n", (t_beforevision1-t_beforevis0)/getTickFrequency());
	//printf("time before vision: %f \n", (double)(t_beforeIf-t0)/CLOCKS_PER_SEC * 1000);
	//====================================
	// if there is no meausrement at all, then the filtering (data fusion) is actually a
	// dead reckoning!!!
	if (bVision==0 && bMag==0 && bAlt==0)
	{
		x_kal_2=x_kal_1pre2;
		P_2=P_1pre2;
		return 1; // no need to do the following
	}

	// (3) Kalman Gain ====================================
	// we assume the vision, altimeter, magnetometer are available at the same time
	if (bVision==1 && bMag==1 && bAlt==1)
	{
		//double t_vis0=(double)getTickCount();
		// compute C_k
		// the vision part
		Mat x_c_1pre2=x_kal_1pre2;
		Mat Delta_R_ins=Delta_R_ins0;
		Mat C_vis=Mat(9,15,CV_64FC1);
		fcn_GetC_withBias(x_c_1pre2, Delta_R_ins, C_vis);
		// the Mag part
		Mat C_mag=Mat::zeros(1,15,CV_64FC1);
		C_mag.at<double>(0,8)=1;
		// the Alt part
		Mat C_alt=Mat::zeros(1,15,CV_64FC1);
		C_alt.at<double>(0,2)=1;

		Mat C_k=Mat(11,15,CV_64FC1);
		C_k.rowRange(0,8+1)=C_vis+m_scalar_zero;
		C_k.row(9)=C_mag+m_scalar_zero;
		C_k.row(10)=C_alt+m_scalar_zero;

		Mat R_k=R_cov;
		//double t_vis1=(double)getTickCount();
		//printf("Before inverse time: %g s\n", (t_vis1-t_vis0)/getTickFrequency());
		//////////////////////////////////////////////////////////////////////////
		// Compute Kalman gain
		// K_k=P_1pre2*C_k'/(C_k*P_1pre2*C_k'+R_k);
		//t0=(double)getTickCount();
		Mat temp_for_K_k=(C_k*P_1pre2*C_k.t()+R_k);
		Mat K_k=P_1pre2*C_k.t()*temp_for_K_k.inv(DECOMP_CHOLESKY); // DECOMP_LU DECOMP_CHOLESKY DECOMP_SVD
		//t1=(double)getTickCount();
		//printf("Inverse time: %g s\n", (t1-t0)/getTickFrequency());
		//fcn_DispDoubleMat(K_k.colRange(0,5+1));
		//fcn_DispDoubleMat(K_k.colRange(6,10+1));
		// (4) Correct current state x_kal_2 ====================================
		// compute y_k
		//t0=(double)getTickCount();
		Mat y_k=Mat(11,1,CV_64FC1);
		Mat H_vis_T=H_vis.t();
		y_k.rowRange(0,8+1)=H_vis_T.reshape(0,9)+m_scalar_zero;
		y_k.at<double>(9,0)=y_mag;
		y_k.at<double>(10,0)=y_alt;

		// compute y_1pre2
		// computing H using x_c_1pre2
		Mat pn_2=x_c_1pre2.rowRange(0,2+1);
		Mat vn_2=x_c_1pre2.rowRange(3,5+1);
		double phi_2=x_c_1pre2.at<double>(6,0);
		double tht_2=x_c_1pre2.at<double>(7,0);
		double psi_2=x_c_1pre2.at<double>(8,0);
		Mat R_bln2=Mat(3,3,CV_64FC1);
		fcn_Euler2Rotation(phi_2, tht_2, psi_2, R_bln2);
		// compute Delta_R from x_ins
		//Mat R=Delta_R_ins;
		Mat T= -R_bln2*vn_2*Tv;
		Mat N=Delta_R_ins.t()*R_bln2.col(2);
		double d=-e3.dot(pn_2-vn_2*Tv);
		Mat H_1pre2 = Delta_R_ins+1/d*T*N.t();
		//
		Mat y_1pre2=Mat(11,1,CV_64FC1);
		Mat H_1pre2_T=H_1pre2.t();
		y_1pre2.rowRange(0,8+1)=H_1pre2_T.reshape(0,9)+m_scalar_zero;
		y_1pre2.at<double>(9,0)=x_kal_1pre2.at<double>(8,0);
		y_1pre2.at<double>(10,0)=x_kal_1pre2.at<double>(2,0);

		// innovation filtering !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//t0=(double)getTickCount();
		int bSpike = fcn_Innovation_Filter(y_k.rowRange(0,8+1), y_1pre2.rowRange(0,8+1), temp_for_K_k);
		if (bSpike==1)
		{
			printf("Dectect spike!!!!!!!!!!$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
			x_kal_2=x_kal_1pre2;
			P_2=P_1pre2;
			return 2; // no need to do the following
		}
		//t1=(double)getTickCount();
		//printf("Innovation filter: %g s\n", (t1-t0)/getTickFrequency());
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		// correct estimation
		x_kal_2=x_kal_1pre2+K_k*(y_k - y_1pre2);

		// (5) Current P_2 ====================================
		P_2=(I_15x15 - K_k*C_k)*P_1pre2;

//		printf("\n f_xc1: \n");
//		fcn_DispDoubleMat(f_xc1);
//		printf("\n x_kal_1pre2: \n");
//		fcn_DispDoubleMat(x_kal_1pre2);
//		printf("\n A_k_1: \n");
//		fcn_DispDoubleMat(A_k_1);
//		printf("\n B_k_1: \n");
//		fcn_DispDoubleMat(B_k_1);
//		printf("\n P_1pre2: \n");
//		fcn_DispDoubleMat(P_1pre2);
//		printf("\n C_k: \n");
//		fcn_DispDoubleMat(C_k);
//		printf("\n K_k: \n");
//		fcn_DispDoubleMat(K_k);
//		printf("\n y_1pre2: \n");
//		fcn_DispDoubleMat(y_1pre2);
//		printf("\n x_kal_2: \n");
//		fcn_DispDoubleMat(x_kal_2);
		//double t1=(double)getTickCount();
		//printf("After inverse: %g s\n", (t1-t0)/getTickFrequency());
		return 1;
	}

	// if neither all=1 or all=0, then error
	return 0;
}

void clsVision::fcn_INS_DeadReckoning(Mat x_ins_1, Mat omg_ins_2, Mat a_nb_ins_2, Mat x_ins_2)
{
	Mat vn_ins_1=x_ins_1.rowRange(3,5+1);
	double phi_ins_1=x_ins_1.at<double>(6,0);
	double tht_ins_1=x_ins_1.at<double>(7,0);
	double psi_ins_1=x_ins_1.at<double>(8,0);

	Mat R_bln_ins=Mat(3,3,CV_64FC1);
	fcn_Euler2Rotation(phi_ins_1, tht_ins_1, psi_ins_1, R_bln_ins);
	Mat R_nlb_ins=R_bln_ins.t();

	Mat L_nlb_ins=( Mat_<double>(3,3) <<
		1, sin(phi_ins_1)*tan(tht_ins_1), cos(phi_ins_1)*tan(tht_ins_1),
		0, cos(phi_ins_1), -sin(phi_ins_1), 0,
		sin(phi_ins_1)/cos(tht_ins_1), cos(phi_ins_1)/cos(tht_ins_1));
	Mat f_ins=Mat(9,1,CV_64FC1);
	f_ins.rowRange(0, 2+1)=vn_ins_1+m_scalar_zero; // have to multiply an identity matrix or add a zero matrix!!!
	f_ins.rowRange(3, 5+1)=R_nlb_ins*a_nb_ins_2+g*e3;
	f_ins.rowRange(6, 8+1)=L_nlb_ins*omg_ins_2;

	x_ins_2=x_ins_1+Ts*f_ins;
}

int clsVision::fcn_Innovation_Filter(Mat y_k, Mat y_1pre2, Mat pred_cov)
{
	// input: y_k current vision measurement, 9x1
	// y_1pre2: predicted measurement, 9x1
	// pred_cov: predicted measurement covariance, 9x9
	Mat delta_y=y_k-y_1pre2;
	double norm_innov;
	for (int i=0; i<9; i++)
	{
		norm_innov = delta_y.at<double>(i,0) / sqrt(pred_cov.at<double>(i,i));
		if (abs(norm_innov) > 3) // threshold
		{
			return 1; // detect spike
		}
	}
	return 0; // no spike
}


void clsVision::fcn_GetHomoMatrixFrom1to2(Mat x1, Mat x2, Mat H)
{
	// get H from time 1 to time 2
	// here H is a function of the states at both time 1 and time 2
	//Mat pn1=x1.rowRange(0,2+1)+m_scalar_zero;
	//Mat pn2=x2.rowRange(0,2+1)+m_scalar_zero;
	double phi1=x1.at<double>(6);
	double tht1=x1.at<double>(7);
	double psi1=x1.at<double>(8);
	double phi2=x2.at<double>(6);
	double tht2=x2.at<double>(7);
	double psi2=x2.at<double>(8);

	double d= - x1.at<double>(2);

	Mat R_bln1=Mat(3,3,CV_64FC1);
	Mat R_bln2=Mat(3,3,CV_64FC1);
	fcn_Euler2Rotation(phi1, tht1, psi1, R_bln1);
	fcn_Euler2Rotation(phi2, tht2, psi2, R_bln2);
	//Mat R=R_bln2*R_bln1.t();

	//Mat T=-R_bln2*(x2.rowRange(0,2+1)-x1.rowRange(0,2+1));

	Mat N=R_bln1.col(2);

	// H=R+1/d*T*N.t();
	H=R_bln2*R_bln1.t() + 1/d*( -R_bln2*(x2.rowRange(0,2+1)-x1.rowRange(0,2+1)) )*N.t();
}

//void clsVision::fcn_RodriguesRotation(Mat omega, Mat Rotation)
//{
//	// input: omega: 3 by 1 vector with norm not 1
//	// output: Rotation: 3 by 3 rotation matrix
//	double vector_norm = norm(omega);
//	if (vector_norm<0.0001)
//	{
//		Rotation=I_3x3;
//	}
//	else
//	{
//		double theta=-vector_norm*VISION_INTERVAL;
//		Mat omega_normalized=omega/vector_norm;
//		Mat Omega_skew=Mat(3,3,CV_64FC1);
//		fcn_GetSkew(omega_normalized, Omega_skew);
//		Rotation=I_3x3+Omega_skew*sin(theta)+Omega_skew*Omega_skew*(1-cos(theta));
//	}
//}

void clsVision::fcn_RodriguesRotation(Mat omega, double theta, Mat Rotation)
{
	// input: omega: 3 by 1 vector with norm =1
	// output: Rotation: 3 by 3 rotation matrix
	Mat Omega_skew=Mat(3,3,CV_64FC1);
	fcn_GetSkew(omega, Omega_skew);
	Rotation=I_3x3+Omega_skew*sin(theta)+Omega_skew*Omega_skew*(1-cos(theta));
}
