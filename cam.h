/*
 * cam.h
 *
 *  Created on: Mar 15, 2011
 * this is the header file for camera function
 */

#ifndef CAM_H_
#define CAM_H_

#include "thread.h"
#include "uav.h"

//CAM
#define CAM_BAUDRATE    115200
#define COUNT_UAVSTATE_CAM	5
#define CAM_TARGETINFO 99
#define MASTERMIND 11

#pragma pack(push, 1)
/*struct CAMINFO {
	int type;				//type of the object detected by the camera
	double x, y, z;				//detected position of the object
	double u, v, w;				//velocity of the object
};*/

struct CAMINFO {
	double t;				//time
	double l, m, n;				//target vector
};


struct CAM_CONFIG {
	char devPort[32];
	short flag;
	int baudrate;
};

struct UAVPOSE {
	double x, y, z;				//position or (longitude, latitude, altitude)
	double u, v, w;				//velocity
	double a, b, c;				//attitude
	double p, q, r;				//rotating
	double acx, acy, acz;		// acceleration in x,y,z
	double longitude, latitude, altitude;
	double target_c;

};

struct UAVBJPOSE {
	double x, y, z;
	double a, b, c, target_c;
	int trackCircleNum;
};

#define MAX_CAMBUFFER		4096
#define DETECT_LOCK		1
struct TARGETINFO {
	short code;
	short DetectState;
	double   time;
	// pose of the target related to the world coordinate
	double ga, gb, gc;	//angles of the target
	//double gp, gq, gr;  //angular velocity of the target
    double gx, gy, gz;	//position or (longitude, latitude, altitude) of the target
	//double gu, gv, gw;	//velocity	of the target

	// pose of the target related to the body coordinate
	double ta, tb, tc;	//angles of the target
	//double tp, tq, tr;  //angular velocity of the target
    double tx, ty, tz;	//position or (longitude, latitude, altitude) of the target
	//double tu, tv, tw;	//velocity	of the target
};

struct TARGETSTATE {
	// pose of the target related to the world coordinate
	double ga, gb, gc;	//angles of the target
	//double gp, gq, gr;  //angular velocity of the target
    double gx, gy, gz;	//position or (longitude, latitude, altitude) of the target
	//double gu, gv, gw;	//velocity	of the target

	// pose of the target related to the body coordinate
	double ta, tb, tc;	//angles of the target
	//double tp, tq, tr;  //angular velocity of the target
    double tx, ty, tz;	//position or (longitude, latitude, altitude) of the target
	//double tu, tv, tw;	//velocity	of the target
};

struct CAMTELEINFO
{
	double detectState;		// detect status: true, false
	double ta, tb, tc;		// target angle
	double tx, ty, tz;		// target position w.r.t uav body frame
	double tmp1, tmp2, tmp3;
};

struct BJ_TARGET{
	int flags[10];
	double tvec[3];
};

#pragma pack(pop)

#define MAX_CAMINFO		128

class clsCAM : public clsThread {
public:
	clsCAM();
	virtual ~clsCAM();

protected:
	COMMAND m_cmd;
	pthread_mutex_t m_mtxCmd;

public:
	void PutCommand(COMMAND *pCmd);
	void GetCommand(COMMAND *pCmd);

	BOOL ProcessCommand(COMMAND *pCommand);
public:
	BOOL Init();
	virtual BOOL InitThread();
	virtual int EveryRun();
	virtual void ExitThread();

	BOOL StartInputThread(int priority);
	static void *InputThread(void *pParameter);
	void Input();

	BOOL ReadCommand(COMMAND *pCmd);
protected:
	FILE *m_pfCAM;
	int m_nsCAM;
	char m_buffer[MAX_CAMBUFFER];
	int m_nBuffer;
	int m_nCountCAMRD;
//	BOOL ReadCamInfo(CAMINFO *pInfo);

//	int SearchPackage(char *pBuffer, int len);
	CAM_CONFIG m_camConfig;
protected:
	CAMTELEINFO m_camTeleInfo;
	double m_tInfo0;
	CAMINFO m_info0;

	double m_tInfo[MAX_CAMINFO];
	CAMINFO m_info[MAX_CAMINFO];
	int m_nInfo;
	double m_tInfo2;    //latest time chosed from m_tInfo[]
	CAMINFO m_info2;    //latest info chosed from m_Info[]

	pthread_mutex_t m_mtxCAM;

	void MakeTelegraph(TELEGRAPH *pTele, short code, double time, void *pData, int nDataSize);
	void MakeCAMTeleInfo(const TARGETINFO &targetInfo, const TARGETSTATE &targetState);

protected:
	double m_tTargetState0;  //the current time
	TARGETINFO  m_targetInfo0;
	TARGETSTATE m_targetState0;  //the current state of tracking and attacking
	void Update();       //calcualte the state of target
	void Estimator();    //esimste the velocity of target in the world coordinate

protected:
	short m_DetectState;    //detect the target: true, didn't detect the target: false
	void GetInfo();

public:
	short IsDetected() { return m_DetectState; }
	void SetCamConfig(char *devPort, short size, short flag, int baudrate);

public:
	TARGETSTATE &GetState() { return m_targetState0; }
	TARGETINFO &GetTargetInfo()    { return m_targetInfo0; }  //get the latest time and info
//	double GetStateTime() { return m_tTargetState0; }
	double GetStateTime() { return m_targetInfo0.time; }
	CAMTELEINFO &GetCAMTeleInfo()	{ return m_camTeleInfo; }
	CAM_CONFIG GetCamConfig() const {return m_camConfig;}

/* added by LPD for 2013-UAVGP 2013-08-06 */
protected:
	BJ_TARGET m_targetInfo;
	BJ_TARGET m_targetInfoUpdate;
	UAVBJPOSE m_UavBJpose;
	double tStart;
	double m_releaseTargetFinalPhase;
	BOOL m_bVisionFinalPhaseFlag;

public:
	BJ_TARGET GetVisionTargetInfo();  //return relative position in SHIP frame;
	void SetReleaseFinalPhaseStatus() { m_releaseTargetFinalPhase = 1; }
	void ResetReleaseFinalPhaseStatus() { m_releaseTargetFinalPhase = 0; }

	BOOL GetVisionFinalPhase() { return m_bVisionFinalPhaseFlag; }
	void SetVisionFinalPhase() { m_bVisionFinalPhaseFlag = true; }
	void ResetVisionFinalPhase() { m_bVisionFinalPhaseFlag = false; }
	/* end of LPD's part */
};


#endif /* CAM_H_ */
