//svo.cpp
//implementation file for servo related class & functions

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
using namespace std;

#include "matrix.h"

#include "uav.h"
#include "svo.h"
#include "state.h"
#include "ctl.h"

extern clsState _state;
extern EQUILIBRIUM _equ_Hover;
extern clsIM9	_im9;

BOOL clsSVO::InitThread()
{
		m_nsSVO = open("/dev/ser2", O_RDWR | O_NONBLOCK);
	    if (m_nsSVO == -1) { printf("[SVO] Open servo serial port (/dev/ser2) failed.\n"); return FALSE; }

	    termios termSVO;
	    tcgetattr(m_nsSVO, &termSVO);

		cfsetispeed(&termSVO, SVO_BAUDRATE);				//input and output baudrate
		cfsetospeed(&termSVO, SVO_BAUDRATE);

		termSVO.c_cflag = CS8 | CLOCAL | CREAD;
	//	termSVO.c_iflag = IGNBRK | IGNCR | IGNPAR;

		tcsetattr(m_nsSVO, TCSANOW, &termSVO);
		tcflush(m_nsSVO, TCIOFLUSH);
		printf("[SVO] UAV100 Start\n");

//		usleep(10000);				//wait for 10 ms
//		WriteCommand();

		//init variables
		m_tSVO0 = -1;
		m_nSVO = 0;
		m_tTrimvalue = -1;
		m_trimT0 = -1;

//	::memcpy(&m_svo0, 0, sizeof(m_svo0));
//	::memcpy(&m_svo1, 0, sizeof(m_svo1));

	m_bManualTrimFlag = FALSE;

	m_avgTrimvalue.aileron = m_svoEqu.aileron = _equ_Hover.ea;
	m_avgTrimvalue.elevator = m_svoEqu.elevator = _equ_Hover.ee;
	m_avgTrimvalue.auxiliary =  m_svoEqu.auxiliary = _equ_Hover.eu;
	m_avgTrimvalue.rudder = m_svoEqu.rudder = _equ_Hover.er;
	m_avgTrimvalue.throttle = m_svoEqu.throttle = _equ_Hover.et;

	m_svoSet = m_svoEqu;

	m_svoManualTrimRaw.aileron = m_svoManualTrimRaw.elevator = m_svoManualTrimRaw.auxiliary = m_svoManualTrimRaw.rudder = m_svoManualTrimRaw.throttle = 15000;

	m_limit = 0.02;
	/* below for the online trimvalue updating */
	m_bTrimvalue = FALSE;
	m_nTrimCount = 0;

	printf("[SVO] Start\n");

	return TRUE;
}

void clsSVO::PutCommand(COMMAND *pCmd)
{
	pthread_mutex_lock(&m_mtxCmd);
	m_cmd = *pCmd;
	pthread_mutex_unlock(&m_mtxCmd);
}

void clsSVO::GetCommand(COMMAND *pCmd)
{
	pthread_mutex_lock(&m_mtxCmd);

	if(m_cmd.code == 0)
		pCmd->code = 0;
	else
	{
		*pCmd = m_cmd;
		m_cmd.code = 0;
	}

	pthread_mutex_unlock(&m_mtxCmd);
}

void clsSVO::SetTrimvalue()
{
	m_svoEqu.aileron = 0.5*_equ_Hover.ea + 0.5*m_avgTrimvalue.aileron;
	m_svoEqu.elevator = 0.5*_equ_Hover.ee + 0.5*m_avgTrimvalue.elevator;
	m_svoEqu.auxiliary = 0.5*_equ_Hover.eu + 0.5*m_avgTrimvalue.auxiliary;
	m_svoEqu.rudder = 0.5*_equ_Hover.er + 0.5*m_avgTrimvalue.rudder;
	m_svoEqu.throttle = 0.5*_equ_Hover.et + 0.5*m_avgTrimvalue.throttle;

	m_svoSet = m_svoEqu;	// put the servo deflections to the new trimvalues

	m_tTrimvalue = ::GetTime();
}

BOOL clsSVO::ProcessCommand(COMMAND *pCmd)
{
	COMMAND &cmd = *pCmd;
//	char *paraCmd = cmd.parameter;
	BOOL bProcess = TRUE;

	switch(cmd.code)
	{
	case COMMAND_GETTRIM:
//		m_bTrimvalue = TRUE;
		/* set all variables concering get trimvalue to zeros */
		m_nTrimCount = 0;
		m_avgTrimvalue.aileron = _equ_Hover.ea;
		m_avgTrimvalue.auxiliary = _equ_Hover.eu;
		m_avgTrimvalue.elevator = _equ_Hover.er;
		m_avgTrimvalue.rudder = _equ_Hover.er;
		m_avgTrimvalue.throttle = _equ_Hover.et;
		break;

	case COMMAND_HOLD:
		m_bTrimvalue = TRUE;
		m_trimT0 = ::GetTime();
		break;

	default:
		bProcess = FALSE;
		break;
	}
	return bProcess;
}

BOOL clsSVO::ValidateTrimvalue()
{
	UAVSTATE &state = _state.GetState();
	BOOL bValid =
		::fabs(state.u) <= THRESHOLDHIGH_U &&
		::fabs(state.v) <= THRESHOLDHIGH_V &&
		::fabs(state.w) <= THRESHOLDHIGH_W &&
		::fabs(state.p) <= THRESHOLDHIGH_P &&
		::fabs(state.q) <= THRESHOLDHIGH_Q &&
		::fabs(state.r) <= THRESHOLDHIGH_R;

	return bValid;
}

int clsSVO::EveryRun()
{
#ifndef TICSERVO
	/// get autocontrol signal from innerloop
	HELICOPTERRUDDER sig;
	_state.GetSIG(&sig);

	/// send to servo board
	int nMode = MODE_NAVIGATION; //_ctl.GetMode();
//	BOOL bThrottleBypass = _ctl.GetThrottleByPassFlag();
	BOOL bThrottleBypass = TRUE;
	if (/*nMode == MODE_SEMIAUTO &&*/ bThrottleBypass) {
		m_svoSet.aileron = sig.aileron;
		m_svoSet.elevator = sig.elevator;
		m_svoSet.auxiliary = sig.auxiliary;
		m_svoSet.rudder = sig.rudder;
		m_svoSet.throttle = sig.throttle;	// bypass throttle manual signal to servo board
	}
	else if (nMode == MODE_SEMIAUTO && !bThrottleBypass) {
		m_svoSet.aileron = sig.aileron;
		m_svoSet.elevator = sig.elevator;
		m_svoSet.auxiliary = sig.auxiliary;
		m_svoSet.rudder = sig.rudder;
		m_svoSet.throttle = sig.throttle;	// semi-auto throttle sig to servo board
	}
	else if (nMode == MODE_ALLAUTO) {
		m_svoSet.aileron = sig.aileron;
		m_svoSet.elevator = sig.elevator;
		m_svoSet.auxiliary = sig.auxiliary;
		m_svoSet.rudder = sig.rudder;
		m_svoSet.throttle = sig.throttle;
	}
	else if (nMode == MODE_NAVIGATION) {
		m_svoSet.aileron = sig.aileron;
		m_svoSet.elevator = sig.elevator;
		m_svoSet.auxiliary = sig.auxiliary;
		m_svoSet.rudder = sig.rudder;
		m_svoSet.throttle = sig.throttle;
	}
	if (m_nCount % _DEBUG_COUNT == 0) {
		printf("[SVO] auto data, ea %.3f, ee %.3f, eu %.3f, er %.3f\n",
		m_svoSet.aileron, m_svoSet.elevator, m_svoSet.auxiliary, m_svoSet.rudder);
	}

	SetRudder(&m_svoSet);

//#ifdef _CAMERA
//	double camera = _state.GetCamera();
//	SetCamera(camera);
//#endif

	/// read manual servo data requested in the last loop
	SVORAWDATA svoraw = {0};
	BOOL bGetData = GetData(&svoraw);
	/// send request for manual servo data
	if (m_nCount % 10 == 0){
		WriteCommand();
	}

	if (!bGetData) return TRUE;
	if (m_nCount % _DEBUG_COUNT == 0) {
		printf("[SVO] manual data, ea %d, ee %d, eu %d, er %d, et %d, toggle %d\n",
		svoraw.aileron, svoraw.elevator, svoraw.auxiliary, svoraw.rudder, svoraw.throttle, svoraw.sv6);
	}

	/// check manual trim value update function is to be done or not
	SVODATA svo = {0};
	if (_HELICOPTER == ID_GREMLION || _HELICOPTER == ID_QUADLION) {	// for GremLion
		BOOL bManualTrim = GetManualTrimFlag();
		SVORAWDATA &svoRawTrim = GetManualTrimRawData();
		Translate_GremLion(&svoraw, &svo, &svoRawTrim, bManualTrim);
	}
	else {	// for HeLion, SheLion
		Translate_HeLion(&svoraw, &svo);
	}

#if (_DEBUG & DEBUGFLAG_SVO)
if (m_nCount % _DEBUG_COUNT == 0) {
	printf("[SVO] manual data, ea %.3f, ee %.3f, eu %.3f, er %.3f, et %.3f\n",
	svo.aileron, svo.elevator, svo.auxiliary, svo.rudder, svo.throttle);
}
#endif

	m_tSVO0 = m_tRetrieve;
	m_svoRaw0 = svoraw;

	if (svo.aileron >= -1 && svo.aileron <= 1) {
		m_svo0.aileron = svo.aileron;
	}

	if (svo.elevator >= -1 && svo.elevator <= 1) {
		m_svo0.elevator = svo.elevator;
	}

	if (svo.auxiliary >= -1 && svo.auxiliary <= 1) {
		m_svo0.auxiliary = svo.auxiliary;
	}

	if (svo.rudder >= -1 && svo.rudder <= 1) {
		m_svo0.rudder = svo.rudder;
	}


	if (svo.throttle >= -1 && svo.throttle <= 1) {
		m_svo0.throttle = svo.throttle;
	}

	m_svo0.sv6 = svo.sv6;

	/// copy into memory servo time, raw data and translated data
	pthread_mutex_lock(&m_mtxSVO);
	if (m_nSVO != MAX_SVO) {
		m_tSVO[m_nSVO] = m_tSVO0;
		m_svoRaw[m_nSVO] = m_svoRaw0;
		m_svo[m_nSVO++] = m_svo0;
	}
	pthread_mutex_unlock(&m_mtxSVO);

#else
	// Get manual input from RC
//	SVORAWDATA svoRaw;
	SVORAWDATA_TIC svoRawTic;
	GetDataFromTICBoard(&svoRawTic);

	SVODATA svo = {0};
	TranslateTICBoard(&svoRawTic, &svo);

//	::memcpy(&m_svo0, &svo, sizeof(svo));
	m_svo0.aileron = svo.aileron;
	m_svo0.elevator = svo.elevator;
	m_svo0.auxiliary = svo.auxiliary;
	m_svo0.rudder = svo.rudder;
	m_svo0.throttle = svo.throttle;
	m_svo0.sv6 = svo.sv6;
	m_tSVO0 = ::GetTime();

/*	if (m_nCount % _DEBUG_COUNT == 0) {
		printf("[SVO] manual data, ea %.3f, ee %.3f, eu %.3f, er %.3f, et %.3f\n",
				m_svo0.aileron, m_svo0.elevator, m_svo0.auxiliary, m_svo0.rudder, m_svo0.throttle);
	}*/

	// Send auto signal
	HELICOPTERRUDDER sig;
	_state.GetSIG(&sig);
	m_svoSet.aileron = sig.aileron;
	m_svoSet.elevator = sig.elevator;
	m_svoSet.auxiliary = sig.auxiliary;
	m_svoSet.rudder = sig.rudder;
	m_svoSet.throttle = sig.throttle;

/*
	if (m_nCount % _DEBUG_COUNT == 0) {
		printf("[SVO] auto data, ea %.3f, ee %.3f, eu %.3f, er %.3f, th %.3f\n",
		m_svoSet.aileron, m_svoSet.elevator, m_svoSet.auxiliary, m_svoSet.rudder, m_svoSet.throttle);
	}
*/
	SetTICServo(&m_svoSet);

	pthread_mutex_lock(&m_mtxSVO);
	if (m_nSVO != MAX_SVO) {
		m_tSVO[m_nSVO] = ::GetTime();
		m_svoRaw[m_nSVO] = m_svoRaw0;
		m_svo[m_nSVO++] = m_svo0;
	}
	pthread_mutex_unlock(&m_mtxSVO);
#endif

	return TRUE;
}

BOOL clsSVO::CalcNewTrim()
{
	if( ValidateTrimvalue() )
	{
		m_avgTrimvalue.aileron = SVO_WEIGHT1*m_svo0.aileron + (1-SVO_WEIGHT1)*m_avgTrimvalue.aileron;
		m_avgTrimvalue.elevator = SVO_WEIGHT1*m_svo0.elevator + (1-SVO_WEIGHT1)*m_avgTrimvalue.elevator;
		m_avgTrimvalue.auxiliary = SVO_WEIGHT1*m_svo0.auxiliary + (1-SVO_WEIGHT1)*m_avgTrimvalue.auxiliary;
		m_avgTrimvalue.rudder = SVO_WEIGHT1*m_svo0.rudder + (1-SVO_WEIGHT1)*m_avgTrimvalue.rudder;
		m_avgTrimvalue.throttle = SVO_WEIGHT1*m_svo0.throttle + (1-SVO_WEIGHT1)*m_avgTrimvalue.throttle;
		return TRUE;
	}
	return FALSE;
}

void clsSVO::ExitThread()
{
	::close(m_nsSVO);
	printf("[SVO] quit\n");
}

BOOL clsSVO::GetDataFromTICBoard(SVORAWDATA_TIC *pRaw)
{
	int nRead = read(m_nsSVO, m_szSVO, 512);
	m_szSVO[nRead] = '\0';
//	printf("[SVO] TIC read bytes %d\n", nRead);

/*	if (m_nCount % 50 == 0) {
		for (int i=0; i<nRead; i++) {
			printf("%02x ", (unsigned char)m_szSVO[i]);
		}
		printf("\n");
	}*/

	for (int i=0; i<nRead; i++) {
		if ((unsigned char)m_szSVO[i] != 0xA5) {
			i++; continue;
		}

		if ((unsigned char)m_szSVO[i+1] != 0xA5) {
			i += 2; continue;
		}

		unsigned char checkSumCalc = CheckSumOneByte((m_szSVO+i), 38);
		unsigned char checkSum = (unsigned char)(m_szSVO[i+38]);
		if (checkSumCalc != checkSum) {
			i += 3; continue;
		}
		memcpy(m_szSVOADCData, m_szSVO+i+2, 36);
	}

	// parse the data fields: 12 pwm channels + 4 ADC channels
	int ch[12] = {0};

	for (int i=0; i<12; i++) {
		ch[i] = (unsigned int)((unsigned char)m_szSVOADCData[2*i] << 8 | (unsigned char)(m_szSVOADCData[2*i+1]));
//		printf("%d ", ch[i]);
	}
//	printf("\n");

	char adcDataBuf1[4] = {0};
//	m_szSVOADCData[24] = 0xc2; m_szSVOADCData[25] = 0xbf; m_szSVOADCData[26] = 0x39;
	double adc[4] = {0};
//	adcDataBuf[0] = 0x39; adcDataBuf[1] = 0xbf; adcDataBuf[2] = 0xc2; adcDataBuf[3] = 0x00;
	adcDataBuf1[0] = m_szSVOADCData[26]; adcDataBuf1[1] = m_szSVOADCData[25]; adcDataBuf1[2] = m_szSVOADCData[24]; adcDataBuf1[3] = 0x00;

	unsigned int adcReading1 = GETLONG(adcDataBuf1);
	double res = 2.5 / double((pow(2,23) - 1));
	adc[0] = adcReading1 * res;

	/* ADC channel 2 */
	char adcDataBuf2[4] = {0};
	adcDataBuf2[0] = m_szSVOADCData[29]; adcDataBuf2[1] = m_szSVOADCData[28]; adcDataBuf2[2] = m_szSVOADCData[27]; adcDataBuf2[3] = 0x00;
	unsigned int adcReading2 = GETLONG(adcDataBuf2);
	adc[1] = adcReading2 * res;

	/* ADC channel 3 */
	char adcDataBuf3[4] = {0};
	adcDataBuf3[0] = m_szSVOADCData[32]; adcDataBuf3[1] = m_szSVOADCData[31]; adcDataBuf3[2] = m_szSVOADCData[30]; adcDataBuf3[3] = 0x00;
	unsigned int adcReading3 = GETLONG(adcDataBuf3);
	adc[2] = adcReading3 * res * 20.1 / 5.1;

	/* ADC channel 4 */
	char adcDataBuf4[4] = {0};
	adcDataBuf4[0] = m_szSVOADCData[35]; adcDataBuf4[1] = m_szSVOADCData[34]; adcDataBuf4[2] = m_szSVOADCData[33]; adcDataBuf4[3] = 0x00;
	unsigned int adcReading4 = GETLONG(adcDataBuf4);
	adc[3] = adcReading4 * res * 2.0 ;
/*		for (int i=0; i<4; i++) {
			printf("%02x ", (unsigned char)adcDataBuf4[i]);
		}
		printf("\n");*/
/*	unsigned int tmpint = 12762937;
	char a[4] = {0};
	a[3] = (unsigned char)(tmpint>>24 & 0xFF);
	a[2] = (unsigned char)(tmpint>>16 & 0xFF);
	a[1] = (unsigned char)(tmpint>>8 & 0xFF);
	a[0] = (unsigned char)(tmpint & 0xFF);

	for (int i=0; i<4; i++) {
		printf("%02x ", (unsigned char)a[i]);
	}
	printf("\n");
	unsigned int tmpa = GETLONG(a);*/


/*	if (m_nCount % 50 == 0) {
		for (int i=0; i<12; i++) {
			printf("%d ", ch[i]);
		}
		for (int i=0; i<4; i++)
			printf("%.3f ", adc[i]);
		printf("\n\n");
	}*/


//	char cha = 0xfa;
//	printf("%d \n", (unsigned char)cha);

//	short int ch0 = (short int)((unsigned char)m_szSVOADCData[0] << 8 | (unsigned char)(m_szSVOADCData[1]));
//	int nScanf = sscanf(m_szSVOADCData, "%hd%hd%hd%hd%hd%hd%hd%hd%hd%hd%hd%hd", &ch[0], &ch[1], &ch[2], &ch[3], &ch[4],
//			&ch[5], &ch[6], &ch[7], &ch[8], &ch[9], &ch[10], &ch[11]);


/*	if (adcDataBuf[3] & 0x80) {
		adc[0] = adc[0] - (2^24);
	}*/
	pRaw->aileron = ch[0]; pRaw->elevator = ch[1]; pRaw->auxiliary = ch[2]; pRaw->rudder = ch[3];
	pRaw->throttle = ch[5]; pRaw->sv6 = ch[7];

	return TRUE;
}

void clsSVO::SetTICServo(HELICOPTERRUDDER *pRudder)
{
	HELICOPTERRUDDER rudder = *pRudder;
	rudder.aileron = range(rudder.aileron, -0.5, 0.5);
	rudder.elevator = range(rudder.elevator, -0.5, 0.5);
	rudder.auxiliary = range(rudder.auxiliary, -0.8, 0.8);
	rudder.rudder = range(rudder.rudder, -0.5, 0.5);
	rudder.throttle = range(rudder.throttle, -0.8, 0.8);

	unsigned int nPosition;
	char svoSentBuf[27] = {0};
	svoSentBuf[0] = 0xA5; svoSentBuf[1] = 0xA6;

	nPosition = 18000 + (int)(6000*rudder.aileron);
	svoSentBuf[2] = (nPosition>>8) & 0xFF; svoSentBuf[3] = nPosition & 0xFF;
	nPosition = 18000 + (int)(6000*rudder.elevator);
	svoSentBuf[4] = (nPosition>>8) & 0xFF; svoSentBuf[5] = nPosition & 0xFF;
	nPosition = 18000 + (int)(6000*rudder.auxiliary);
	svoSentBuf[6] = (nPosition>>8) & 0xFF; svoSentBuf[7] = nPosition & 0xFF;
	nPosition = 18000 + (int)(6000*rudder.rudder);
	svoSentBuf[8] = (nPosition>>8) & 0xFF; svoSentBuf[9] = nPosition & 0xFF;
//	nPosition = 18000 + (int)(6000*rudder.throttle);
//	nPosition = 12000 + (int)(12000*rudder.throttle);
//	svoSentBuf[10] = (nPosition>>8) & 0xFF; svoSentBuf[11] = nPosition & 0xFF;

	nPosition = 12000 + (int)(12000*rudder.throttle);
	svoSentBuf[12] = (nPosition>>8) & 0xFF; svoSentBuf[13] = nPosition & 0xFF;
	nPosition = 20000; //+(int)(5000*rudder.throttle);
	svoSentBuf[14] = (nPosition>>8) & 0xFF; svoSentBuf[15] = nPosition & 0xFF;
	nPosition = 21000; //+(int)(5000*rudder.throttle);
	svoSentBuf[16] = (nPosition>>8) & 0xFF; svoSentBuf[17] = nPosition & 0xFF;
	nPosition = 22000; //+(int)(5000*rudder.throttle);
	svoSentBuf[18] = (nPosition>>8) & 0xFF; svoSentBuf[19] = nPosition & 0xFF;
	nPosition = 15000; //+(int)(5000*rudder.throttle);
	svoSentBuf[20] = (nPosition>>8) & 0xFF; svoSentBuf[21] = nPosition & 0xFF;
	nPosition = 22800; //+(int)(5000*rudder.throttle);
	svoSentBuf[22] = (nPosition>>8) & 0xFF; svoSentBuf[23] = nPosition & 0xFF;
	nPosition = 15000; //+(int)(5000*rudder.throttle);
	svoSentBuf[24] = (nPosition>>8) & 0xFF; svoSentBuf[25] = nPosition & 0xFF;

	svoSentBuf[26] = CheckSumOneByte(svoSentBuf, 26);

/*	if (m_nCount % 50 == 0) {
		for (int i=0; i<27; i++)
			printf("%02x ", (unsigned char)svoSentBuf[i]);
		printf("\n");
	}*/


	write(m_nsSVO, svoSentBuf, 27);
//	printf("[SVO] TIC write %d\n", nWrite);
}

void clsSVO::SetRudder(HELICOPTERRUDDER *pRudder)
{
	HELICOPTERRUDDER rudder = *pRudder;

	rudder.aileron = range(rudder.aileron, -0.5, 0.5);
	rudder.elevator = range(rudder.elevator, -0.5, 0.5);
	rudder.auxiliary = range(rudder.auxiliary, -0.7, 0.3);
	rudder.rudder = range(rudder.rudder, -0.5, 0.5);
	rudder.throttle = range(rudder.throttle, 0.2, 0.8);

	char szSVOCommand[114]; int nPosition;
	nPosition = 15000+(int)(5000*rudder.aileron);
	sprintf(szSVOCommand, "SV3 M%d\r M%d\r", nPosition, nPosition);

	nPosition = 15000+(int)(5000*rudder.elevator);
	sprintf(szSVOCommand + 19, "SV4 M%d\r M%d\r", nPosition, nPosition);

	nPosition = 15000+(int)(5000*rudder.auxiliary);
	sprintf(szSVOCommand + 38, "SV5 M%d\r M%d\r", nPosition, nPosition);

	nPosition = 15000+(int)(5000*rudder.rudder);
	sprintf(szSVOCommand + 57, "SV1 M%d\r M%d\r", nPosition, nPosition);

	nPosition = 10000+(int)(10000*rudder.throttle);
	sprintf(szSVOCommand + 76, "SV0 M%d\r M%d\r", nPosition, nPosition);

/* added by LPD for 2013-UAVGP 2013-08-02
 * channel 3 for grasper control
 * svo = -0.0314 for grasp
 * svo = 0.9675 for release
 */
	if (_ctl.GetGrasperStatus()){ //true for release
		nPosition = 15000+(int)(5000*0.9675);
//		printf("release\n");
	}
	else{
		nPosition = 10000;
//		printf("Grasp\n");
	}
	sprintf(szSVOCommand + 95, "SV2 M%d\r M%d\r", nPosition, nPosition);
	write(m_nsSVO, szSVOCommand, 114);
}

void clsSVO::SetRudder_FeiLion(HELICOPTERRUDDER *pRudder)
{
	HELICOPTERRUDDER rudder_ = *pRudder;

	int nAilPos = (int)(rudder_.aileron*834);
	int nElePos = (int)(rudder_.elevator*834);
	int nThrPos = (int)(rudder_.throttle*834);
	int nRudPos	= (int)(rudder_.rudder*834);

	// Cap the control inputs to protect the servos and motors
	nAilPos = range(nAilPos, -500, 500);
	nElePos = range(nElePos, -500, 500);
	nThrPos = range(nThrPos, -500, 500);
	nRudPos	= range(nRudPos, -500, 500);

//	nAilPos = m_trim_aileron;
	nAilPos = m_trim_aileron + nAilPos;
//	nAilPos = m_trim_aileron + ::sin(0.2*PI*GetTime())*300;

//	nElePos = m_trim_elevator;
	nElePos = m_trim_elevator + nElePos;
//	nElePos = m_trim_elevator + ::sin(0.2*PI*GetTime())*300;
//	nElePos = m_trim_elevator + floor(::sin(0.5*PI*t))*700;

//	nThrPos = m_trim_throttle;
//	nThrPos = m_trim_throttle + nThrPos;
//	nThrPos = m_trim_throttle + ::sin(0.2*PI*t)*300;
//	nThrPos = 2900 + floor(::sin(0.5*PI*t))*400;
	nThrPos = 600;

	nRudPos = m_trim_rudder;
//	nRudPos = m_trim_rudder + nRudPos;
//	nRudPos = m_trim_rudder + ::sin(0.2*PI*t)*300;

	if (m_nCount % 50 == 0 )
	{
		cout<<"Ail: "<<nAilPos<<endl;
		cout<<"Ele: "<<nElePos<<endl;
		cout<<"Thr: "<<nThrPos<<endl;
		cout<<"Rud: "<<nRudPos<<endl;
		cout<<endl;
	}

	// Send to servo controller
    char ail[6], ele[6], thr[6], rud[6];
    ail[0] = ele[0] = thr[0] = rud[0] = 0x80;
    ail[1] = ele[1] = thr[1] = rud[1] = 0x01;
    ail[2] = ele[2] = thr[2] = rud[2] = 0x04;

    // Channel numbers for FeiLion
    ail[3] = 0x00;
    ele[3] = 0x01;
    thr[3] = 0x02;
    rud[3] = 0x03;

	ail[4] = nAilPos / 128;
	ail[5] = nAilPos % 128;
	ele[4] = nElePos / 128;
	ele[5] = nElePos % 128;
	thr[4] = nThrPos / 128;
	thr[5] = nThrPos % 128;
	rud[4] = nRudPos / 128;
	rud[5] = nRudPos % 128;

	write(_im9.m_nsIM9, ail, 6);
	write(_im9.m_nsIM9, ele, 6);
	write(_im9.m_nsIM9, thr, 6);
	write(_im9.m_nsIM9, rud, 6);
}

void clsSVO::SetCamera(double camera)
{
	char szCmd[32];

	//Assert the camera is installed PI/4 down
	double angle = camera + PI/4;
	angle = range(angle, -PI/4, PI/4);

	int nPosition = 15000 + (int)(5000*angle/(PI/4));
	sprintf(szCmd, "SV5 M%d\r", nPosition);
	write(m_nsSVO, szCmd, 11);
}

void clsSVO::WriteCommand()
{
//	tcflush(m_nsSVO, TCIOFLUSH);


	if  (_HELICOPTER == ID_GASSER) {
		write(m_nsSVO, "M?8\rM?9\rM?11\rM?12\rM?13\rM?15\r", 28);
	}

	else if (_HELICOPTER == ID_HELION || _HELICOPTER == ID_SHELION) {
		write(m_nsSVO, "M?9\r", 4);
		write(m_nsSVO, "M?10\r",5);
		write(m_nsSVO, "M?12\r",5);
		write(m_nsSVO, "M?13r",5);
		write(m_nsSVO, "M?14\r",5);
		write(m_nsSVO, "M?8\r", 4);
	}


	m_tRequest = GetTime();
}

// GremLion setting
void clsSVO::WriteCommand1()
{
//	tcflush(m_nsSVO, TCIOFLUSH);

/*	write(m_nsSVO, "M?10\r",5);
	write(m_nsSVO, "M?8\r",4);
	write(m_nsSVO, "M?11\r",5);
	write(m_nsSVO, "M?9\r",4);
	write(m_nsSVO, "M?13\r",5);
	write(m_nsSVO, "M?14\r",5);*/

	write(m_nsSVO, "M?12\r",5);		// elevator
	write(m_nsSVO, "M?10\r",5);		// throttle
	write(m_nsSVO, "M?8\r",4);		// aileron
	write(m_nsSVO, "M?10\r",5);		// auxiliary
	write(m_nsSVO, "M?9\r",4);		// rudder
	write(m_nsSVO, "M?15\r",5);		// toggle
	m_tRequest = GetTime();
}

BOOL clsSVO::GetData(SVORAWDATA *pData)
{
	char szSVO[256];

	SVORAWDATA svo;

	int nRead = read(m_nsSVO, szSVO, 256);
	if (nRead < 0) nRead = 0;
	szSVO[nRead] = '\0';

	int nScanf = sscanf(szSVO, "%hd%hd%hd%hd%hd%hd",
		&svo.throttle, &svo.rudder, &svo.aileron, &svo.elevator, &svo.auxiliary, &svo.sv6);

	if (nScanf != 6) {
		return FALSE;
	}

	*pData = svo;
	m_tRetrieve = m_tRequest + 0.004;

	return TRUE;
}

clsSVO::clsSVO()
{
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutex_init(&m_mtxSVO, &attr);
}

clsSVO::~clsSVO()
{
	pthread_mutex_destroy(&m_mtxSVO);
}

void clsSVO::Translate_HeLion(SVORAWDATA *pSVORAW, SVODATA *pSVO)
{
	pSVO->aileron = (double)(pSVORAW->aileron-15000)/5000;		// normalized to -1 ~ 1
	pSVO->elevator = (double)(pSVORAW->elevator-15000)/5000;	// normalized to -1 ~ 1
	pSVO->auxiliary = (double)(pSVORAW->auxiliary-15000)/5000;	// normalized to -1 ~ 1
	pSVO->rudder = (double)(pSVORAW->rudder-15000)/5000;		// normalized to -1 ~ 1
	pSVO->throttle = (double)(pSVORAW->throttle-10000)/10000;	// normalized to  0 ~ 1

	pSVO->sv6 = (double)(pSVORAW->sv6-15000)/5000;
}

void clsSVO::Translate_GremLion(SVORAWDATA *pSVORAW, SVODATA *pSVO)
{
	pSVO->aileron = (double)(pSVORAW->aileron-15290)/5000;		// normalized to -1 ~ 1
	pSVO->elevator = (double)(pSVORAW->elevator-15516)/5000;	// normalized to -1 ~ 1
	pSVO->auxiliary = (double)(pSVORAW->auxiliary-15000)/5000;	// normalized to -1 ~ 1
	pSVO->rudder = (double)(pSVORAW->rudder-14685)/5000;		// normalized to -1 ~ 1
	pSVO->throttle = (double)(pSVORAW->throttle-15000)/5000;	// normalized to -1 ~ 1
	pSVO->sv6 = (double)(pSVORAW->sv6-15000)/5000;
}

void clsSVO::Translate_GremLion(SVORAWDATA *pSVORAW, SVODATA *pSVO, SVORAWDATA *pSVOTrim, BOOL bManualTrim)
{
	if ( bManualTrim ) {
		pSVO->aileron = (double)(pSVORAW->aileron-pSVOTrim->aileron)/5000;		// normalized to -1 ~ 1
		pSVO->elevator = (double)(pSVORAW->elevator-pSVOTrim->elevator)/5000;		// normalized to -1 ~ 1
		pSVO->auxiliary = (double)(pSVORAW->auxiliary-15000)/5000;	// normalized to -1 ~ 1
		pSVO->rudder = (double)(pSVORAW->rudder-pSVOTrim->rudder)/5000;			// normalized to -1 ~ 1
		pSVO->throttle = (double)(pSVORAW->throttle- 15000 /*pSVOTrim->throttle*/)/5000;		// normalized to -1 ~ 1
		pSVO->sv6 = (double)(pSVORAW->sv6-15000)/5000;
	} else {
		pSVO->aileron = (double)(pSVORAW->aileron-15000)/5000;		// normalized to -1 ~ 1
		pSVO->elevator = (double)(pSVORAW->elevator-15000)/5000;		// normalized to -1 ~ 1
		pSVO->auxiliary = (double)(pSVORAW->auxiliary-15000)/5000;	// normalized to -1 ~ 1
		pSVO->rudder = (double)(pSVORAW->rudder-15000)/5000;			// normalized to -1 ~ 1
		pSVO->throttle = (double)(pSVORAW->throttle-15000)/5000;		// normalized to -1 ~ 1
		pSVO->sv6 = (double)(pSVORAW->sv6-15000)/5000;
	}
}

void clsSVO::TranslateTICBoard(SVORAWDATA_TIC *pSVORAW, SVODATA *pSVO)
{
	pSVO->aileron = (double)(pSVORAW->aileron-18000)/6000;		// normalized to -1 ~ 1
	pSVO->elevator = (double)(pSVORAW->elevator-18000)/6000;	// normalized to -1 ~ 1
	pSVO->auxiliary = (double)(pSVORAW->auxiliary-18000)/6000;	// normalized to -1 ~ 1
	pSVO->rudder = (double)(pSVORAW->rudder-18000)/6000;		// normalized to -1 ~ 1
	pSVO->throttle = (double)(pSVORAW->throttle-12000)/12000;	// normalized to  0 ~ 1

	pSVO->sv6 = (double)(pSVORAW->sv6-18000)/6000;

//	printf("raw aileron %d, ele %d\n", pSVORAW->aileron, pSVORAW->elevator);
//	printf("aileron %.3f, ele %.3f, aux %.3f, rud %.3f, thr %.3f\n", pSVO->aileron, pSVO->elevator, pSVO->auxiliary,
//			pSVO->rudder, pSVO->throttle);
}

void clsSVO::SetManualTrimRawData(SVORAWDATA svoraw)
{
	m_svoManualTrimRaw.aileron = svoraw.aileron;
	m_svoManualTrimRaw.elevator = svoraw.elevator;
	m_svoManualTrimRaw.auxiliary = svoraw.auxiliary;
	m_svoManualTrimRaw.rudder = svoraw.rudder;
	m_svoManualTrimRaw.throttle = svoraw.throttle;

}

void clsSVO::SetSvoConfig(char *devPort, short size, short flag, int baudrate)
{
	memcpy(m_svoConfig.devPort, devPort, size);
	m_svoConfig.flag = flag;
	m_svoConfig.baudrate = baudrate;
}
