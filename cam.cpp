/*
 * cam.cpp
 *
 *  Created on: Mar 15, 2011

 */

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>

#include "uav.h"
#include "cam.h"
#include "state.h"
#include "ctl.h"

extern EQUILIBRIUM _equ_Hover;
extern clsState _state;
extern clsCTL _ctl;
extern double Psimeasureall[4];
/*
 * clsCAM
 */


clsCAM::clsCAM()
{
	m_nsCAM = -1;

	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutex_init(&m_mtxCAM, &attr);
}

clsCAM::~clsCAM()
{
}

BOOL clsCAM::Init()
{
	m_nsCAM = open("/dev/serusb3", O_RDWR|O_NONBLOCK);

	if (m_nsCAM == -1) {
		printf("[CAM] open serusb3 failed!\n");
		return FALSE;
	}

	termios termCAM;
    tcgetattr(m_nsCAM, &termCAM);

	cfsetispeed(&termCAM, CAM_BAUDRATE);				//input and output baudrate
	cfsetospeed(&termCAM, CAM_BAUDRATE);

	termCAM.c_cflag &= ~(CS5|CS6|CS7|HUPCL|IHFLOW|OHFLOW|PARENB|CSTOPB|CSIZE); // Note: must do ~CSIZE before CS8
	termCAM.c_cflag |= (CS8 | CLOCAL | CREAD);
	termCAM.c_iflag = 0; // Raw mode for input
	termCAM.c_oflag = 0; // Raw mode for output

	tcflush(m_nsCAM, TCIOFLUSH);
	tcsetattr(m_nsCAM, TCSANOW, &termCAM);

	return TRUE;
}

BOOL clsCAM::InitThread()
{
	m_nBuffer = 0;
	m_tInfo0 = -1;

	m_nInfo=0;

	m_DetectState = 0;
	::memset(&m_targetInfo0, 0, sizeof(TARGETINFO));
	::memset(&m_targetState0, 0, sizeof(TARGETSTATE));
	::memset(&m_targetInfo, 0, sizeof(BJ_TARGET));
	::memset(&m_targetInfoUpdate, 0, sizeof(BJ_TARGET));
	::memset(&m_UavBJpose, 0, sizeof(UAVBJPOSE));

	int index;

	for (index = 0; index < MAX_CAMINFO; index++) {
		m_tInfo[index]  = 0;
		m_info[index].l = 0;
		m_info[index].m = 0;
		m_info[index].n = 0;
	}

	tStart = 0;

	m_releaseTargetFinalPhase = 0;
	m_bVisionFinalPhaseFlag = false;

	printf("[CAM] start\n");

	return TRUE;
}

void *clsCAM::InputThread(void *pParameter)
{
	clsCAM *pCAM = (clsCAM *)pParameter;

	pCAM->Input();

	return NULL;
}

BOOL clsCAM::StartInputThread(int priority)
{
    pthread_attr_t attribute;
    pthread_attr_init(&attribute);
    pthread_attr_setdetachstate(&attribute, PTHREAD_CREATE_DETACHED);
    pthread_attr_setinheritsched(&attribute, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(&attribute, SCHED_RR);

    sched_param_t param;
    pthread_attr_getschedparam(&attribute, &param);
    param.sched_priority = priority;
    pthread_attr_setschedparam(&attribute, &param);

	pthread_t thread;
    pthread_create(&thread, &attribute, &clsCAM::InputThread, this);

    return TRUE;
}

void clsCAM::Input()
{
	printf("[CAM] start (input thread)\n");

	COMMAND cmd; cmd.code = -1; //initialize
	CAMINFO info = {0};

	//struct TARGETSTATE targetState;

	m_nCountCAMRD=0;

	while (1) {
		if (ReadCommand(&cmd)) {
			switch (cmd.code){
			case CAM_TARGETINFO:{
				tStart = ::GetTime();
				for(int i = 0 ; i < 10; i++) m_targetInfo.flags[i] = GETWORD(cmd.parameter + 4*i);
				for(int i = 0; i< 3; i++) m_targetInfo.tvec[i] = GETDOUBLE(cmd.parameter + 40 + i*8);

//				printf("[Cam Target Camera] %d %.2f %.2f %.2f\n", m_targetInfo.flags[1], m_targetInfo.tvec[0], m_targetInfo.tvec[1], m_targetInfo.tvec[2]);

				double abc[3] = {(-1.0)*_state.GetState().a, (-1.0)*_state.GetState().b, 0};
				double r_t_c[3] = {m_targetInfo.tvec[0], m_targetInfo.tvec[1], m_targetInfo.tvec[2]};

				double camera_body[3] = {0};
				B2G(abc, r_t_c, camera_body);
				camera_body[0] += 0.3;
				camera_body[1] += 0;
				camera_body[2] += 0.13;

				for(int i = 0 ; i < 10; i++) m_targetInfoUpdate.flags[i] = m_targetInfo.flags[i];

				double abc_ned[3] = {_state.GetState().a, _state.GetState().b, _state.GetState().c};
				double delta_body[3] = {camera_body[0], camera_body[1], camera_body[2]};
				double delta_ned[3] = {0};
				B2G(abc_ned, delta_body, delta_ned);

				// in ship frame
				m_targetInfoUpdate.tvec[0] = delta_ned[0]*cos(Psimeasureall[1]) + delta_ned[1]*sin(Psimeasureall[1]);
				m_targetInfoUpdate.tvec[1] = (-1)*delta_ned[0]*sin(Psimeasureall[1]) + delta_ned[1]*cos(Psimeasureall[1]);
				m_targetInfoUpdate.tvec[2] = delta_ned[2];

				if(_ctl.m_taskCounter % 2 == 0 && _ctl.m_taskCounter > 0){ // release the target phase
							if(_state.GetState().z < CONFUSE_VISION_HEIGHT){ //the height of the helicopter;
									double dc = _state.GetState().c - Psimeasureall[1]; INPI(dc);
									if ( ::fabs(dc) < PI/2.0 ){ // the helicopter has the same heading direction as the ship
											m_targetInfoUpdate.tvec[0] = m_targetInfoUpdate.tvec[0] - 1;
									}
									else{ // the helicopter has the opposite heading direction as the ship
											m_targetInfoUpdate.tvec[0] = m_targetInfoUpdate.tvec[0] + 1;
									}
							}
							else{
								if (m_targetInfoUpdate.flags[0] == 1){
									SetVisionFinalPhase();
								}
							}
//							else if(_state.GetState().z < CUT_VISION_HEIGHT && _state.GetState().z > CONFUSE_VISION_HEIGHT){
//											// do nothing now
//							}
//							else if(_state.GetState().z > CUT_VISION_HEIGHT){//the height of the helicopter is lower, cut the vision measurement
//											m_targetInfoUpdate.flags[0] = 0;
//							}
				}

//				printf("[Cam Target ShipFrame] %d %.2f %.2f %.2f\n", m_targetInfo.flags[0], m_targetInfoUpdate.tvec[0], m_targetInfoUpdate.tvec[1], m_targetInfoUpdate.tvec[2]);
				break;
				}
			}
		}
		m_nCountCAMRD++;
		usleep(100000);
	}
}

BOOL clsCAM::ReadCommand(COMMAND *pCmd)
{
	int nRead = read(m_nsCAM, m_buffer+m_nBuffer, MAX_CAMBUFFER-m_nBuffer);	//get data as many as it can

	if (nRead < 0) return FALSE;
//	if (/*nRead != -1*/ nRead > 0){
//		printf("[CAM] ReadCommand, read byte %d, buffer size %d\n", nRead, m_nBuffer);
//		printf("tElapse %.2f: ", ::GetTime());
//		for (int i= 0; i<nRead; i++)
//		printf("%02x ",  (unsigned char)m_buffer[m_nBuffer + i]);
//		printf("\n");
//	}
	m_nBuffer += nRead;

#if ( _DEBUG & DEBUGFLAG_CAM )
	if(m_nCountCAMRD%_DEBUG_COUNT_CAMRD ==0){
	if(nRead!=0){
		printf("\n");
		printf("[CAM] ReadCommand, read byte %d, buffer size %d\n", nRead, m_nBuffer);
//     	char *pdChar = m_buffer;
//    	printf("[CAM] ReadCommand Buffer=");
//    	while(pdChar< m_buffer+nRead){
//    		printf("%4x", *(unsigned char*)pdChar);  pdChar++;
//   		}
//    	printf("\n");
	}
	}
#endif

	COMMAND cmd = {0};

	char *pChar = m_buffer;
	char *pCharMax = m_buffer+m_nBuffer-1;
	enum { BEGIN, HEADER, SIZE, PACKAGE, CHECK } state = BEGIN;

	char package[MAXSIZE_PACKAGE];
	int nPackageSize = 0;

	char *pNextTelegraph = NULL;

	while (pChar <= pCharMax) {
		if (state == BEGIN) {
			if ( *pChar == 0x80|MASTERMIND) {
				state = HEADER;
			}
			pChar ++; continue;
		}

		if (state == HEADER) {
			if ((*pChar >= 0 && *pChar <= 99) || *pChar & 0x80) state = SIZE;
			else  state = BEGIN;
			pChar ++; continue;
		}

		if (state == SIZE) {
			if (pChar + 1 > pCharMax) break;
			nPackageSize = GETWORD(pChar);
			if (nPackageSize < 1 || nPackageSize > MAXSIZE_PACKAGE) {
				state = BEGIN;
				continue;
			}
			else {
				state = PACKAGE;
				pChar += 2;
				continue;
			}
		}

		if (state == PACKAGE) {
			if (pChar + nPackageSize + 1 > pCharMax) break;

			unsigned short sum = CheckSum(pChar, nPackageSize);
			unsigned short check = GETWORD(pChar+nPackageSize);

//#if (_DEBUG & DEBUGFLAG_CAM)
//		printf("[CAM] Readcommand PACKAGE: sum=%d, check=%d\n", sum, check);
//#endif
			if (sum != check) {
				printf("[CAM] checksum error\n");
				state = BEGIN; continue;
			}

			::memcpy(package, pChar, nPackageSize);		// copy the correct package

			pNextTelegraph = pChar + nPackageSize + 2;

//			break;
			pChar=pNextTelegraph;
		}
	}

	//if (pNextTelegraph == NULL) return FALSE;
	if ( (pNextTelegraph==NULL)) {//didn't find a completed data package
//		if (state!=PACKAGE) {// the state is not PACKAGE
//			m_nBuffer=0;
//		}
//		printf("pNextTelegraph NULL!\n");
		return FALSE;
	}

	if (pNextTelegraph > pCharMax) {
//		printf("pNextTelegraph > pCharMax!\n");
		m_nBuffer = 0;
	}
	else {
		m_nBuffer = pCharMax - pNextTelegraph + 1; //unfinished telegraph copy to buffer for prcessing in next cycle
		::memcpy(m_buffer, pNextTelegraph, m_nBuffer);
	}

	cmd.code = GETWORD(package);

//	m_DetectState = GETWORD(package+2);
	::memcpy(cmd.parameter, package+10, sizeof(BJ_TARGET) );
	*pCmd = cmd;

	return TRUE;
}

void clsCAM::Update()
{
//	UAVSTATE &state = _state.GetState();

	/*double l, m, n, h;

	h = state.z;

	m_tTARGETState0   = m_tInfo2;
	m_TARGETState0.tq = atan(-1*n/sqrt(l*l+m*m));
	m_TARGETState0.tr = atan(m/l);
	*/
}

void clsCAM::GetInfo()
{
	double tInfo = 0;
	int index, tIndex=0;

	for (index=0; index< MAX_CAMINFO; index++) {
		if ( m_tInfo[index]>tInfo ) {
			tIndex = index;
			tInfo  = m_tInfo[index];
		}
	}

	m_tInfo2 = tIndex;
	m_info2.l = m_info[tIndex].l;
	m_info2.m = m_info[tIndex].m;
	m_info2.n = m_info[tIndex].n;
}

void clsCAM::PutCommand(COMMAND *pCmd)
{
	pthread_mutex_lock(&m_mtxCmd);

	m_cmd = *pCmd;

	pthread_mutex_unlock(&m_mtxCmd);
}

void clsCAM::GetCommand(COMMAND *pCmd)
{
	pthread_mutex_lock(&m_mtxCmd);

	if (m_cmd.code == 0) pCmd->code = 0;
	else {
		*pCmd = m_cmd;
		m_cmd.code = 0;				//clear
	}

	pthread_mutex_unlock(&m_mtxCmd);
}

BOOL clsCAM::ProcessCommand(COMMAND *pCommand)
{
	COMMAND &cmd = *pCommand;
//	char *paraCmd = cmd.parameter;

//	int &behavior = m_behavior.behavior;
//	char *paraBeh = m_behavior.parameter;

	BOOL bProcess = TRUE;
//	_state.EnableVision();
	switch (cmd.code) {
	case COMMAND_CAMVBS:
//		behavior = BEHAVIOR_TAKEOFF;
		_state.EnableVision();
		break;
	default:
		bProcess = FALSE;
		break;
	}
	return bProcess;
}

void clsCAM::MakeTelegraph(TELEGRAPH *pTele, short code, double time, void *pData, int nDataSize)
{
	char *pBuffer = pTele->content;

	//pBuffer[0] = 0x50 + _HELICOPTER;				//_nHelicopter 6 or 7, 0x56 for helion, 0x57 for shelion
	//pBuffer[1] = 0x55;				//ground station

//	pBuffer[0] = 0x57;				//_nHelicopter 6 or 7, 0x56 for helion, 0x57 for shelion
//	pBuffer[1] = 0x57;

	pBuffer[0] = 0x87;				//_nHelicopter 6 or 7, 0x56 for helion, 0x57 for shelion
	pBuffer[1] = 0x93;
	
	switch(code) {
		case COMMAND_CAMRUN:
		case COMMAND_CAMSTOP:
		case COMMAND_CAMQUIT:
		case COMMAND_CAMTHOLD: {
//			(short &)pBuffer[2] = nDataSize+2;
//			(short &)pBuffer[4] = code;
//			(double &)pBuffer[6] = *(double*)pData;
//			(unsigned short &)pBuffer[6+nDataSize] = CheckSum(pBuffer+4, 2+nDataSize);
//			pTele->size = nDataSize + 8;
			PUTWORD(pBuffer+2, nDataSize+2);
			PUTWORD(pBuffer+4, code);
			::memcpy(pBuffer+6, pData, nDataSize);

			unsigned short sum  = CheckSum(pBuffer+4, 2+nDataSize);
			PUTWORD(pBuffer+14+nDataSize, sum);

			pTele->size = nDataSize + 8;
			break; }
		case COMMAND_CAMUAVPOSE: {
//			(short &)pBuffer[2] = nDataSize+10;
//			(short &)pBuffer[4] = code;
//			(double &)pBuffer[6]= time;
//			::memcpy(pBuffer+14, pData, nDataSize);
//			(unsigned short &)pBuffer[14+nDataSize] = CheckSum(pBuffer+4, 10+nDataSize);

			PUTWORD(pBuffer+2, nDataSize+10);
			PUTWORD(pBuffer+4, code);
			PUTDOUBLE(pBuffer+6, time);

			::memcpy(pBuffer+14, pData, nDataSize);

			unsigned short sum  = CheckSum(pBuffer+4, 10+nDataSize);
			PUTWORD(pBuffer+14+nDataSize, sum);

			pTele->size = nDataSize + 16;
			break;}
	}

#if(_DEBUG  & DEBUGFLAG_CAMWR )
	switch(code) {
		case COMMAND_CAMRUN:
		case COMMAND_CAMSTOP:
		case COMMAND_CAMQUIT:
		case COMMAND_CAMTHOLD:
			printf("[CAM] Tele: code=%3d, parameter=%7.3f\n",(short &)pBuffer[4], (double &)pBuffer[6]);
			break;
		case COMMAND_CAMUAVPOSE:
			printf("[CAM] Tele: code=%3d, time=%7.3f\n",(short &)pBuffer[4], (double &)pBuffer[6]);
			printf("[CAM] Tele: data=");

			char *pChar;
			for (pChar=&pBuffer[14]; pChar<pBuffer+nDataSize+14; pChar+=sizeof(double) ) {
				printf("%6.2f", *(double *)pChar);
			}
			printf("\n");

			printf("[CAM] Tele (binary):");
			for (pChar=pBuffer; pChar<pBuffer+nDataSize+16; pChar++ ) {
				printf("%4x", *(unsigned char*)pChar);
			}
			printf("\n");
			break;
	}
#endif

}
int clsCAM::EveryRun()
{
//	CAMINFO info;
//	if (!ReadCamInfo(&info)) return TRUE;
	// get the state of the UAV
	UAVSTATE &state = _state.GetState();

//	double t = _state.GetStateTime();
	double t = ::GetTime();


//	state.x=1; state.y=2; state.z=3;
//	state.u=4; state.v=5; state.w=6;
//	state.a=7; state.b=8; state.c=9;
//	state.p=10; state.q=11; state.r=12;
//	state.longitude=13; state.latitude=14; state.altitude=15;


	double x[18] = {
		state.x, state.y, state.z,
		state.u - _equ_Hover.u, state.v - _equ_Hover.v, state.w - _equ_Hover.w,
		state.a - _equ_Hover.a, state.b - _equ_Hover.b, state.c - _equ_Hover.c,
		state.p - _equ_Hover.p, state.q - _equ_Hover.q, state.r - _equ_Hover.r,
		state.acx, state.acy, state.acz,
		state.longitude, state.latitude, state.altitude
	};

	COMMAND cmd;
	GetCommand(&cmd);

	if (cmd.code != 0) {
		if (ProcessCommand(&cmd)) cmd.code = 0;
	}

	double Parameter0;
	int nWrite;

	TELEGRAPH tele;

	//if (m_nCount != 0 && m_nCount != 100) return TRUE;				//send command in the beginning and 2s later, twice

	switch(cmd.code) {
		case COMMAND_CAMRUN:
		case COMMAND_CAMSTOP:
		case COMMAND_CAMQUIT:
		case COMMAND_CAMVBS:
		    Parameter0 = 1;
		    MakeTelegraph(&tele, cmd.code, t, &Parameter0, sizeof(double));
		    nWrite=write(m_nsCAM, tele.content, tele.size);
		    break;
		case COMMAND_CAMTHOLD:
		    Parameter0 = *(double *)cmd.parameter;
		    MakeTelegraph(&tele, cmd.code, t, &Parameter0, sizeof(double));
		    nWrite=write(m_nsCAM, tele.content, tele.size);
		    break;
	}

	//write the pose of the UAV
	/* added by LPD for 2013- UAVGP 2013-08-06 */
	double shipFrameX = _ctl.GetUavMeasurement2013().x;
	double shipFrameY = _ctl.GetUavMeasurement2013().y;

	m_UavBJpose.x = m_releaseTargetFinalPhase; /*shipFrameX; */ m_UavBJpose.y = shipFrameY; m_UavBJpose.z =  _ctl.GetUavMeasurement2013().z;
	m_UavBJpose.a = state.a;  m_UavBJpose.b = state.b; m_UavBJpose.c = state.c; m_UavBJpose.target_c = Psimeasureall[1];
	m_UavBJpose.trackCircleNum = _ctl.m_nTrackTargetNumber;

//	if(m_nCount%50 == 0)
//		printf("Track Number %d %.2f\n", m_UavBJpose.trackCircleNum,  m_UavBJpose.target_c*180/PI);

	if ( (m_nCount% 5 /* COUNT_UAVSTATE_CAM */ ==0) && (cmd.code==0) )  {
		MakeTelegraph(&tele, COMMAND_CAMUAVPOSE, t, &m_UavBJpose, sizeof(UAVBJPOSE));
		nWrite=write(m_nsCAM, tele.content, tele.size);
//		printf("Write %d bytes.\n",nWrite);
	}
	/* End of LPD's part */

	return TRUE;
}

void clsCAM::ExitThread()
{
//	if (m_pfCAM != NULL) ::fclose(m_pfCAM);
	printf("[CAM] quit\n");
}

void clsCAM::SetCamConfig(char *devPort, short size, short flag, int baudrate)
{
	memcpy(m_camConfig.devPort, devPort, size);
	m_camConfig.flag = flag;
	m_camConfig.baudrate = baudrate;
}

BJ_TARGET clsCAM::GetVisionTargetInfo() {  //return relative position in NED frame;
		return m_targetInfoUpdate;
}
/*
 * clsVision
 */
