# Summary #
This repository is to implement the automatic flight control and navigation based on thunder tiger Raptor 90 helicopters. The raptor 90 is customized with a Zenoarh G270RC gasoline engine.

# Details #
The onboard software is based on QNX6.5.0 and implemented on Gumstix Overo Fire COM. 
Components: IG-500N, TIC Servo Board, Micro Hard N2400.