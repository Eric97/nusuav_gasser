//daq.cpp
// created on 2011-03-15
// implementation file for clsDAQ, in charge of data acquisition
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <termios.h>
#include "state.h"

#include "uav.h"
#include "daq.h"

extern double Pmeasureall[12];
extern double Vmeasureall[12];
extern double Vdesireall[12];

extern clsState _state;

void clsDAQ::Translate(DAQRAWDATA *pDAQRAW, DAQDATA *pDAQ)
{
#ifndef _GUMSTIX
	DAQRAWDATA &daqraw = *pDAQRAW;
	DAQDATA &daq = *pDAQ;

	unsigned short b16 = 0x8000;

	//change format
	daq.level = (double)daqraw.level*10/b16;
	daq.height = (double)daqraw.height*800/b16 - HEIGHT_CORRECTION;
	daq.height /= 1000;				//translated into meter
//*** modify_5
//	daq.elevator = (double)daqraw.Elevator_Svo*10/b16;

	if ((daq.bRPM = daqraw.bRPM)) {
		daq.status = daqraw.status;
		daq.rpm = (double)daqraw.num*PI*_cps/daqraw.cycleElapse;
	}
#endif
}

clsDAQ::clsDAQ()
{
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutex_init(&m_mtxDAQ, &attr);
}

clsDAQ::~clsDAQ()
{
	pthread_mutex_destroy(&m_mtxDAQ);
}

BOOL clsDAQ::InitThread()
{
	//initialization
	m_nDAQ = 0;
	m_tDAQ0 = -1;

	/*
	m_nsSNA = ::open("/dev/serusb3", O_RDWR | O_NOCTTY );//O_NONBLOCK
//	m_nsSNA = ::open("/dev/ser2", O_RDWR | O_NOCTTY );//O_NONBLOCK
	if (m_nsSNA == -1) {
		printf("[DAQ] Sonar usb serial port cannot be opened!\n");
		return FALSE;
	}
    termios term;
    memset(&term,0,sizeof(term));
    tcgetattr(m_nsSNA, &term);
    cfsetispeed(&term, 9600);			//input and output baudrate
    cfsetospeed(&term, 9600);
    term.c_cflag &= ~(PARENB | CSTOPB | CSIZE); // Note: must do ~CSIZE before CS8
    term.c_cflag |= CS8 | CLOCAL | CREAD;
    term.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // Non-canonical interpretation
    term.c_iflag = 0; // Raw mode for input
    term.c_oflag = 0; // Raw mode for output
    term.c_cc[VMIN] = 1;
	tcflush(m_nsSNA,TCIOFLUSH);
	tcsetattr(m_nsSNA, TCSANOW, &term);

	m_nBuffer = 0;

//	m_nSNA = 0;
	m_sna = 0;

	printf("[DAQ] Sonar Start\n");
	*/

	return TRUE;
}

int clsDAQ::EveryRun()
{
//	int nRead = read(m_nsSNA, m_buffer+m_nBuffer, BUFFERSIZE_SNA - m_nBuffer);

	if (m_nCount % 5 == 0) return TRUE;

	pthread_mutex_lock(&m_mtxDAQ);
	if (m_nDAQ != MAX_DAQ) {
		m_tDAQ[m_nDAQ] = m_tDAQ0;
		m_daq[m_nDAQ++] = m_daq0;
	}
	pthread_mutex_unlock(&m_mtxDAQ);
	return TRUE;
}

void clsDAQ::ExitThread()
{
	::close(m_nsSNA);
	printf("[DAQ] Sonar quit\n");
}

