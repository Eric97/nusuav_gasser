/*
 * vision.h
 *
 *  Created on: Dec 14, 2012
 *      Author: nusuav
 */

#ifndef VISION_H_
#define VISION_H_

#include "opencv2/opencv.hpp"
using namespace cv;

#include "matrix.h"
#include "cam.h"

#define FUSION_KALMAN6		1
#define VISION_INTERVAL 	0.1

struct UAVSTATE;

struct VISIONSTATE {
	double estx, esty, estz;
	double estug, estvg, estwg;
	double esta, estb, estc;

	double bias1, bias2, bias3;
	double bias4, bias5, bias6;

	double homo11, homo12, homo13;
	double homo21, homo22, homo23;
	double homo31, homo32, homo33;
};

class clsVision
{
public:
	clsVision();
	~clsVision();
	void Init(const UAVSTATE uavstate);

private:
	VISIONSTATE m_visionState;
	short m_bDetectStatus;		// TRUE: target locked; FALSE target lost
	short m_fusionMethod;
	BOOL m_bFusion;			// flag to indicate vision fusion is performed or not
	BOOL m_bFirst;				// First iteration flag, can be used for reset
	BOOL m_bBias;

private:
	Scalar m_scalar_zero;
	Mat m_Mat_homo_all, m_Mat_sigma_wa, m_Mat_sigma_wg, m_Mat_Q_cov, m_Mat_R_cov;
	Mat m_Mat_P_1, m_Mat_P_1_novis, m_Mat_P_2, m_Mat_P_2_novis;
	Mat m_Mat_x_init, m_Mat_x_novis_all, m_Mat_x_hat_all, m_Mat_bias_hat_all, m_Mat_x_kal_1, m_Mat_x_kal_1_novis;
	Mat m_Mat_a_nb_ins_1, m_Mat_omg_ins_1;
	Mat m_Mat_H_vis, m_Mat_temp_H_vis_vec, m_Mat_singlarvalues, m_Mat_omg_t_sk, m_Mat_Delta_R_ins;
	Mat m_Mat_x_kal_2, m_Mat_x_kal_2_novis;
public:
	// only for test from TEXT
	Mat m_Mat_prevUAVState, m_Mat_curUAVState;
//	int num; // the total number of the time
//	int index; // the index of the current time
//	Mat x_real_all, omg_real_all, a_nb_real_all, homo_all;

protected:
	double _Kalman_A[6][6];	clsMatrix Kalman_A;
	double _Kalman_B[6][3];	clsMatrix Kalman_B;
	double _Kalman_C[3][6];	clsMatrix Kalman_C;
	double _Kalman_D[3][3];	clsMatrix Kalman_D;
	double _Kalman_Q[3][3];	clsMatrix Kalman_Q;
	double _Kalman_R[3][3];	clsMatrix Kalman_R;
	double _Kalman_P[6][6]; clsMatrix Kalman_P;
	double _BQBt[6][6];		clsMatrix BQBt;
	double _xVision[6];

	double m_camx0, m_camy0, m_camz0; 	// initial x,y,z in NED once vision fusion starts
public:
	void ResetbFirst() { m_bFirst = FALSE; }
	void SetbFirst() { m_bFirst = TRUE; }
	BOOL GetbFirst() { return m_bFirst; }

	void SetFusionFlag()	{ m_bFusion = TRUE; }
	void ResetFusionFlag()	{ m_bFusion = FALSE; }
	BOOL GetFusionFlag() { return m_bFusion; }
	BOOL VisionFusion(UAVSTATE &uavstate, CAMTELEINFO *pcamInfo);
	BOOL KalmanFilterWithVision(BOOL bVisionUpdate, BOOL bVisionInterval, UAVSTATE &uavstate, CAMTELEINFO &camInfo);
	BOOL Kalman();
	VISIONSTATE &GetVisionState() { return m_visionState; }

	void fcn_GetSkew(Mat x, Mat x_skew);
	//void fcn_RodriguesRotation(Mat omega, Mat Rotation);
	void fcn_RodriguesRotation(Mat omega, double theta, Mat Rotation);
	void fcn_DispDoubleMat(Mat& A);
	void fcn_Euler2Rotation(double phi, double theta, double psi, Mat Rbln);
	void fcn_fxu(Mat x, Mat a_nb, Mat omg, Mat fxu);
	void fcn_GetFG_noBias(Mat x_c_1, Mat a_nb_c1, Mat omg_c1, Mat F_ext, Mat G_ext);
	void fcn_GetFG_withBias(Mat x_c_1, Mat a_nb_c1, Mat omg_c1, Mat F_ext, Mat G_ext);
	void fcn_GetC_noBias(Mat x_c_1pre2, Mat Delta_R_ins, Mat C_ext);
	void fcn_GetC_withBias(Mat x_c_1pre2, Mat Delta_R_ins, Mat C_ext);
	BOOL fcn_CheckInput_noBias(Mat H_vis, Mat Delta_R_ins0, double y_alt,
							  Mat x_kal_1, Mat P_1, Mat a_nb_ins_1, Mat omg_ins_1, Mat Q_cov, Mat R_cov,
							  Mat x_kal_2, Mat P_2);
	BOOL fcn_CheckInput_withBias(Mat H_vis, Mat Delta_R_ins0, double y_alt,
								Mat x_kal_1, Mat P_1, Mat a_nb_ins_1, Mat omg_ins_1, Mat Q_cov, Mat R_cov,
								Mat x_kal_2, Mat P_2);
	BOOL fcn_KalmanFilter_noBias(int bVision, Mat H_vis, Mat Delta_R_ins0, int bMag, double y_mag, int bAlt, double y_alt,
								Mat x_kal_1, Mat P_1, Mat a_nb_ins_1, Mat omg_ins_1, Mat Q_cov, Mat R_cov,
								Mat &x_kal_2, Mat &P_2);
	BOOL fcn_KalmanFilter_withBias(int bVision, Mat H_vis, Mat Delta_R_ins0, int bMag, double y_mag, int bAlt, double y_alt,
								  Mat x_kal_1, Mat P_1, Mat a_nb_ins_1, Mat omg_ins_1, Mat Q_cov, Mat R_cov,
								  Mat &x_kal_2, Mat &P_2);
	void fcn_INS_DeadReckoning(Mat x_ins_1, Mat omg_ins_2, Mat a_nb_ins_2, Mat x_ins_2);
	int fcn_Innovation_Filter(Mat y_k, Mat y_1pre2, Mat pred_cov);
	void fcn_GetHomoMatrixFrom1to2(Mat x1, Mat x2, Mat H);
};

#endif /* VISION_H_ */
