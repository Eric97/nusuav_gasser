#include <iostream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <list>

#include "height.h"
#include "state.h"

/*** For indoor test ***/
//#define MIN_SM_GROUP_SIZE 10
//#define SPLIT_MERGE_THR 50
//#define SHIP_HEIGHT_MIN 160
//#define SHIP_HEIGHT_MAX 230
//#define SHIP_SEPARATION 300
//#define SHIP_WIDTH 200

/*** For outdoor real flight ***/
#define MIN_SM_GROUP_SIZE 40
#define SPLIT_MERGE_THR 50
#define SHIP_HEIGHT_MIN 400
#define SHIP_HEIGHT_MAX 700
#define SHIP_SEPARATION 5000
#define SHIP_WIDTH 1000

extern clsState _state;

using namespace std;

void height::init()
{
    for (int i=0; i<reading_count_height; i++)
    {
		bearing_mat[i] = start_angle + i*angle_per_index;
		if(bearing_mat[i] > pi)
			bearing_mat[i] = bearing_mat[i]-2*pi;
		cos_bearing_mat[i] = cos(bearing_mat[i]);
		sin_bearing_mat[i] = sin(bearing_mat[i]);
    }

    height_flag = false;
	confidence_y = false;
	y = 0;
	h = 0;
}

double height::localization(long int input[])
{
	a = _state.GetState().a;
	b = _state.GetState().b;

//	printf("phi, theta = %f, %f\n", a, b);

	SlamPoints(input); // Get Cartesian coordinates of all raw points
	LineDetection();   // Extract all possible line segments within requirements

	if (GroundDetection() == 1) // Find which line is ground
	{
//		printf("Height: %f, Angle: %f, (No. of lines: %d)\n", h, ground.theta*180/pi, detected_line_local.size());
//		ship_count = ShipDetection();   // Decide which cluster of points belong to a ship
	}
	
	else
		confidence_y = false;

//    printf("y: %f; h: %f ", y, h);
//    printf("No. of ships: %d   ", ship_count);
//    printf("Ship point count: %d / %d \n", ship_point_count, valid_count);

	return h;
}

int height::ShipDetection()
{
	int count = 0; // number of ships
	int k = 0; // number of points belong to ship(s)
    for (int i=0; i<valid_count; i++)
    {
//    	printf("ground angle: %f\n", ground.theta*180/pi);
//    	double d = h - fabs(points[i].r*sin(points[i].theta-a)*cos(b));
    	double d = h - fabs(points[i].r * sin(points[i].theta - (ground.theta-pi/2)) * cos(b));
    	if ( d>SHIP_HEIGHT_MIN && d<SHIP_HEIGHT_MAX )
    	{
    		ship_points[k] = points[i];
    		k++;
//    		printf("Ship_points [%d]: x: %f, y: %f, r: %f, tht: %f, h:%f, d:%f\n", points[i].point_index, points[i].x, points[i].y, points[i].r, points[i].theta, h, d);
    	}
    }

    ship_point_count = k;

    if (ship_point_count < 5)
    {
    	return 0;
		confidence_y = false;
    }

    ships.clear();
    ship_ this_ship;
    for (int i=0; i<ship_point_count; i++)
    {
    	if (i==0)
    	{
    		this_ship.p1.x = ship_points[i].x;
    		this_ship.p1.y = ship_points[i].y;
    		continue;
    	}

    	double d = (ship_points[i].x - ship_points[i-1].x)*(ship_points[i].x - ship_points[i-1].x)
    			 + (ship_points[i].y - ship_points[i-1].y)*(ship_points[i].y - ship_points[i-1].y);

    	if (sqrt(d) > (SHIP_WIDTH))
    	{
    		this_ship.p2.x = ship_points[i-1].x;
    		this_ship.p2.y = ship_points[i-1].y;
    		this_ship.width = sqrt((this_ship.p1.x - this_ship.p2.x)*(this_ship.p1.x - this_ship.p2.x) +
    				           (this_ship.p1.y - this_ship.p2.y)*(this_ship.p1.y - this_ship.p2.y));
    		if ((this_ship.width>SHIP_WIDTH*0.25) && (this_ship.width<SHIP_WIDTH*1.5))
    		{
    			ships.push_back(this_ship);
    		}

    		this_ship.p1.x = ship_points[i].x;
    		this_ship.p1.y = ship_points[i].y;
    	}

    	if (i==ship_point_count-1)
    	{
    		this_ship.p2.x = ship_points[i].x;
    		this_ship.p2.y = ship_points[i].y;
    		this_ship.width = sqrt((this_ship.p1.x - this_ship.p2.x) * (this_ship.p1.x - this_ship.p2.x) +
    				           (this_ship.p1.y - this_ship.p2.y) * (this_ship.p1.y - this_ship.p2.y));
    		if ((this_ship.width>SHIP_WIDTH*0.25) && (this_ship.width<SHIP_WIDTH*1.5))
    		{
    			ships.push_back(this_ship);
    		}
    	}

    }

    count = ships.size();

    // To refine the ship edges
    for (list<ship_>::iterator iter = ships.begin(); iter!=ships.end(); iter++)
    {

//    	printf("Raw - Left: (%f, %f), Right: (%f, %f)\n", iter->p1.x, iter->p1.y, iter->p2.x, iter->p2.y);

    	point p1 = Laser2Ship(iter->p1, ground.theta-pi/2, b);
    	point p2 = Laser2Ship(iter->p2, ground.theta-pi/2, b);

    	double left_point = p1.y;
    	double right_point = p2.y;

    	// the smaller one should be on the left
    	if (left_point > right_point)
    	{
    		double temp = left_point;
    		left_point = right_point;
    		right_point = temp;
    	}

    	if ((left_point+right_point) < 0)
    	{
    		iter->p2.y = right_point;
    		iter->p1.y = right_point - SHIP_WIDTH;
    		iter->p_mid.y = right_point - SHIP_WIDTH*0.5;
    	}

    	else
    	{
    		iter->p1.y = left_point;
    		iter->p2.y = left_point + SHIP_WIDTH;
    		iter->p_mid.y = left_point + SHIP_WIDTH*0.5;
    	}

//    	printf("(%f, %f, %f)\n", iter->p1.y, iter->p_mid.y, iter->p2.y);
    }

    if (count == 2)
    {
    	list<ship_>::iterator iter = ships.begin();
    	double left = iter->p_mid.y;
    	iter++;
    	double right = iter->p_mid.y;

//    	if (left > right)
//    	{
//    		double temp = left;
//    		left = right;
//    		right = temp;
//    	}

//    	printf("Left mid: %f, Right mid: %f\n", left, right);

//    	double result1 = -left;
//   	double result2 = -right + SHIP_SEPARATION + SHIP_WIDTH;

//    	if (fabs(result1-result2)<1000)
//    		y = (result1+result2)*0.5;

		if (abs(left) < abs(right))
			y = left;
		else
			y = right;
			
		confidence_y = true;
    }

    else if (count == 1)
    {
/*    	double temp1 = 0 - ships.begin()->p_mid.y;
    	double temp2 = SHIP_SEPARATION + SHIP_WIDTH - ships.begin()->p_mid.y;
    	if (fabs(temp1-y) < fabs(temp2-y))
    	{
    		if (fabs(temp1-y)<1000)
    			y = temp1;
    	}
    	else
    	{
    		if (fabs(temp2-y)<1000)
    			y = temp2;
    	}*/
		
		y = ships.begin()->p_mid.y;
		
		confidence_y = true;
    }
	
	else
		confidence_y = false;

    return count;
}

point height::Laser2Ship (point in, double a, double b)
{
	point out = in;

	// Convert point in laser scanner frame to body frame
	double x = -200;
	double y = -in.x;
	double z = in.y + 100;

	// Convert point in body frame to ship frame
	double body[3] = {x, y, z};
	double ship[3] = {0};
	double abc[3] = {a, b, 0}; // TODO
	B2G(abc, body, ship);

	out.x = ship[0];
	out.y = ship[1];

	return out;
}

void height::SlamPoints(long int data[])
{
	int k = 0;
    for (int i=0; i<reading_count_height; i++)
    {
    	if (data[i+START_INDEX] >= 20 && data[i+START_INDEX] < 30000)
    	{
			points[k].x = data[i+START_INDEX]*cos_bearing_mat[i];
			points[k].y = data[i+START_INDEX]*sin_bearing_mat[i];
			points[k].theta = bearing_mat[i];
			points[k].r = data[i+START_INDEX];
			points[k].point_index = k;
			k++;
    	}
    }
    valid_count = k;
//    printf("Valid number of points: %d\n", valid_count);
}

void height::LineDetection()
{
    detected_line_local.clear();
    int start_index = 0;

    for (int i=0; i<valid_count-1; i++)
    {
        if(fabs(points[i].r-points[i+1].r)>100 /*|| points[i+1].r<20*/ || i==valid_count-2)
        {
            if (start_index != i)
            {
                int size = i-start_index+1;
                if(size>MIN_SM_GROUP_SIZE)
                	SplitMerge(&points[start_index], size, SPLIT_MERGE_THR);
            }
            start_index = i+1;
        }
    }
}

int height::GroundDetection()
{
    // Exclude lines with wrong orientation
	list<line_> lines;
    list<line_> lines2;

    list<line_>::iterator iter1 = detected_line_local.begin();
    list<line_>::iterator iter2 = detected_line_local.begin();
    for( ; iter1!=detected_line_local.end(); iter1++)
	{
        if (fabs(iter1->theta - a -1.57) < 0.24 ) // Reversed sign because laser scanner 180 degrees now
        {
			lines.push_back(*iter1);
//			cout<< "h is"<<iter1->r <<endl;
//			cout<< "theta is "<<iter1->theta <<endl;
//			cout<< "length is "<<iter1->length <<endl;
//			cout<<"End"<<endl;
        }
	}

	line_ temp;
	iter1 = lines.begin();
	iter2 = lines.begin();

	// Sort lines according to their perpendicular distances to the scanner
    for( ; iter1!=lines.end(); iter1++)
    {
		iter2 = iter1;
		for( ; iter2!=lines.end(); iter2++)
		{
			if(iter1->r < iter2->r)
			{
				temp = *iter1;
				*iter1 = *iter2;
				*iter2 = temp;
			}
		}
    }

    iter1 = lines.begin();
	double furthest = iter1->r;

	for( ; iter1!=lines.end(); iter1++)
	{
		if( fabs(iter1->r - furthest) < 250 )
		{
			lines2.push_back(*iter1);
//			cout<< "h is"<<iter1->r <<endl;
//			cout<< "theta is "<<iter1->theta <<endl;
//			cout<< "length is "<<iter1->length <<endl;
//			cout<<"End"<<endl;
		}
	}

	iter1 = lines2.begin();
	for( ; iter1!=lines2.end(); iter1++)
	{
		iter2 = iter1;
		for( ;iter2 != lines2.end();iter2++)
		{
			if(iter1->length < iter2->length)
			{
				temp = *iter1;
				*iter1 = *iter2;
				*iter2 = temp;
			}
		}
	}

	iter1 = lines2.begin();

	if (iter1 == lines2.end())
	{
		height_flag = false;
		return 0;
	}

	else
	{
		if ((iter1->p2.point_index - iter1->p1.point_index) < 40 )
		{
			height_flag = false;
			return 0;
		}

		else
		{
			height_flag = true;
			ground = LMS(iter1->p1.point_index+3, iter1->p2.point_index-3);

		//	cout << "Theta, length, distance: " << iter1->theta*180/pi << ", "
		//		 << iter1->length << ", " << iter1->r << endl;
		//	h = iter1->r;
			h = ground.r * cos(b);
		//	cout<<"R is "<<h<<endl;
			return 1;
		}
	}
}

void height::SplitMerge(point detected_points[], int n , int threshold)
{
    int last = n-1;
    double m, c, theta, r;
    double point_distance[1081];
    double winner_value;
    int winner_index;
    double l;
    line_ detect;
    if(fabs(detected_points[last].x-detected_points[0].x)>=1) // y = mx+c
    {
        m = (detected_points[last].y-detected_points[0].y)/(detected_points[last].x-detected_points[0].x);
        c = detected_points[0].y-detected_points[0].x*m;
        theta = atan(-1/m);
        r = c*sin(theta);
        if(m*c>0)
        	 theta = theta + pi;
    }
    else // x = my+c
    {
        m = (detected_points[last].x-detected_points[0].x)/(detected_points[last].y-detected_points[0].y);
        c = detected_points[0].x-detected_points[0].y*m;
        theta = atan(-m);
        r = c*cos(theta);
        if(m*c<0)
           	 theta = theta + pi;
    }

    r = fabs(r);

    point_distance[0] = detected_points[0].x*cos(theta)+detected_points[0].y*sin(theta)-r;
    winner_value = fabs(point_distance[0]);
    winner_index = 0;
    for (int j=1; j<last; j=j+4)
    {
        point_distance[j] = detected_points[j].x*cos(theta) + detected_points[j].y*sin(theta) - r;
        if (fabs(point_distance[j]) > winner_value)
        {
            winner_value = fabs(point_distance[j]);
            winner_index = j;
        }
    }

    if (winner_value > threshold)
    {
        SplitMerge(detected_points, winner_index+1, threshold);
        SplitMerge(&detected_points[winner_index+1], n-winner_index-1, threshold);
    }
    else
    {
        l = (detected_points[last].x-detected_points[0].x)*(detected_points[last].x-detected_points[0].x);
        l+= (detected_points[last].y-detected_points[0].y)*(detected_points[last].y-detected_points[0].y);
        l = sqrt(l);
        detect.p1 = detected_points[0];
        detect.p2 = detected_points[last];
        detect.theta = theta;
        detect.r = r;
        detect.length = l;
        detected_line_local.push_back(detect);
    }
}

line_ height::LMS(int start,int end)
{
   int n = end-start+1;
   double X[n];
   double Y[n];
   double xTx[2][2];
   double xTx_R[2][2];
   int i = 0;
   double m=0;
   double c=0;
   line_  result;
   xTx[0][0]=0;
   xTx[0][1]=0;
   xTx[1][0]=0;
   xTx[1][1]=0;

   for (i=0; i<n; i++)
   {
	 X[i] = points[start+i].x;
	 Y[i] = points[start+i].y;
   }

   for(i=0; i<n; i++)
   {
	   xTx[0][0]+=X[i]*X[i];
	   xTx[0][1]+=X[i];
	   xTx[1][0]+=X[i];
	   xTx[1][1]+=1;
   }

  double det = xTx[0][0]*xTx[1][1]-xTx[0][1]*xTx[1][0];

  if(fabs(det)<0.00001)
  {
	  result.r = 0;
	  return result;
  }

  xTx_R[0][0] = xTx[1][1]/det;
  xTx_R[1][1] = xTx[0][0]/det;
  xTx_R[0][1] = -xTx[0][1]/det;
  xTx_R[1][0] = -xTx[1][0]/det;

  for(i=0; i<n; i++)
  {
	m += (xTx_R[0][0]*X[i]+xTx_R[0][1])*Y[i];
	c += (xTx_R[1][0]*X[i]+xTx_R[1][1])*Y[i];
  }

  result.theta = atan(-1/m);
  result.r = fabs(c*sin(result.theta));
  if(m*c>0)
  	 result.theta = result.theta + pi;
  return result;
}

